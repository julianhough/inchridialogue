import numpy as np
import cv2
import config

class Object(object):
	"""
	Keeps track of an object's properties, after it has been segmented
	Usage:
		- to calculate raw shape features call calcShapeProp
		- to calculate raw colour features call calcHSV
		- to get a probabability distribution over a set of 12 shapes
		  and 8 colours call classify
		- to update it's current location, call update with a new object
		  as argument
	"""
	def __init__(self, id, image, contour, selected = False):
		self.id = id
		self.contour = contour
		self.colour = None
		self.outlineColour = (0,0,0)
		self.position = None
		self.image = image
		self.x = 0
		self.y = 0
		self.shape = None
		self.huMoments = None
		self.selected = selected
		self.landmark = False
		self.left   = list()
		self.right  = list()
		self.top    = list()
		self.bottom = list()
		self.box = None
		# mask for the contour
		self.mask = np.zeros(self.image.getImage().shape, np.uint8)
		self.hsv_hist = None
		self.area = None
		self.confidence = 0
	
	def __repr__(self):
		"""
		The object's representation it's given by its id
		and its centroid
		"""
		return repr((self.id, self.x, self.y))
	
	def edges(self):
		"""
		Returns a list of the object's edges, as found from the contour
		Returns:
			Array of object's edges
		"""
		peri = cv2.arcLength(self.contour, True)
		approx = cv2.approxPolyDP(self.contour, 0.025 * peri, True)
		return approx
		
	def getHistogram(self):
		"""
		Returns the object's hsv_histrogram signature
		Returns:
			Hsv histrogram bin - 2 - dimensional array
		"""
		return self.hsv_hist
	
	def getBGRHistogram(self):
		"""
		Returns the object's hsv_histrogram signature
		Returns:
			Hsv histrogram bin - 2 - dimensional array
		"""
		return self.bgr_hist
		
	def update(self, object, gamma = config.gamma):
		"""
		As we are tracking an object through different time frames,
		we need to constantly update it's properties such as any wrong
		initial belief are corrected throughout the scene
		
		Update model 
		
		P (Object = o) = gamma * P(Object = o) + (1 - gamma) P (NewObject = o) 
		
		Args:
			object (Object) : the object detected in the new frame
			gamma (float) : weight given to previous beliefs to current belief
		Raises:
			Exception: Missing shape/colour classifier
		
		"""
		# only update the distributions if it is not partially hidden
		if self.getArea()/object.getArea() > config.minRelativeArea:
			if config.classifier == None:
				raise Exception("Missing shape classifier")
			newDistr = config.classifier.predict(object.getFeatures())
			if not newDistr.has_key('unknown'): 
				# only update if the shape is not too different(this can	
				# happen due to partial occlusions or wrong match
				self.__updateDistr(self.shape, newDistr, gamma)
				self.__updateHistogram(object.getHistogram(), object.getBGRHistogram(), gamma)
			if config.colourClassifier == None:
				raise Exception("Missing colour classifier")
			newDistr = config.colourClassifier.predict(object.getColourFeatures())
			self.__updateDistr(self.colour, newDistr, gamma)
				
		else:
			if config.verbose >= 1:
				print "Not updating distribution for " + str(self.getId()) + " due to partial occlusion"
		
		# but always update the current position/contour
		self.updateContour(object.getContour())
		self.__updateProperties(object)
		
	def updateContour(self, contour):
		"""
		When the object moves, update to the its position
		Args:
			contour(vector of points) : the new object contour
		"""
		self.contour = contour
		moments = cv2.moments(self.contour)
		self.__setGlobalPosition(moments)
	
	def __boundingBoxHsv(self):
		"""
		Calculates mean colour around the object, used as feature
		in the colour classification, this can help keeping the 
		colour constancy, by using the surrounding area's colour
		as a sort of normalization
		"""
		x, y , w, h = self.getBox()
		box = self.image.getHSV()[x:w, y:h]
		self.boxHSV = cv2.mean(box)[:2]
	
	def calcHSV(self):
		"""
		Calculates raw colour properties, does not 
		included classification
		
		Returns:
			array of features used for colour classification
		"""
		img = self.image.getBlurred()
		# Convert BGR to HSV
		hsv_img = self.image.getHSV()
		
		#this delimits the object's ROI
		cv2.drawContours(self.mask,[self.contour],0, (255,255,255), -1)
		mask, m, mm = cv2.split(self.mask)
		
		# mean hsv and rgb values
		self.hsv = cv2.mean(hsv_img, mask = mask)[:3]
		self.rgb = cv2.mean(img, mask = mask)[:3]

		# don't use very dark pixels
		dark = hsv_img[...,2] < 32
		hsv_img[dark] = 0
		size = 60
		# histogram bins used in tracking
		self.hsv_hist = cv2.calcHist([hsv_img], [0,1], mask, [size,size], [0, 180, 0, 255])
		self.bgr_hist = cv2.calcHist([img], [0,1,2], mask, [16,16,16], [0, 255, 0, 255, 0, 255])
		
		cv2.normalize(self.hsv_hist,self.hsv_hist,0, size,cv2.NORM_MINMAX)
		cv2.normalize(self.bgr_hist,self.bgr_hist,0, 16,cv2.NORM_MINMAX)
		# the HS values of the surrounding area used for colour constancy
		# in classification
		self.__boundingBoxHsv()
		return  self.getColourFeatures()
	
	def __updateProperties(self, newObject):	
		self.hsv = newObject.getHSV()
		self.rgb = newObject.getRGB()
		self.orientation = newObject.getOrientation()
		self.skewness = newObject.getSkewness()
		self.hskewness = newObject.getHSkewness()	
		self.nedges = len(newObject.edges())
		self.huMoments = newObject.getFeatures()
		self.area = newObject.getArea()
		
	def calcShapeProp(self):
		"""
		Calculates raw shape properties, does not 
		included classification
		
		Returns:
			array of features currently used for shape classification 

		"""		
		# calculate moments 
		mo = cv2.moments(self.contour)
		self.__setGlobalPosition(mo)
		if  mo['nu20'] != mo['nu02'] :
			theta = np.arctan(200 * mo['nu11']/(100 * mo['nu20'] - 100 * mo['nu02'])) / 2
		else:
			theta = 0
		self.orientation = (theta / np.pi) * 180
		
		# if n30 close to 0 the probability distribution of the object on x axis is symmetric
		self.skewness = 'symmetric'
		
		if mo['nu30'] < -0.005: # perfectly symmetric when it's exactly 0, allow for some variation
			self.skewness = 'left-skewed'
		elif mo['nu30'] > 0.005:  # perfectly symmetric when it's exactly 0, allow for some variation
			self.skewness = 'right-skewed'
		
		# if n03 close to 0 the probability distribution of the object on y axis is symmetric
		self.hskewness = 'symmetric'		
		if mo['nu03'] < -0.005:
			self.hskewness = 'top-skewed'
		elif mo['nu03'] > 0.005:
			self.hskewness= 'bottom-skewed'
			
		self.nedges = len(self.edges())
		self.initContour = self.contour
		self.huMoments = cv2.HuMoments(mo)
		
		return self.huMoments
		
	def classify(self):
		"""
		This will give a probability distribution over the shapes and
		colours, the classifier must be previously initialised and trained
		Raises:
			Expetion : Missing shape/colour classifier
		"""
		if config.colourClassifier == None:
			raise Exception("Missing colour classifier")
		self.colour = config.colourClassifier.predict(self.getColourFeatures())
		if config.classifier == None:
			raise Exception("Missing shape classifier")
		self.shape = config.classifier.predict(self.getFeatures())
	
	def extractProperties(self):
		"""
		Calculates both colour and shape properties, does not include classification,
		just raw features
		"""
		self.calcHSV()
		self.calcShapeProp()
		
	def __updateHistogram(self, newHist, newBGRHist, gamma = config.gamma):
		"""
		Since the object is present in different timestamps, its
		histogram signature might change, for example due to lighting 
		conditions; gamma is used to control how much importance we
		give to previous belief
		Args:
			newHist () : histogram at time t
			gamma (float) : weight given to previous beliefs to current belief
			
		Update model 
		
		P (hsv_hist = hist) = gamma * P(hsv_hist = hist) + (1 - gamma) P (newHist = hist) 
		
		"""
		h,s= self.hsv_hist.shape
		self.hsv_hist = self.hsv_hist * gamma + (1-gamma) * newHist
		self.bgr_hist = self.bgr_hist * gamma + (1-gamma) * newBGRHist

		cv2.normalize(self.hsv_hist,self.hsv_hist,0, 60, cv2.NORM_MINMAX)
		cv2.normalize(self.bgr_hist,self.bgr_hist,0, 16, cv2.NORM_MINMAX)
		
	def __updateDistr(self, distr, newDistr, gamma = config.gamma):
		"""
		As we are tracking an object through different time frames,
		we need to constantly update it's properties such as any wrong
		initial belief are corrected throughout the scene
		
		Update model 
		
		P (Object = o) = gamma * P(Object = o) + (1 - gamma) P (NewObject = o) 
		
		Args:
			distr(dict) : the distribution at time t - 1 
			newDistr(dict) : the distribution at time t
			gamma (float) : weight given to previous beliefs to current belief
		Raises:
			Exception: Missing shape/colour classifier
		
		"""
		for key, prob in distr.iteritems():
			distr[key] = gamma * prob + (1 - gamma) * newDistr[key]
	
	def __setGlobalPosition(self, mo):
		"""
		Calculates centorid, area and global spatial
		positioning 
		Args:
			mo - the moments calculated from the contour
		"""
		# calculate centroids of object 
		if mo['m00'] != 0:
			self.x = int(mo['m10']/mo['m00'])
			self.y = int(mo['m01']/mo['m00'])
		
		# the area is the 0 order moment
		self.area = mo['m00']
		
		# to compute the global positioning in the image we need to
		# know its size
		sizeX, sizeY = self.image.getSize()
		
		# in the experiment we used a white delimiting area and we
		# want to report the coordinates relative to that
		if config.reportRelative and config.boundingBox != None:
			x, y , sizeX, sizeY = config.boundingBox.getBox()
			self.x -=x
			self.y -=y
			
		# get position relative to the size of the image
		if self.x < sizeX / 3:
			position = "left "
		elif self.x < 2 * sizeX / 3:
			position = "center "
		else:
			position = "right "
		
		if self.y < sizeY / 3:
			position += "top"
		elif self.y < 2 * sizeY / 3:
			position += "center"
		else :
			position += "bottom"
		
		self.position = position
	
	def computeRelativePos(self):
		"""
		Compute the relative spatial ordering of the object
		"""
		if self.image.getXAxis():
			for object in self.image.getXAxis():
				if object.getXPosition() < self.x:
					self.left.append(object)
				else:
					break
		if self.image.getXAxis():
			for object in reversed(self.image.getXAxis()):
				if object.getXPosition() > self.x:
					self.right.append(object)
				else:
					break
					
		if self.image.getYAxis():
			for object in self.image.getYAxis():
				if object.getYPosition() < self.y:
					self.top.append(object)
				else:
					break
	
		if self.image.getYAxis():
			for object in reversed(self.image.getYAxis()):
				if object.getYPosition() > self.y:
					self.bottom.append(object)
				else:
					break
		return self.bottom
	
	def isSelected(self):
		"""
		Used only in the case of computer generated images,
		(TAKE corpous) when a piece has been identified as selected
		"""
		return self.selected
		
	def printProps(self):	
		"""
		Print object's properties
		"""
		print "Object " + str(self.id)
		print self.colour
		print self.getShape()
		print self.shape
		print [self.x, self.y]
		self.__printRelativePos__()
		print "\n"
		
	def __printRelativePos__(self):

		left = [o.getId() for o in self.left]
		print "Left" + str(left)
		
		right = [o.getId() for o in self.right]
		print "Right" + str(right)
		
		top = [o.getId() for o in self.top]			
		print "Top" + str(top)
		
		bottom = [o.getId() for o in self.bottom]
		print "Bottom" + str(bottom)
		
	def setShapeDistribution(self, newDistr):
		self.shape = newDistr
		
	def setColourDistribution(self, newDistr):
		self.colour = newDistr
	
	def setHistogram(self, newHist):
		self.hsv_hist = newHist
		
	def getFeatures(self):
		return self.huMoments
	
	def getColourFeatures(self):
		return np.append(self.hsv[:2], self.rgb)
		
	def getInitialContour(self):
		return self.initContour

	def getMask(self):
		return self.mask
	
	def getSkewness(self):
		return self.skewness
	
	def getNbEdges(self):
		return self.nedges
	
	def getHSkewness(self):
		return self.hskewness
		
	def setId(self, id):
		self.id = id
	
	def getWindow(self, ws):
	
		return self.x1 - ws, self.y1 - ws, self.x1 + self.x2 + ws, self.y1 + self.y2 + ws 
		
	def getBox(self):
	
		self.x1, self.y1, self.x2, self.y2 = cv2.boundingRect(self.contour)
		return self.x1, self.y1, self.x1 + self.x2, self.y1 + self.y2
	
	def getBB(self):
		return cv2.boundingRect(self.contour)
		
	def getHSV(self):
		return self.hsv
	
	def getRGB(self):
		return self.rgb
		
	def getCentroid(self):
		return (self.x, self.y)
	
	def getArea(self):
		return self.area

	def setDistribution(self, distr):
		self.shape = distr
	
	def getOrientation(self):
		return self.orientation
		
	def getShapeDistribution(self):
		return self.shape
	
	def getShape(self):
		if self.shape != None:
			return max(self.shape, key=self.shape.get)
		return self.shape
		
	def getGlobalPosition(self):
		return self.position
		
	def getContour(self):
		return self.contour
		
	def getColour(self):
		if self.colour != None:
			return max(self.colour, key=self.colour.get)
		return self.colour
	
	def getColourDistribution(self):
		return self.colour
			
	def getId(self):
		return self.id
		
	def getImage(self):
		return self.image
	
	def getXPosition(self):
		return self.x
	
	def getYPosition(self):
		return self.y
	
