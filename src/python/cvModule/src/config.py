classifier = None
colourClassifier = None
saveToXML = False
boundingBox = None
writeToVideo = False
perspectiveTransform = False
verbose = 0
reportRelative = False
gamma = 0.9 # value used for belief updating gamma * old + (1 - gamma) * new
minSize = 2000 # minimum object area, 1000 for semdial demo
maxSize = 6000 # maximum object area, 4000 for semdial demo
staticMinSize = 600 # minimum object area for computer generated images
ksize = 17  # Gaussian kernel size. (sizex, sizey)
sigma = 2.45 # Gaussian kernel standard deviation
lowCannyTH = 5 # low threshold value for Canny edge detector
highCannyTH = 30 # high value for  Canny edge detector
argmax = True
boundingBoxArea = 50000 # minimum area for the bounding box used in the experiment for calibration
posColTH  = 1.64 # threshold for position and colour similarity 
colTH 	  = 1.1 # threshold for colour similarity
gaussianWin = 30 # gaussian search window
minRelativeArea = 0.9 # minimum fraction of object area's such that the object is considered unoccluded
					 # ie non-occluded if area1/area2 > 85%
fps = 10 # number of frames per second at which the video is processed
bufferEmpty = False # buffer empty frames at the beginning of the video if a start is specified, this is done so that
					# the length of the objects match
rsb = False
