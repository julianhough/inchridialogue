'''
Created on Jul 16, 2015

@author: casey
'''
from xml.dom import *
from xml.dom.minidom import * 
import rsb
import numpy as np
import config


class RsbOutput():
    
    def __init__(self):
        """
        Args:
            title: the name of the root tag
        """
        title = "fake"
        self.doc = Document()
        self.root = self.doc.createElement('file')
        self.root.appendChild(self.doc.createTextNode("title"))
        self.doc.appendChild(self.root)
        self.filename = title + '.xml'
        #self.__createBeginTag(title)
        self.nodelist = list()
        #TODO decide on how to talk to InproTK and PentoRob on different nodes          
        self.object_informer = rsb.createInformer("/opencv/objects/", dataType=str)
        self.informer = rsb.createInformer("/opencv/pentorob/", dataType=str) #fake inprotk source, should really go to inprotk
        self.pentorob_TL = (40,40) #the limits of pento rob's movements, TODO can actually get these upon initially connecting to PentoRob
        self.pentorob_BR = (540,370)
        self.pentorob_size = (self.pentorob_BR[0] - self.pentorob_TL[0],self.pentorob_BR[1] - self.pentorob_TL[1])
        self.offset_array = np.array([self.pentorob_TL[0],self.pentorob_TL[1]])
        self.b_box_x = None
        self.b_box_y = None
        self.b_box_scale = None
        
    def publish_objects(self, objects):
        for obj in objects:
            obj =  self.format_object(obj)
            #print obj.toprettyxml()
            self.object_informer.publishData(obj.toprettyxml())
        

    def publish_coordinates(self,x,y,perspective=True,scale=False,make_offset_to_box=False):
        """
    	If scale is true, scale the coordinates relative to PentoRob's coord system.
    	If make_offset_to_box is true, subtract the x and y corner positions of the box
    	Else output a raw x,y coordinate from the video screen
    	"""
        print x,y
        if perspective == True: #We're doing the fancy transform to pentorob's coordinates
            M = self.bbox_Matrix
            def dest(x,y):
                newx = (M[0][0]*x + M[0][1]*y + M[0][2])/(M[2][0]*x + M[2][1]*y + M[2][2])
                newy = (M[1][0]*x + M[1][1]*y + M[1][2])/(M[2][0]*x + M[2][1]*y + M[2][2])
                return [newx,newy]
            newpoint = dest(x,y) + self.offset_array
            x, y = newpoint[0], newpoint[1]
        else:
            if (make_offset_to_box): #with the pieces this could be false
                x-=self.b_box_x
                y-=self.b_box_y
                print x,y
            if (scale): #scaling to PentoRob's coordinate system	
                x, y = self.b_box_scale * float(x), self.b_box_scale * float(y)
                x+=self.pentorob_TL[0]
                y+=self.pentorob_TL[1]	
        #print x,y
        self.informer.publishData('command,MOVE:'+str(int(x))+":"+str(int(y))) #PentoRob only takes ints
    
    def publish_commands(self, char):
        """Publish the char-based commands in terms of chars and keysymbs typed at the GUI"""
        if char == 32:
            char = 'space'
        elif char == 13:
            char = 'Return'
        elif char == ord('q'):
            char = 'Escape'
        else:
            char = unichr(char)
        print "key," + str(char)
        self.informer.publishData("key," + str(char))

    def set_boundingbox_scale(self, x, y , sizeX, sizeY):
        """
    	Simply takes the top left x,y and bottom righ x,y coords of box from the video screen.
    	This is scaled to PentoRob's coordinates system.
    	Whatever the size of the bounding box according to the cvModule, scale any positions
    	On the screen to PentoRob's boundary area.
    	"""
        #print x, y , sizeX, sizeY
        self.b_box_x = x
        self.b_box_y = y
        self.b_box_scale = float(self.pentorob_size[0])/float((sizeX-x)) #TODO assuming uniform scale
        #print "bounding box scaling to pentorob", self.b_box_scale

    def set_boundingbox_perspective(self, M):
        """
    	Setting the transformation matrix to PentoRob's coordinates system.
    	"""	
        self.bbox_Matrix = M
    
    def format_object(self, obj):
        """
        Construct tree for each obj
        """
        xmlObject = self.doc.createElement('obj')
        
        xmlObject.setAttribute('id', str(obj.getId()))
        
        position = self.doc.createElement('position')
        position.setAttribute('global', str(obj.getGlobalPosition()))
        myx, myy = obj.getCentroid()
        
        #TODO am converting to pentorob coordinates here!
        #Should feed inprotk both set of coords
        if not self.b_box_x == None:
            #print myx, myy
            myx+=self.b_box_x #reset from before
            myy+=self.b_box_y
            M = self.bbox_Matrix
            def dest(x,y):
                newx = (M[0][0]*x + M[0][1]*y + M[0][2])/(M[2][0]*x + M[2][1]*y + M[2][2])
                newy = (M[1][0]*x + M[1][1]*y + M[1][2])/(M[2][0]*x + M[2][1]*y + M[2][2])
                return [newx,newy]
            newpoint = dest(myx,myy) + self.offset_array
            myx, myy = int(newpoint[0]), int(newpoint[1])
            #print "pentorob", str(obj.getId()), myx,myy
        else:
            print "warning no bounding box"
        x = myx
        y = myy
        position.setAttribute('x', str(x))
        position.setAttribute('y', str(y))
        xmlObject.appendChild(position)
        
        shape = self.doc.createElement('shape')
        shape.setAttribute('BestResponse', obj.getShape())
        shapeDist = self.doc.createElement('distribution')
        shapeDistr = obj.getShapeDistribution()
        for key, prob in shapeDistr.iteritems():
            shapeDist.setAttribute(key , str(prob))
        shape.appendChild(shapeDist)
        orientation = self.doc.createElement('orientation')
        orientation.setAttribute("value" , str(obj.getOrientation()))
        shape.appendChild(orientation)
        
        skewness = self.doc.createElement('skewness')
        skewness.setAttribute("horizontal" , str(obj.getSkewness()))
        skewness.setAttribute('vertical', str(obj.getHSkewness()))
        shape.appendChild(skewness)
        
        nbEdges = self.doc.createElement('nbEdges')
        nbEdges.setAttribute("value", str(obj.getNbEdges()))
        shape.appendChild(nbEdges)
        
        xmlObject.appendChild(shape)
        
        
        colour = self.doc.createElement('colour')
        colour.setAttribute('BestResponse', obj.getColour())
        
        colDist = self.doc.createElement('distribution')
        colourDistr = obj.getColourDistribution()
        for col, prob in colourDistr.iteritems():
            colDist.setAttribute(str(col) , str(prob))
        colour.appendChild(colDist)
        hsvColour = self.doc.createElement('hsvValue')
        h,s,v = obj.getHSV()
        hsvColour.setAttribute("H", str(h))
        hsvColour.setAttribute("S", str(s))
        hsvColour.setAttribute("V", str(v))
        colour.appendChild(hsvColour)
        
        rgbColour = self.doc.createElement('rgbValue')
        b,g,r = obj.getRGB()
        rgbColour.setAttribute("R", str(r))
        rgbColour.setAttribute("G", str(g))
        rgbColour.setAttribute("B", str(b))
        colour.appendChild(rgbColour)
        
        xmlObject.appendChild(colour)
        return xmlObject;
        