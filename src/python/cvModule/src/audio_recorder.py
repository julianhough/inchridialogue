#simplest thing imaginable which listens for START abd STOP commands on rsb with a filename and starts and stops recording
import rsb
import subprocess
import time
import argparse
import os

current_milli_time = lambda: int(round(time.time() * 1000))

home_dir = "/home/dsg-labuser/Desktop/hri/raw_data/audio/"

def main(args):
 
    #make folder for the whole experiment
    folder = home_dir + str(args.participant)
    #folder  = "/home/dsg-labuser/Desktop/rnn_experiments/"+ s['exp_id']
    if not os.path.exists(folder): 
    #    quit = raw_input('Overwrite contents of folder for experiment {}? [y][n]'.format(args.participant))
    ##    if quit != "y": return
    #else:
        os.makedirs(folder)
    
    listener = rsb.createListener('/inprotk/rr/')
    print "audio recorder got listener"
    p = None
    
    def rsb_handler(x):
        global p
        """ Handles RSB events. """
        
        d = x.data
        #print "rsb message: " + d
        if "START_RECORD" in d:
            
            recordingName = d.split(":")[1]
            command  = "arecord -f S16_LE -D hw:0,0 {0}.wav".format(folder + "/" + recordingName + ":" + str(current_milli_time()))
            #command  = "arecord -f S16_LE -D hw:0,0 {0}.wav".format(folder + "/" + recordingName + ":" + str(current_milli_time()))
            print command
            p = subprocess.Popen("exec " + command, shell=True)
        elif "STOP_RECORD" in d:
            if p:
                print "killing recording"
                #p.terminate()
                p.kill()
            recordingName = False
    
    listener.addHandler(rsb_handler)
    print "added handler"
    print "waiting for input.."
    going = True
    while going:
        pass
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("participant", type=int, help="The participant number")
    args = parser.parse_args()
    main(args)