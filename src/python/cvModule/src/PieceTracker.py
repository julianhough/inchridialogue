import cv2
from Image import *
from collections import Counter
import math
import rsb
import numpy as np

DIST_COL = 0
DIST_LOC_COL = 1
DIST_LOC_COL_SHAPE = 2
CV_COMP_BHATTACHARYYA = 3

class PieceTracker():
	"""
	Tracker class - tracks a list of objects
	Usage: To register objects to be tracked call the add(Object) method
		   call the track method with a new list of objects to do the 
		   matching and update the positions of the currently tracked 
		   objects
	"""
	def __init__(self, distance = DIST_LOC_COL):
		self.pieces = []
		self.frame = None
		self.previousFrame = None
		# self.fgbg = cv2.createBackgroundSubtractorMOG2(history = 1)
		self.wasOccluded = set()
		self.idCount = 0
		self._alpha = self._beta = 0.5
		self.newObject = None
		self.matched = set()
		self.newMatched = set()
		self.distribution = {}
		self.server = rsb.createListener('/inprotk/rr/')
		self.server.addHandler(self.rsbInputHandler)
		self.reset = False #a signal to reset the tracking
		self.recordingName = None #the name of the recording of this interaction
		self.selected = '-1'
		self.dist = None

	def rsbInputHandler(self, x):
		d = x.data
		if "START_RECORD" in d:
			self.recordingName = d.split(":")[1]
		elif "STOP_RECORD" in d:
			self.recordingName = False
		elif d == "<reset>":
			self.reset = True
			print "Reseting!"
		elif d.startswith('[object'):
			d = d[9:-2]
			d = d.split('][')
			self.dist = d
			print "distribution", d
		else:
			print "selection", d
			if d in [str(i) for i in range(0,13)]:
				print "arg max selected!!",d
			self.selected = d
		
	def add(self, object):
		""" 
		Add an object to be tracked
		Args:
			object (Object): Identified object to be tracked
		"""
		self.pieces.append(object)
		object.setId(self.idCount)
		self.idCount += 1
		
	def __locDistance(self, new, old):
		"""
		Returns the Euclidean distance between two objects
		Args:
			new(Object) : object detected in current frame
			old(Object) : object that is tracked
		"""
		x1, y1 = new.getCentroid()
		x2, y2 = old.getCentroid()

		return np.sqrt((x2 - x1)**2 + (y2-y1)**2) 
	
	def __histogramDist(self, object, newObject):
		"""
		Returns the colour distance between two objects by computing the correlation 
		between the colour histograms
		Args:
			new(Object) : object detected in current frame
			old(Object) : object that is tracked
		"""
		a = cv2.compareHist(object.getHistogram(), newObject.getHistogram(), CV_COMP_BHATTACHARYYA)
		return a
#		b = cv2.compareHist(object.getBGRHistogram(), newObject.getBGRHistogram(), CV_COMP_BHATTACHARYYA)
#		return min(a,b)

	def __shapeDist(self, object, newObject):
		"""
		Returns the shape similarity of two objects by computing by comparing their 
		contours, the contour detected in the first frame is used to avoid comparing
		against a partially occluded contour
		Args:
			new(Object) : object detected in current frame
			old(Object) : object that is tracked
		"""
		return cv2.matchShapes(newObject.getInitialContour(), object.getInitialContour(), 2, 0.0) 
	
	def distance(self, newObject, oldObject, measure = DIST_LOC_COL):
		"""
		Returns the distance between two objects 
		
		Args:
			new(Object) : object detected in current frame
			old(Object) : object that is tracked
		"""
		if measure == DIST_COL:
			# if self.__shapeDist(newObject, object) > 4:
				# return 0
			return np.exp(1 - self.__histogramDist(newObject, oldObject))
		if measure == DIST_LOC_COL:
			return self.gaussian(newObject , oldObject) * np.exp(1 - self.__histogramDist(newObject, oldObject))
		else:
			return self.gaussian(newObject , oldObject) * np.exp(1 - self.__histogramDist(newObject, oldObject)) * ( 2 - self.__shapeDistance(newObject, oldObject)) 
			
	def update(self, trackedObj, object):
		"""
		This method is called to update the current position of a tracked object
		as well as update our beliefs about the class the object belongs to
		(colour or shape)
		Args:
			trackedObj(Object) - object from the set of self.pieces that is 
									 currently being tracked
			object(Object)	   - the object identified in current frame, matching
								 the tracked object
		"""
		trackedObj.update(object)
		
		if trackedObj in self.wasOccluded:
			self.wasOccluded.remove(trackedObj)
			
	def __argmax(self, distance):
		"""
		Args:
			distance (2-dim matrix) distance between pairs of objects(current frame 
			and tracked)
		Returns:
			(i, j) Indexes of the best match in the distance matrix
			where i is the object in current frame and j the tracked
			object
		"""
		return np.unravel_index(distance.argmax(), distance.shape)
	
	def match(self, oldObjects, threshold, dist):
		"""
		Args:
			oldObjects: list of objects to track 
			threshold : minimum similarity distance between objects
			dist (DIST_COL | DIST_LOC_COL | DIST_LOC_COL_SHAPE ) - distance measure used
				DIST_COL = 0           - use only colour
				DIST_LOC_COL = 1	   - use colour and location
				DIST_LOC_COL_SHAPE = 2 - use colour location and shape
		
		Returns:
			List of unmatched objects
		"""
		distance = np.zeros((len(self.newObjects), len(oldObjects)))
		
		for i in range(0, len(self.newObjects)):
			for j in range(0, len(oldObjects)):
				distance[i][j] = self.distance(self.newObjects[i], oldObjects[j], dist)
				if config.verbose > 2:
					print str(newObject.getId()) +" " +str(oldObject.getId()) + " "+str(self.__histogramDist(newObject, oldObject))
		
		if config.argmax:
			for a in range(0, len(self.newObjects)):
				if distance.size != 0:
					i, j = self.__argmax(distance)

					if distance[i][j] > threshold:
						newObject = self.newObjects[i]
						bestMatch = oldObjects[j]
						del self.newObjects[i]
						del oldObjects[j]
						self.update(bestMatch, newObject)
						distance = np.delete(distance, j, 1)
						distance = np.delete(distance, i, 0)
					else: 
						break
		else:
			
			for i in range(0,len(oldObjects)):
				oldObject = oldObjects[i]
				if dist == DIST_LOC_COL:
					probs = self.normalise(np.transpose(distance)[i])
				else:
					probs = self.normalise2(np.transpose(distance)[i])
				
				distr = dict()
				for key, x in oldObject.getShapeDistribution().iteritems():
					distr[key] = config.gamma * x 
				for j in range(0, len(self.newObjects)):
					newObject = self.newObjects[j]
					newProb = probs[j]
					for key, prob in newObject.getShapeDistribution().iteritems():

						distr[key] += (1 - config.gamma) * newProb * prob
				oldObject.setShapeDistribution(distr)
				
				distr = dict()
				for key, x in oldObject.getColourDistribution().iteritems():
					distr[key] = config.gamma * x 
				for j in range(0, len(self.newObjects)):
					newObject = self.newObjects[j]
					newProb = probs[j]
					for key, prob in newObject.getColourDistribution().iteritems():
						distr[key] += (1 - config.gamma) * newProb * prob	
				oldObject.setColourDistribution(distr)

				j = np.argmax(probs)
	
				if distance[i][j] > threshold:
					matchingObject = self.newObjects[j]
					oldObject.updateContour(matchingObject.getContour())
					oldObject.updateHistogram(matchingObject.getHistogram(), matchingObject.getBGRHistogram())
					self.matched.add(oldObject)
					self.newMatched.add(matchingObject)
					if oldObject in self.wasOccluded:
						self.wasOccluded.remove(oldObject)
		
		return oldObjects
		
	def track(self, newObjects):
		"""
		This function gets a list of objects identified in the current frame(time t)
		and tries to match these against the list of objects that are tracked
		
		The distance between two objects is calculated as a combination of Euclidean
		distance(of the object's centroids) and the distance between their colour
		histograms - this is the correlation between the histogram bins
		
		Args:
			newObejects(list of Objects) : objects as identified in the timeframe t
		Returns:
			A list of the matched objects, with their updated location
		"""
		self.newObjects = newObjects
		if config.verbose > 1:
			print "Found " + str(len(newObjects)) + " new objects"
		
		# match first the occluded pieces using both location and colour histogram
		unoccluded = list()
		for piece in self.pieces:
			if piece not in self.wasOccluded:
				unoccluded.append(piece)
		
		unmatched = self.match(unoccluded, config.posColTH, DIST_LOC_COL)
		if not config.argmax:
			a = Counter(self.pieces)
			b = Counter(self.matched)
			unmatched = a - b
			a = Counter(self.newObjects)
			b = Counter(self.newMatched)
			self.newObjects = a - b
			
		for piece in self.wasOccluded:
			unmatched.append(piece)
		
		if len(unmatched) > 0 and len(self.newObjects) > 0:
			# now match the remaining pieces with the pieces that were previously occluded
			unmatched = self.match(unmatched, config.colTH, DIST_COL)

		for piece in unmatched:	
			self.wasOccluded.add(piece)

		return self.pieces		
		
	def drawRectangles(self, img):
		"""
		Debugging/helper function, this draws the contours and bounding boxes
		in the video the is displayed when processing a video
		"""
		font_size = 2
		if config.reportRelative and config.boundingBox != None:
				relX, relY, w, h = config.boundingBox.getBox()
				cv2.rectangle(img,(relX, relY),(w,h), (0,0,255) ,2)
		for object in self.pieces:
			x,y,w,h = object.getBox()
			colour = object.outlineColour
			if object in self.wasOccluded:
				colour = (0,0,0)
			if self.dist is not None:
				for o in self.dist:
					o = o.split(':')
					if len(o) > 1:
						prob = float(o[1]) * 255
						if o[0] == str(object.getId()):
							colour = (prob,prob,prob)
			if self.selected != '-1':
				if self.selected == str(object.getId()):
					cv2.rectangle(img,(x-5,y-5),(w+5,h+5), (0,0,255) ,2)
			cv2.rectangle(img,(x,y),(w,h), colour ,2)
			cv2.drawContours(img, object.getContour(), -1, colour)
			desc = str(object.getId()) + " "
# 			if object.getColour():
# 				desc += str(object.getColour())
			cv2.putText(img,desc,(x,y-5), cv2.FONT_HERSHEY_PLAIN, font_size, (255, 255, 255))
			desc = ""
# 			if object.getShape():
# 				desc += str(object.getShape())
			cv2.putText(img,desc,(x+50,y+15), cv2.FONT_HERSHEY_PLAIN, font_size, (0, 0, 255))
			desc = object.getGlobalPosition()
			x, y = object.getCentroid()
			# cv2.putText(img,"Center " + str(object.getId()),(x+relX,y + relY), cv2.FONT_HERSHEY_PLAIN, font_size, (0, 0, 255))
	
		cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
		return img	
		
	def getOccluded(self):
		"""
		Returns:
			list of unmatched objects in the last frame
		"""
		return self.wasOccluded
		
	def gaussian(self, new, old):
		x0, y0 = new.getCentroid()
		x, y = old.getCentroid()
		return np.exp(-4*np.log(2) * ((x-x0)**2 + (y-y0)**2) / config.gaussianWin**2)
		 
	def normalise2(self,probs):
		probs = [ math.exp(x) for x in probs]
		probs = [x/np.sum(probs) for x in probs]
		return probs
	
	def normalise(self, distance):
		"""
		Takes a distance and returns a probability distribution
		"""
		probs = np.array(distance)
		# probs = [ math.exp(x) for x in probs]
		# if min(probs) < 0:
			# probs = [x - min(probs) for x in probs]	
		probs = [x/np.sum(probs) for x in probs]
		for x in probs:
			if x == 1:
				x = 1 - np.sum(probs) 

		return probs
		
	def findOccluded(self, frame):
		occluded = set()
		for piece in self.pieces:
			this_frame = frame
			mask = piece.getMask()
			overlap = cv2.bitwise_and(this_frame, mask)

			if np.sum(overlap) > 700:
				occluded.add(piece)
		return occluded
		
	def setFrame(self, frame):
		self.frame = frame
		# self.fgmask = self.fgbg.apply(frame)
	
	def inWindow(self, new, old):
		x1, y1 = new.getCentroid()
		x2, y2 = old.getCentroid()
		return (abs(x1 - x2) < 20 and abs(y1 - y2) < 20) 
	
	def tracked(self):
		return self.pieces