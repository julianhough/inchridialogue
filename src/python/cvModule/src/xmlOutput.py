import os
from xml.dom import *
from xml.dom.minidom import * 
import config

class XmlOutput():
	"""
	Incremental xml writer
	"""
	def __init__(self, title):
		"""
		Args:
			title: the name of the root tag
		"""
		self.doc = Document()
		self.root = self.doc.createElement('file')
		self.root.appendChild(self.doc.createTextNode(title))
		self.doc.appendChild(self.root)
		self.filename = title + '.xml'
		self.__createBeginTag(title)
		self.nodelist = list()
	
	def __createBeginTag(self, title):
		f = open(self.filename, "w")
		f.write("<file name=\" " + title + "\">\n")
		f.close()
		
	def __writeObject(self, object):
		"""
		Construct tree for each object
		"""
		xmlObject = self.doc.createElement('object')
		
		xmlObject.setAttribute('id', str(object.getId()))
		
		if config.static:
			xmlObject.setAttribute('isSelected', str(object.isSelected()))
		position = self.doc.createElement('position')
		position.setAttribute('global', str(object.getGlobalPosition()))
		x, y = object.getCentroid()
		position.setAttribute('x', str(x))
		position.setAttribute('y', str(y))
		xmlObject.appendChild(position)
		
		if object.getBB() is not None:
			box = self.doc.createElement('boundingBox')
			b = object.getBB()
			box.setAttribute('x', str(b[0]))
			box.setAttribute('y', str(b[1]))
			box.setAttribute('w', str(b[2]))
			box.setAttribute('h', str(b[3]))		
			xmlObject.appendChild(box)			
		
		shape = self.doc.createElement('shape')
		shape.setAttribute('BestResponse', object.getShape())
		shapeDist = self.doc.createElement('distribution')
		shapeDistr = object.getShapeDistribution()
		for key, prob in shapeDistr.iteritems():
			shapeDist.setAttribute(key , str(prob))
		shape.appendChild(shapeDist)
		orientation = self.doc.createElement('orientation')
		orientation.setAttribute("value" , str(object.getOrientation()))
		shape.appendChild(orientation)
		
		skewness = self.doc.createElement('skewness')
		skewness.setAttribute("horizontal" , str(object.getSkewness()))
		skewness.setAttribute('vertical', str(object.getHSkewness()))
		shape.appendChild(skewness)
		
		nbEdges = self.doc.createElement('nbEdges')
		nbEdges.setAttribute("value", str(object.getNbEdges()))
		shape.appendChild(nbEdges)
		
		xmlObject.appendChild(shape)
		
		
		colour = self.doc.createElement('colour')
		colour.setAttribute('BestResponse', object.getColour())
		
		colDist = self.doc.createElement('distribution')
		colourDistr = object.getColourDistribution()
		for col, prob in colourDistr.iteritems():
			colDist.setAttribute(str(col) , str(prob))
		colour.appendChild(colDist)
		hsvColour = self.doc.createElement('hsvValue')
		h,s,v = object.getHSV()
		hsvColour.setAttribute("H", str(h))
		hsvColour.setAttribute("S", str(s))
		hsvColour.setAttribute("V", str(v))
		colour.appendChild(hsvColour)
		
		rgbColour = self.doc.createElement('rgbValue')
		b,g,r = object.getRGB()
		rgbColour.setAttribute("R", str(r))
		rgbColour.setAttribute("G", str(g))
		rgbColour.setAttribute("B", str(b))
		colour.appendChild(rgbColour)
		
		xmlObject.appendChild(colour)
		
		return xmlObject
	
	def saveProperties(self, objects):
		"""
		Append list of objects to xml root
		Args:
			objects: detected objects
		"""
		for object in objects:
			xmlObject = self.__writeObject(object)
			self.root.appendChild(xmlObject)
		
	def update(self, timestamp, objects, occluded = None):
		"""
		Update object's properties for current timestamp
		Args:
			timestamp(MSEC): offset from the start of video
			objects:  tracked objects
			occluded: list of objects that were not detected in the
					  current scene
		"""
		ts = self.doc.createElement('timestamp')
		ts.setAttribute('time', str(timestamp))
		if config.reportRelative and config.boundingBox != None:
			boundingBox = self.doc.createElement('boundingBox')
			x, y , w, h = config.boundingBox.getBox()
			boundingBox.setAttribute("x", str(x))
			boundingBox.setAttribute("y", str(y))
			boundingBox.setAttribute("width", str(w))
			boundingBox.setAttribute("height", str(h))
			ts.appendChild(boundingBox)
		if occluded:
			occ = self.doc.createElement('occluded')
			for object in occluded:
				id = self.doc.createElement("object")
				id.setAttribute("id",str(object.getId()))
				occ.appendChild(id)
			ts.appendChild(occ)
		for object in objects:
			xmlObject = self.__writeObject(object)
			ts.appendChild(xmlObject)
		self.__append(self.filename, ts)

			
	def toPrettyXml(self):
		"""
		Add closing tag, this is done such that a long
		xml file can be constructed without having to store it
		in memory
		"""
		with open(self.filename, "a") as f:
			f.write("</file>")
	
	def __append(self, filename, ts):
		# when list gets too big dump the content to file( to avoid
		# out of memory exception
		if len(self.nodelist) == 50:
			xmlRoot = list()
			for ts in self.nodelist:				
				str = ts.toprettyxml(indent="   ", encoding = "utf-8")
				xmlRoot.append(str)
			xmlRoot = "".join(xmlRoot)
			with open(filename, "a") as f:
				f.write(xmlRoot)
			self.nodelist = []
			self.doc = Document()
			
		self.nodelist.append(ts)
		
	def writeToFile(self, filename):
		"""
		Write xml to file
		"""
		f = open(filename, "w")
		self.doc.writexml(f, "\n", "  ")
		f.close()