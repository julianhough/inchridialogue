import cv2
import os
import config
import xmlOutput
import numpy as np
import Object as o
import xmlOutput as out

class Image:
	"""
	Usage: Call findContour to detect edges and then closed contours in
	the image
	
	traverseContours: function traverses the contour hierarchy and identifies
	objects of interest(having an area ranging between minSize and maxSize)
		
	findObjects : executes the above two functions and extracts raw information
	about the detected objects
	
	extractProperties: does all the above and additionally classifies the objects
	into 8 colour classes and 12 shape ones
	"""
	def __init__(self, stream = None, static = False, file = None):
		self.image = stream
		self.xAxis = list()
		self.yAxis = list()
		self.objects = list()
		self.static = static
		self.parents = list()
		self.visited = list()
		self.filename = 'captureFrame.png'
		self.blurred = None
		self.contours = None
		self.hierarchy = None
		self.hsv = None
		if file != None:
			readFromFile(file)
			
	def getObjects(self):
		return self.objects
	
	def readFromFile(self, filename):
		self.filename = filename
		try:
			self.image = cv2.imread(filename)
		except:
			print "Could not read image"
			
	def getHSV(self):
		"""
		Colour converstion from BGR to HSV
		"""
		if self.hsv == None: # only compute this once
			self.hsv = cv2.cvtColor(self.image, cv2.COLOR_BGR2HSV)
		return self.hsv
		
	def getBlurred(self, gSize = config.ksize, sigma = config.sigma):
		"""
		Applies GaussianBlur blur to the image
		"""
		if self.blurred != None: # expensive operation, don't compute twice
			return self.blurred
			
		if self.static:
			self.blurred = cv2.medianBlur(self.image, 9)
		else:
			img  = cv2.pyrMeanShiftFiltering(self.image, 5.0, 20)
			self.blurred = cv2.GaussianBlur(img, (gSize,gSize), sigma)

		return self.blurred
		
	def __add(self, object):
		"""
		Add to the list of objects detected in the image
		"""
		self.objects.append(object)
	
	def getSize(self):
		"""
		Returns:
			image resolution, or white area resolution 
		"""
		sizeX, sizeY , channels = self.image.shape
		if config.reportRelative and config.boundingBox != None:
			x, y , sizeX, sizeY = config.boundingBox.getBox()
		return (sizeX, sizeY)
		
	def __calculateRelPos(self):
		"""
		Orders the objects on the x and y axis and computes their
		relative positions
		"""
		self.xAxis = sorted(self.objects, key=lambda obj: obj.x)
		self.yAxis = sorted(self.objects, key=lambda obj: obj.y)
		for object in self.objects:
			object.computeRelativePos()
	
	def findContour(self, lowTh = config.lowCannyTH, highTh = config.highCannyTH):
		"""
		Processes the image and find the object's contours
		
		This function starts but filtering noise and then extracts the
		edges(which are intensity changes). In order to find a closed
		contour, erosion and dilation are applied repeatdly - these
		eliminate noise after the edge detection and are meant to close 
		contours of sufficiently closed edges
		
		A hierarchy of contours is extracted from the image
		
		Args:
			lowTh : low threshold used for Canny edge detection
			highTh: high threshold used for Canny edge detection
		"""
		blur = self.getBlurred()
		edges = cv2.Canny(blur, lowTh, highTh)
		
		if not self.static:
			kernel2 = np.ones((1,1), np.uint8)
		
			cross = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
			# erode and dilate to obtain a closed contour
			roi = cv2.erode(edges, kernel2,iterations = 1)
			roi = cv2.dilate(edges,cross, iterations=4)
			
			edges = roi
		if cv2.__version__.startswith("3.0.0"):
			im, self.contours, self.hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
		else:
			self.contours, self.hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
			
		return self.contours, self.hierarchy
	
	def traverseContours(self, i = 0, level = 0, minSize = config.minSize, maxSize = config.maxSize):
		"""
		OpenCV returns all of the contours and reconstructs a full hierarchy of nested contours containing information
		about the image topology. For each i-th contour contours[i] , the elements hierarchy[i][0] , hiearchy[i][1] ,
		hiearchy[i][2] , and hiearchy[i][3] are set to 0-based indices in contours of the next and previous contours
		at the same hierarchical level, the first child contour and the parent contour, respectively. If for the contour
		i there are no next, previous, parent, or nested contours, the corresponding elements of hierarchy[i] will
		be negative.
		
		This function recursively traverses the contours finding objects in a certain size range and adds those to
		the list of segmented objects. TO NOTE: two contours are detected for each object, and inner and an outer 
		contour, only the inner contour is used to define an object
		
		In this function we are also looking for a white enclosing area that was used in the experiment to delimit
		the area where the objects were manipulated
		"""
		if self.static:
			return self.staticTraverse(i)
		
		if self.visited and (i in self.visited or i < 0):
			return
		self.visited.append(i)
		contour = self.contours[i]
		
		# go deeper in the tree
		if level < 10: # avoid too deep recursion exception
			self.traverseContours(self.hierarchy[0][i][2], level + 1,  minSize, maxSize)
		if config.reportRelative or config.perspectiveTransform:
			if cv2.contourArea(contour) > config.boundingBoxArea and config.boundingBox == None :
				config.boundingBox = o.Object(-1, self, contour, False)
				config.boundingBox.extractProperties()
				
		if (cv2.contourArea(contour) > minSize and cv2.contourArea(contour) < maxSize # we are only interested in objects in a certain range
			and self.hierarchy[0][i][3] != -1):	# remember we want just the inner contour, so a contour that has a parent
			
			# make sure we are not adding any of the parent's of this object
			# to avoid including the same object twice, by its inner and outer
			# contour
			if i not in self.parents:
				self.__add(o.Object(len(self.objects), self, contour, False)) 
			self.parents.append(self.hierarchy[0][i][3])
			
		# traverse nodes at the same level as current node
		self.traverseContours(self.hierarchy[0][i][0], level, minSize, maxSize)
		self.traverseContours(self.hierarchy[0][i][1], level, minSize, maxSize)	
	
	def staticTraverse(self, i = 0, minSize = config.staticMinSize):
		"""
		Same as traverseContours but used for computer generated images
		The difference here is that we care about the outer contour not the inner contour
		and we are also looking for selected pieces( as present in Take corpus)
		"""
		selected = False
		if self.visited and (i in self.visited or i < 0):
			return
		self.visited.append(i)
		contour = self.contours[i]
		
		# a selected piece is one with more than one inner contour
		if self.hierarchy[0][self.hierarchy[0][i][2]][2] != -1:
		
			if cv2.contourArea(self.contours[self.hierarchy[0][self.hierarchy[0][i][2]][2]]) > 300:
				contour =  self.contours[self.hierarchy[0][self.hierarchy[0][i][2]][2]]
			else:
				contour = self.contours[self.hierarchy[0][i][2]]
			selected = True
		
		if cv2.contourArea(contour) > minSize: 
			self.__add(o.Object(len(self.objects), self, contour, selected))
	
		self.staticTraverse(self.hierarchy[0][i][0], minSize = minSize)
		self.staticTraverse(self.hierarchy[0][i][1], minSize = minSize)	
		
	def findObjects(self, minSize = config.minSize, maxSize = config.maxSize):
		"""
		Finds objects in an image and extracts raw information
		Args:
			minSize(int) : minimum object area
			maxSize(int) : maximium object area
		"""
		if self.contours == None:
			self.findContour()
			
		self.traverseContours(0,0, minSize, maxSize)
		for object in self.objects:
			object.extractProperties()

		return self.objects

			
	def processObjects(self, minSize = config.minSize, maxSize = config.maxSize):
		"""
		Find and classify objects in the image
		Args:
			minSize(int) : minimum object area
			maxSize(int) : maximium object area
		"""
		self.findObjects(minSize = minSize, maxSize = maxSize)
		for object in self.objects:
			object.classify()
				
		# self.calculateRelPos()
			
	def saveToXML(self, saveDir, filename = None):
		"""
		Save the object's properties as identified
		in this image to xml
		Args:
			saveDir(str) : location to be saved
			filename(str): name of the xml file
		"""
		if filename == None:
			# if filename not specified save under 'image name'.xml
			dirs = os.path.split(self.filename)
			filename = str.split(dirs[-1],".")[0] + '.xml'
		
		xml = out.XmlOutput(filename)
		xml.saveProperties(self.objects)
		xmlName = os.path.join(saveDir, filename)
		xml.writeToFile(xmlName)
	
	def getContours(self):	
		"""
		Returns the list of contours
		"""
		return self.contours
		
	def drawRectangles(self):
		"""
		Debugging drawing function, draws bouding rectangles
		and adds tags with classification responses for each
		object in the image
		"""
		img = self.image
		font_size = 1
		if self.static:
			font_size = 2
		for object in self.objects:
			x,y,w,h = object.getBox()
			cv2.rectangle(img,(x,y),(w,h),(0,255,0),2)
			cv2.drawContours(img, object.getContour(), -1, (0,255,0))
			desc = str(object.getId()) + " "
			if object.getColour():
				desc += str(object.getColour())
			cv2.putText(img,desc,(x,y), cv2.FONT_HERSHEY_PLAIN, font_size, (0, 0, 255))
			desc = ""
			if object.getShape():
				desc += str(object.getShape())
			if object.isSelected():
				desc += 'SELECTED'
			cv2.putText(img,desc,(x+50,y + 15), cv2.FONT_HERSHEY_PLAIN, font_size, (0, 0, 255))
				
		cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
		return img
	
	def display(self):
		"""
		Print object's properties
		"""
		for object in self.objects:
			object.printProps()
		
	def getImage(self):
		return self.image
		
	def getObjects(self):
		return self.objects
		
	def getXAxis(self):
		return self.xAxis
	
	def getYAxis(self):
		return self.yAxis

	def getPerspectiveTransformMatrix(self, boundingBox, maxWidth=None, maxHeight=None):
		
		if not boundingBox.edges().shape == (4,1,2):
			print "ERROR: White background corners or edges not all in view of camera!"
			raise IOError
			
			
		pts = boundingBox.edges().reshape(4, 2)
		rect = np.zeros((4, 2), dtype = "float32")
		 
		# the top-left point has the smallest sum whereas the
		# bottom-right has the largest sum
		s = pts.sum(axis = 1)
		rect[0] = pts[np.argmin(s)]
		rect[2] = pts[np.argmax(s)]
		 
		# compute the difference between the points -- the top-right
		# will have the minumum difference and the bottom-left will
		# have the maximum difference
		diff = np.diff(pts, axis = 1)
		rect[1] = pts[np.argmin(diff)]
		rect[3] = pts[np.argmax(diff)]
		
		print rect
		# multiply the rectangle by the original ratio
		# rect *= ratio
		
		(tl, tr, br, bl) = rect
		if maxWidth==None and maxWidth==None:
			widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[0] - bl[0]) ** 2))
			widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[0] - tl[0]) ** 2))

			# ...and now for the height of our new image
			heightA = np.sqrt(((tr[1] - br[1]) ** 2) + ((tr[1] - br[1]) ** 2))
			heightB = np.sqrt(((tl[1] - bl[1]) ** 2) + ((tl[1] - bl[1]) ** 2))
			 
			# take the maximum of the width and height values to reach
			# our final dimensions
			maxWidth = max(int(widthA), int(widthB))
			maxHeight = max(int(heightA), int(heightB))
		 
		# construct our destination points which will be used to
		# map the screen to a top-down, "birds eye" view
		dst = np.array([
			[0, 0],
			[maxWidth - 1, 0],
			[maxWidth - 1, maxHeight - 1],
			[0, maxHeight - 1]], dtype = "float32")
		print dst
		# calculate the perspective transform matrix and warp
		# the perspective to grab the screen
		M = cv2.getPerspectiveTransform(rect, dst)
		print M
		def dest(x,y):
			newx = (M[0][0]*x + M[0][1]*y + M[0][2])/(M[2][0]*x + M[2][1]*y + M[2][2])
			newy = (M[1][0]*x + M[1][1]*y + M[1][2])/(M[2][0]*x + M[2][1]*y + M[2][2])
			return [newx,newy]
		print "tl mapping", dest(tl[0],tl[1])
		print "br mapping", dest(br[0],br[1])
		return M, maxWidth, maxHeight
		
	def perspectiveTrasform(self):
		self.findContour()
		self.traverseContours(0)	
		if config.boundingBox != None:
			M, maxWidth, maxHeight = self.getPerspectiveTransformMatrix(config.boundingBox)
			warp = cv2.warpPerspective(self.image, M, (maxWidth, maxHeight))
			self.image = warp
		else:
			print "Could not find boundingRectangle"
			return None
		return warp
		
