import cv2 
import os 
import numpy as np
import pickle
import math
import random
from Object import *
from Image import Image
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
import config
import enum

class ProbModel():

	def __init__(self, dataFolder = None, classify = "shape", featuresDir = "../features/pento", static = False):
		"""
		Abstract class for different classifiers
		Provides methods for computing features, loading features/responses files and training and classification
		Args:
			dataFolder(str) : Folder where new examples are located, if we want to recompute the samples
			classify ("shape"|"colour") : Select which features to load, the colour or the shape ones
			featuresDir(str) : Folder where to load/save features to/from
			static   (bool)  : Specify whether we are classifying computer generated images of images coming	
							   from the RGB camera
		Raises:
			Exception "Provided folder doesn't exist" (for dataFolder or featuresDir)
			
		Usage : train    - after __init__ when features were loaded or computed, train the classifier
				classify - predict colour or shape class 
		"""
		self.model = None
		self.labelsDict = dict()
		self.static = static
		self.classify = classify
		
		# get the location of feature/responses files
		if not os.path.exists(featuresDir):
			if dataFolder != None:
				os.mkdir(featuresDir)
			else:
				raise Exception("Provided path doesn't exist")
		self.featureFile = os.path.join (featuresDir, classify + "-samples.data")
		self.responsesFile  = os.path.join (featuresDir, classify + "-responses.data")
		self.dictFile = os.path.join (featuresDir, classify + "labelDictionary.data")
		print self.dictFile
		
		# if we have new examples that we want to extract features for then process these
		# first, otherwise load precomputed feature training vectors
		if not dataFolder:
			self.features, self.labels = self.load()
		else:
			self.features, self.labels = self.process(dataFolder)
			self.save()
	
	def load(self):
		"""
		Loads precomputed features and example's labels
		Returns:
			features(2-dim float vector) : array with features
			responses(1-dim float vector) : labels for each sample in the features vector
		"""
		if not os.path.exists(self.featureFile):
			raise Exception("Could not find features file")
		features = np.loadtxt(self.featureFile, np.float32)
		if not os.path.exists(self.responsesFile):
			raise Exception("Could not find responses file")
		responses = np.loadtxt(self.responsesFile, np.float32)
		responses = responses.reshape((responses.size,1))
		if not os.path.exists(self.dictFile):
			raise Exception("Could not find labels file")
		f = open(self.dictFile, "r")
		self.labelsDict = pickle.load(f)
		f.close()
		return features, responses
		
	def save(self):
		"""
		Saves features for later use
		"""
		np.savetxt(self.featureFile, self.features)
		self.lables = np.array(self.labels, np.float32)
		np.savetxt(self.responsesFile, self.labels)
		f = open(self.dictFile, "w")
		pickle.dump(self.labelsDict, f)
		f.close()
		
	
	def process(self, dataFoler, static = False):
		"""
		Load the examples from dataFolder and compute the feature 
		vectors for them 
		Args:
			dataFolder(str) : location of the training examples
			static (bool)   : wether we are processing computer generated
							  images or data coming from the camera
		Raises:
			Exception : Provided folder doesn't exist
		"""
		if not os.path.exists(dataFoler):
			raise Exception ("Provided folder doesn't exist")
		features = list()
		labels = list()
		id = 0
		
		for subdir, dirs, files in os.walk(dataFoler):
			# the names of the folders are also the class labels
			label = os.path.split(subdir)[-1]
			if config.verbose >= 1:
				print "Processing " + label + " examples"
			for file in files:
				
				filename = os.path.join(subdir ,file)
			
				if config.verbose > 1 :
					print "file : " + filename
				img = Image(static = self.static)
				img.readFromFile(filename)
		
				img.findObjects(minSize = 500, maxSize= 50000)
				objects = img.getObjects()
		
				if len(objects) > 0:
					maxobject = objects[0]
					area = maxobject.getArea()
					
					for object in objects:
						if object.getArea() > area:
							maxobject = object
							area = object.getArea()
					object = maxobject
					# object = Object(1, img, extContour)
					
					if self.classify == "shape":
						object.calcShapeProp()
						features.append(object.getFeatures())
					else:
						features.append(object.calcHSV())
					
					if not label in self.labelsDict.itervalues():
						self.labelsDict[id] = label
						id +=1
					currentId = [name for name,key in self.labelsDict.iteritems() if key == label]
					
					labels.append(currentId)

		return features, labels

			
class ShapeMatcher(ProbModel):
	def __init__(self, dataFolder = None):
		ProbModel.__init__(self, folder)
		
	def train(self, templateFolder):
		templateContours = dict();

		if os.path.exists(templateFolder):
			for subdir, dirs, files in os.walk(templateFolder):
				
				for file in files:
					filename = os.path.join(subdir ,file)
					label = str(file).split(".")[0]
					desc = findContour(filename)
					templateContours[label]=desc[0]
					
		else:
			"Could not find examples at " + examplesFolder
		self.save("model.obj")
		
	def predict(self, query):
		
		
		prob = dict()
		for query in sample:
			for key in self.model:
				ret = cv2.matchShapes(query, self.model[key], 2,0.0)
				print str(key) + " " + str(ret)
				prob[key] = ret
		
		print min(prob, key=prob.get)
		
class Knn(ProbModel):

	def __init__(self, dataFolder = None, classify = "shape", featuresDir = "../features/pento", static = False):
		ProbModel.__init__(self, dataFolder, classify, featuresDir = featuresDir, static = static)
		
	def train(self):
	
		self.model = cv2.KNearest()
		self.model.train(np.float32(self.features), np.float32(self.labels))
		
		
	def predict(self, sample, k = 5):
		samples = list()
		samples.append(sample)

		retval, results, neigh_resp, dists = self.model.find_nearest(np.float32(samples), k)

		# probs = list()
		
		# for j in range(0, len(neigh_resp)):
		counts = neigh_resp[0].tolist()
		prob = dict()
		if min(dists[0]) > 0.01 and self.classify == "shape":
			prob['unknown'] = 1
		else:
			for i in range(0,len(self.labelsDict)):
				prob[self.labelsDict[i]] = float(counts.count(i)) / k	
			if max(prob, key=prob.get) != self.labelsDict[results[0][0]]:
				prob[self.labelsDict[results[0][0]]] = prob[self.labelsDict[results[0][0]]] + 0.001

		return prob
		
class NB(ProbModel):
	def __init__(self, dataFolder = None, classify = "shape", featuresDir = "../features/pento", static = False):
		ProbModel.__init__(self, dataFolder, classify,  featuresDir = featuresDir, static = static)
		
	def train(self):
		
		self.model = MultinomialNB()
		self.model.fit(np.array(self.features), np.array(self.labels).ravel())
	
	def predict(self, sample):
		
		results = self.model.predict_proba(np.transpose(sample))
		prob = dict()
		for i in range(0,len(results[0])):
			prob[self.labelsDict[i]] = results[0][i]
		
		return prob
		
		# results = self.model.predict(np.float32([sample]))

		# prob = dict()
		# for i in range(0,len(self.labelsDict)):
			# prob[self.labelsDict[i]] = 0
		# prob[self.labelsDict[results[0]]] = 1
		
		# return prob

		
class MNL(ProbModel):
	def __init__(self, dataFolder = None, classify = "shape", featuresDir = "../features/pento", static = False):
		ProbModel.__init__(self, dataFolder, classify,  featuresDir = featuresDir, static = static)
		
	def train(self):
		
		self.model = LogisticRegression(C=1.0)
		self.model.fit(np.array(self.features), np.array(self.labels).ravel())
	
	def predict(self, sample):
		
		results = self.model.predict_proba(np.transpose(sample))
		prob = dict()
		for i in range(0,len(results[0])):
			prob[self.labelsDict[i]] = results[0][i]
		
		return prob

class SVM(ProbModel):
	def __init__(self, dataFolder = None, classify = "shape", featuresDir = "../features/pento", static = False):
		ProbModel.__init__(self, dataFolder, classify, featuresDir = featuresDir, static = static)
		
	def train(self):
		
		self.model = SVC(probability = True)
		self.model.fit(np.array(self.features), np.array(self.labels).ravel())
	
	def predict(self, sample):
		
		results = self.model.predict_proba(np.transpose(sample))
		prob = dict()
		for i in range(0,len(results[0])):
			prob[self.labelsDict[i]] = results[0][i]
		
		return prob
		
# class SVM(ProbModel):
	# def __init__(self, folder = None, classify = "shape"):
		# ProbModel.__init__(self, folder, classify)
		
	# def train(self):

		# self.model = cv2.SVM()
		# self.model.train(np.float32(self.features), np.float32(self.labels))
		
	# def predict(self, sample):

		# prob = dict()
		# result = self.model.predict(np.float32(sample))
		# for i in range(0,len(self.labelsDict)):
			# prob[self.labelsDict[i]] = 0
		# prob[self.labelsDict[result]] = 1
		
		# return prob
		
class ShapeDistance(ProbModel):

	def __init__(self, dataFolder=None, featuresDir ="../features/pento", static = False):
		ProbModel.__init__(self, dataFolder, featuresDir = featuresDir, static = static)
		self.featureVectors = dict()
	
	def dist(self,a,b):   

		return np.sqrt(sum((x-y)**2 for x,y in zip(a, b)))
	
	def train(self):
		features , labels = ProbModel.train(self, templateFolder)

		for i in range(0,len(labels)):
			for label, key in self.labelsDict.iteritems():
				if key == labels[i]:
					self.featureVectors[str(label)] = features[i]
	
	def predict(self, sample):

		distance = dict()
		for key, shape in self.featureVectors.iteritems():
			distance[key] = self.dist(shape,np.array(sample))
		return distance
		
class Colour(enum.Enum):
	BLACK 	= "Black"
	WHITE 	= "White"
	GRAY  	= "Gray"
	RED     = "Red"
	YELLOW  = "Yellow"
	GREEN   = "Green"
	CYAN    = "Cyan"
	BLUE 	= "Blue"
	MAGENTA = "Magenta"
	PURPLE  = "Purple"
	ORANGE 	= "Orange"
	BROWN 	= "BROWN"
	PINK	= "PINK"

class ThreshColour(ProbModel):
	def __init__(self, dataFolder = None):
		ProbModel.__init__(self, dataFolder, classify = "colour")
		
	def train(self):
		return
	
	def predict(self, list):

		hue = list[0]
		sat = list[1]
		light = list[2]
		prob = dict()
		for key in self.labelsDict.itervalues():
			prob[key] = 0
		if light < 60:
			prob[Colour.BLACK.value] = 1
		elif light > 200 and sat < 30:
			prob[Colour.WHITE.value]= 1
		
		elif sat < 60:
			prob[Colour.GRAY.value] = 1

		elif hue < 25: 
			if light < 130:
				prob[Colour.BROWN.value] = 1
			prob[Colour.RED.value] = 1
		elif hue < 40:
			if light < 120:
				prob[Colour.BROWN.value] = 1
			prob[Colour.ORANGE.value] = 1
		elif hue < 70:
			prob[Colour.YELLOW.value] = 1
		elif hue < 150:
			prob[Colour.GREEN.value] = 1
		elif hue < 200:
			prob[Colour.CYAN.value] = 1
		elif hue < 270:
			if sat < 120:
				prob[Colour.PURPLE.value] = 1
			prob[Colour.BLUE.value] = 1
		elif hue < 330:
			if sat < 120:
				prob[Colour.PINK.value] = 1
			else:
				prob[Colour.MAGENTA.value] = 1
		else:
			prob[Colour.RED.value]  = 1
			
		return prob
