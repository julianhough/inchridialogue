import cv2
import Image as im
import PieceTracker as pt
import xmlOutput as out
import rsbOutput as rsb
import config
import os
import traceback
import time

current_milli_time = lambda: int(round(time.time() * 1000))
	
class Video():
	"""
	Class used to process frames from a camera or a movie file
	Abstract from the opencv VideoCapture class, offering 
	recording, processing(object detection and tracking) as 
	well as snapshot functionality 
	
	"""
	
	def __init__(self, source = 0):
		"""
		Args:
			source(str): the location of the video file to be processed
			default value is 0, meaning process from camera
		"""
		self.capture = cv2.VideoCapture(source)
		self.filename = source
		self.scale_set = False
		if source == 0:
			self.fromcamera = True
		else:
			self.fromcamera = False

		if not self.fromcamera:
			if cv2.__version__.startswith("3.0.0"):
				self.fps = self.capture.get(cv2.CAP_PROP_FPS)
				self.width = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
				self.height = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))
				self.fourcc = self.capture.get(cv2.CAP_PROP_FOURCC)
				self.pos_msec = cv2.CAP_PROP_POS_MSEC
				self.frame_num = cv2.CAP_PROP_POS_FRAMES
				self.frameCount = self.capture.get(cv2.CAP_PROP_FRAME_COUNT)
			else:
				self.fps = self.capture.get(cv2.cv.CV_CAP_PROP_FPS)
				self.width = int(self.capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
				self.height = int(self.capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
				self.pos_msec = cv2.cv.CV_CAP_PROP_POS_MSEC
				self.frame_num = cv2.cv.CV_CAP_PROP_POS_FRAMES
				self.frameCount = self.capture.get(cv2.cv.CV_CAP_PROP_FRAME_COUNT)
				# self.fourcc = self.capture.get(cv2.cv.CAP_PROP_FOURCC)
		else:
			#self.fps = 30
			self.fps = 10
			self.width = int(self.capture.get(cv2.cv.CV_CAP_PROP_FRAME_WIDTH))
			self.height = int(self.capture.get(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT))
			self.fourcc = cv2.cv.CV_FOURCC('F', 'M', 'P', '4')
			
	def initialiseWriter(self, filename = "output.avi", codec = -1, skip = config.fps):
		self.writer = cv2.VideoWriter()
		fps = self.fps / skip
		#codec = cv2.cv.CV_FOURCC('P','I','M','1')
		codec = self.fourcc
		return self.writer.open(filename, codec, fps , (self.width,self.height), True)
		
	def __frameCountToTime(self, frameCount):

		millis = (frameCount/ self.fps) *1000
		min = (int)(millis/60000)
		millis -= min*60000
		sec = int(millis/1000)
		millis -= sec*1000
		timeStr = str(min) + ":" + str(sec) + "."+ str(int(millis))
		return timeStr
		
	def duration(self):
		"""
		Returns time length of video in milliseconds.
		Return - int - Time length [ms].
		"""
		
		return self.fps * self.frameCount
		
	def record(self, filename = "output.avi"):
		"""
		Simply read captures from the camera and outputs them to a file
		Args:
			filename(str): Name of the output file
			start(float): Video offset specifying the initial frame to be processed(in millis)
			stop (float): Specifies the last frame to be processed (in millis)
		"""		
		self.initialiseWriter(filename)
		
		while True:
			if self.capture is None or not self.capture.isOpened():
				print 'Warning: unable to open video source: '
				break
			ret, frame = self.capture.read()
			if not ret:
				print "Could not read capture"
				break
			cv2.imshow('frame', frame)				
			self.writer.write(frame)
			if cv2.waitKey(1) & 0xFF == ord('q'):
				break
		
		self.writer.release()
		self.capture.release()
		cv2.destroyAllWindows()

	def on_mouse(self, event, x, y, flags, params):
		"""
		Mouse event handler. Publishes the x,y position of a click on the video via RSB.
		"""
		if event == cv2.EVENT_LBUTTONDBLCLK:
			print 'Mouse click'
			if config.rsb:
				self.rsb.publish_coordinates(x,y)

		
	def run(self, start = 0, stop = -1, fps = config.fps):
		
		""" 
		Iteratively processes the video frame by frame
		
		It starts by identifying the initial objects to be tracked
		and registers those with the PieceTracker module
		It then processes subsequent frames by detecting object's
		contours and passing those to the PieceTracker to do the matching
		
		Args:
			start(float): Video offset specifying the initial frame to be processed(in millis)
			stop (float): Specifies the last frame to be processed (in millis)
			fps (int) : Number of frames per second to be processed

		"""

		self.start = start
		if not self.fromcamera:
			print "Not reading from camera"
			self.stop = self.duration() if self.duration() < stop or stop == -1 else stop
		self.stop = stop
		self.tracker = pt.PieceTracker()
		if self.start != 0 and not self.fromcamera and not config.bufferEmpty:
			if self.start < self.stop:
				self.capture.set(self.pos_msec, self.start)
			else:
				raise Exception("Given start frame is greater than the end of video")
		
		skip = int (self.fps / fps)

		if config.rsb:
			self.rsb = rsb.RsbOutput()
		if config.saveToXML:
			self.xml = out.XmlOutput(self.filename)
		if config.writeToVideo:
			self.videoDir = "/home/dsg-labuser/Desktop/hri/raw_data/video/{0}".format(1)
			if not os.path.exists(self.videoDir): 
				os.makedirs(self.videoDir)
		#if config.writeToVideo:
		#	self.initialiseWriter(str(self.filename)+".avi", skip = skip)
		writingToVideo = False
		self.classifier = config.classifier
		frameTimeStamp = start
		
		try:
			while (frameTimeStamp < self.stop):
			
				if self.capture is None or not self.capture.isOpened():
					print 'Warning: unable to open video source: '
					break
				ret, self.frame = self.capture.read()
				
				if not ret:
					print "Could not read capture"
					break
				
				if not self.fromcamera:
					frameTimeStamp = self.capture.get(self.pos_msec)
					frames = self.capture.get(self.frame_num)
					if frames % skip != 0:
						continue
				else:
					frameTimeStamp += 1
					
				if (frameTimeStamp < start):
					cv2.imshow('frame', self.frame)
					if config.writeToVideo:
						self.writer.write(self.frame)	
					
					if cv2.waitKey(1) & 0xFF == ord('q'):
						if config.verbose > 1:
							cv2.imwrite('captureFrame.png', frame)
							for piece in tracked:
								piece.printProps()
						break
					continue
						
				if config.verbose > 1:
					print "Timestamp: " + str(frameTimeStamp) + " millis"
					# self.frame = frame.copy()
				
				
				if self.tracker.reset or (cv2.waitKey(1) & 0xFF == ord('r')): #only called if the rsb handler has received a reset
					#reinitialize the piece tracker
					self.tracker = pt.PieceTracker()
				
				img = im.Image(self.frame)
				
				self.tracker.setFrame(img.getBlurred())
				frame = self.frame.copy()
				

				if config.perspectiveTransform:
					wrap = img.perspectiveTrasform()
					img = im.Image(wrap)
					config.boundingBox = None
				img.processObjects()
				
				
					 				
				if len(self.tracker.tracked()) == 0: #only called the first time	
					# initial detection of the objects to be tracked
					for object in img.getObjects():
						# do not add objects that are not what we are looking for
						# (ie their shape is too different from the training examples)
						if object.getShape() != 'unknown':
							self.tracker.add(object)
				
				if config.boundingBox!=None: #TODO need to redo this so it waits for it if not there
					if self.scale_set == False:
						bbx, bby , bbsizeX, bbsizeY = config.boundingBox.getBox() #simple version
						self.rsb.set_boundingbox_scale(bbx,bby,bbsizeX,bbsizeY) #simple version
						try:
							bbox_transform_matrix = img.getPerspectiveTransformMatrix(config.boundingBox,
														  maxWidth=self.rsb.pentorob_size[0], 
														  maxHeight=self.rsb.pentorob_size[1])
							self.rsb.set_boundingbox_perspective(bbox_transform_matrix[0])
							
							self.scale_set = True
						except IOError:
							print "Adjust camera!"
				else: 
					print "Please make sure white box is in view of camera!"
					
				tracked = self.tracker.track(img.getObjects())
				
				if config.writeToVideo:
					if not writingToVideo:
						if self.tracker.recordingName:
							self.initialiseWriter(self.videoDir + "/" + str(self.tracker.recordingName)+":"+str(current_milli_time())+".avi", skip = skip)
							writingToVideo = True
					if writingToVideo:
						self.writer.write(img.getImage()) #without rectangles
						#self.writer.write(self.frame)	 #with rectangles
						if not self.tracker.recordingName: #this has been cancelled
							writingToVideo = False
							self.writer.release()
						
				self.frame = self.tracker.drawRectangles(img.getImage())
 				cv2.drawContours(self.frame, img.getContours(), -1, (255,0,0), 1)
				#Send objects to InproTK				
				if config.rsb and config.boundingBox: #only start sending info once bounding box is there
					self.rsb.publish_objects(tracked)
				if config.saveToXML:
					self.xml.update(frameTimeStamp, tracked, self.tracker.getOccluded())
				cv2.imshow('frame', self.frame)
				cv2.setMouseCallback('frame', self.on_mouse)
				
				 
				
				x = cv2.waitKey(1) & 0xFF
				if x != 0xFF:
					print x
					self.rsb.publish_commands(x)
					if x == ord('q'):
						if config.verbose > 1:
							cv2.imwrite('captureFrame.png', frame)
							for piece in tracked:
								piece.printProps()
						break
					if x == ord('r'): #only called if the rsb handler has received a reset
						#reinitialize the piece tracker
						print "reseting tracker"
						self.tracker = pt.PieceTracker()
					
			if config.saveToXML:
				self.xml.toPrettyXml()
			# Release everything if job is finished
			self.capture.release()
			if config.writeToVideo:
				self.writer.release()
			cv2.destroyAllWindows()
			self.tracker.server.deactivate()
			self.rsb.object_informer.deactivate()
			self.rsb.informer.deactivate()
			print "shutting down"
		except:
			traceback.print_exc()
			if config.saveToXML:
				self.xml.toPrettyXml()
			# Release everything if job is finished
			self.capture.release()
			if config.writeToVideo:
				self.writer.release()
			cv2.destroyAllWindows()
			self.tracker.server.deactivate() #stop rsb
			self.rsb.informer.deactivate()
			self.rsb.object_informer.deactivate()
	
	def snapshot(self, shape, filename = None , storeinFolder = "../database"):
		
		if not os.path.exists(storeinFolder):
			os.mkdir(storeinFolder)
		location = os.path.join(storeinFolder, shape)
		if not os.path.exists(location):
			os.mkdir(location)
		
		ret, frame = self.capture.read()
		id = 1
		
		if not filename:
			filename = str(id) + ".png"
			while(os.path.exists(os.path.join(location, filename))):
				id +=1
				filename = str(id) + ".png"
		else:
			filename = filename + ".png"
		cv2.imwrite(os.path.join(location, filename), frame)
			
	def collectTrainingData(self, shape, trainFolder = '../trainingData/shape'):
		
		if not os.path.exists(trainFolder):
			os.mkdir(trainFolder)
		location = os.path.join(trainFolder, shape)
		if not os.path.exists(location):
			os.mkdir(location)
		id = 1

		while True:
			ret, frame = self.capture.read()
			cv2.imshow('frame', frame)
			k = cv2.waitKey(1) 
			if k == 113:
				break
			elif k == 32:
				filename = str(id) + ".png"
				while(os.path.exists(os.path.join(location, filename))):
					id +=1
					filename = str(id) + ".png"
				cv2.imwrite(os.path.join(location, filename), frame)
