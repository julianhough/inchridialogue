import cv2 
import os
import sys
import Image as im
import argparse
import ProbModel as pm
import Video as vid
import config
from matplotlib import pyplot as plt

def main():
	
	parser = argparse.ArgumentParser()
	parser.add_argument('-video', nargs=1, help="Process video from source, if none specified input from camera will be processed", type=int, default = None)
	parser.add_argument('-start', nargs="?", default = 0, type=float, 
						help='Start frame for the video processing')
	parser.add_argument('-stop', nargs="?", default = sys.float_info.max, type=float,
						help='Stop frame for the video processing')
	parser.add_argument('-processImage', nargs=1,
						help='Filename of the image to be processed')
	parser.add_argument('-processFolder', nargs=1,
						help='Process entire folder of images')
	parser.add_argument('-processFolderV', nargs=1,
						help='Process the entire folder of videos')
	parser.add_argument('-classifier', nargs="+", default=["knn"], 
					   help="Classifier for shape recognition. Second argument is optional, specifies a location for the shape features file")
	parser.add_argument('-colourClassifier', nargs="*", default=["mnl"] , 
					   help='Classifier for colour recognition. Second argument is optional, specifies a location for the colour features file')
	parser.add_argument('-newData', nargs="?", default=None, help="Extract features for shape, argument is location of the new examples")
	parser.add_argument('-newDataC', nargs="?", default=None, help="Extract features for colour, argument is location of the new examples")
	parser.add_argument('-static', action="store_true", help="Enable for computer generated images")
	parser.add_argument('-writeVideo', action="store_true", default = False, help="Process static images")
	parser.add_argument('-calibrationPos', action="store_true", default = True, help="Report x,y centroid coordinates relative to the enclosing bounding box( this is useful for calibration with Kinect data)")
	parser.add_argument('-record', action="store_true", help="Record a video sequence")
	parser.add_argument('-saveToXML', action="store_true", default = False, help="Save the object descriptions in XML format")
	parser.add_argument('-collectData', nargs=1,
						help='Collect image data for specified shape default location for storing is ../data/shape')
	parser.add_argument('-saveTo', nargs=1, default = "captureFrame.png", type = str, 
					   help='save snapshot image under this name')
	parser.add_argument('-snapshot', nargs=1,
						help='Collect image data for specified shape default location for storing is dirpath\examples')
	parser.add_argument('-perspectiveTransform', action="store_true", default = False, help="Adjust for any perspective transformations")
	parser.add_argument('-bufferEmpty', action="store_true", default = False, help="Buffer empty frames for videos to match the length of the original video")
	parser.add_argument('-v','--verbose', action='count', default=0, help="Verbosity level")
	parser.add_argument('-rsb', action='store_true', help='Publish to RSB')
	args = parser.parse_args()
	
	samplesFolder = None
	config.static = args.static
	samplesFolder = args.newData
	config.reportRelative = args.calibrationPos
	config.writeToVideo = args.writeVideo
	config.saveToXML = args.saveToXML
	config.bufferEmpty = args.bufferEmpty
	config.perspectiveTransform = args.perspectiveTransform
	config.verbose = args.verbose
	config.rsb = args.rsb
	
	if len(args.classifier) <= 1:
		if config.static:
			featuresDir = "../features/generatedPento"
		else:
			featuresDir  = "../features/pento"
	else:
		featuresDir = args.classifier[1]

	if args.collectData:
		shape = args.collectData[0]
		print "Collecting data for " + shape
		video = vid.Video()
		video.collectTrainingData(shape)
		
	if args.record:
		print "Recording video ... "
		video = vid.Video(args.video)
		video.record()
		
	if args.snapshot:
		shape = args.snapshot[0]
		print "Collecting data for " + shape
		video = vid.Video()
		if args.saveTo:
			video.snapshot(shape, args.saveTo[0])
		else:
			video.snapshot(shape)
	
	if args.classifier[0] == 'knn':
		classifier = pm.Knn(samplesFolder, featuresDir = featuresDir ,static = args.static)
	elif args.classifier[0]  == 'distance':
		classifier  = pm.ShapeDistance(static = args.static, featuresDir = featuresDir )
	elif args.classifier[0]  == 'svm':
		classifier  = pm.SVM(samplesFolder, featuresDir = featuresDir ,static = args.static)
	elif args.classifier[0]  == 'mnl':
		classifier  = pm.MNL(samplesFolder, featuresDir = featuresDir ,static = args.static)
	elif args.classifier[0]  == 'nb':
		classifier  = pm.NB(samplesFolder, featuresDir = featuresDir ,static = args.static)
		
	classifier.train()
	config.classifier = classifier	
	
	
	if len(args.colourClassifier) > 1:
		featuresDir = args.colourClassifier[1]
	else:
		if config.static:
			featuresDir = "../features/generatedPento"
		else:
			featuresDir  = "../features/pento"
	
	# Use this classifier for colours, default returns values using hard-coded threshold values

	if args.colourClassifier[0] == 'knn':
		colourClassifier = pm.Knn(dataFolder = args.newDataC, classify = "colour", featuresDir = featuresDir ,static = args.static)
	elif args.colourClassifier == 'svm':
		colourClassifier[0] = pm.SVM(dataFolder = args.newDataC, classify = "colour", featuresDir = featuresDir ,static = args.static)
	elif args.colourClassifier[0] == "mnl":
		colourClassifier = pm.MNL(dataFolder = args.newDataC, classify = "colour", featuresDir = featuresDir ,static = args.static)	
	elif args.colourClassifier[0] == "nb":
		colourClassifier = pm.NB(dataFolder = args.newDataC, classify = "colour", featuresDir = featuresDir ,static = args.static)
	else:
		colourClassifier = pm.MNL(classify = "colour")
	
	colourClassifier.train()
	config.colourClassifier = colourClassifier
	
	start = args.start
	stop = args.stop

	if args.video:
		#try:
		print "Processing image from video..."
		source = args.video[0]
		if args.video[0] == '0':
			source = int(0)
		video = vid.Video(source)
		video.run(start, stop)
		
	if args.processImage:

		img  = im.Image(static=args.static)
		img.readFromFile(args.processImage[0])
		
		if args.perspectiveTransform:
				warp = img.perspectiveTrasform()
				cv2.imwrite('flatperspective.png', warp)
				img = im.Image(warp)
				config.boundingBox = None
			
		img.processObjects()
		image = cv2.cvtColor(img.drawRectangles(), cv2.COLOR_BGR2RGB)	
		plt.imshow(image)
		plt.show()
		cv2.imwrite(args.saveTo[0], image)
		if config.verbose >=1:
			img.display()
		if args.saveToXML:
			img.saveToXML(".")
			
	if args.processFolderV:
		if os.path.exists(args.processFolderV[0]):
				
				for subdir, dirs, files in os.walk(args.processFolderV[0]):
					
					for file in files:
						config.boundingBox = None
						filename = os.path.join(subdir ,file)
						video = vid.Video(filename)
						video.run(start, stop)
						
	if args.processFolder:
		if os.path.exists(args.processFolder[0]):
			
			for subdir, dirs, files in os.walk(args.processFolder[0]):
				
				for file in files:
					filename = os.path.join(subdir ,file)
					if os.path.isfile(filename) and str.endswith(filename,".png"):
						if config.verbose >= 1:
							print filename
						label = os.path.split(subdir)[-1]
						#try:
						img = im.Image(static=args.static)
						img.readFromFile(filename)
						img.processObjects()
						
						if args.saveToXML:
							img.saveToXML(subdir)

							

if __name__ == "__main__":
	main()