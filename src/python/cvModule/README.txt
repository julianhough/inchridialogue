==================
Installation 
=================

Dependencies

OpenCV Python library, either 2.4.9 or 3.0 Alpha version. This can be downloaded from http://opencv.org/downloads.html. 

The ffmpeg support needs to be enabled for processing mp4 videos, this is not in the default build. 

For Linux: http://docs.opencv.org/doc/tutorials/introduction/linux_install/linux_install.html
		   http://www.sysads.co.uk/2014/05/install-opencv-2-4-9-ubuntu-14-04-13-10/
For Win : http://docs.opencv.org/doc/tutorials/introduction/windows_install/windows_install.html
For Mac OS : http://tilomitra.com/opencv-on-mac-osx/
			 https://github.com/enekochan/installation-scripts

Install notes:
I had to make the changes here: https://github.com/Itseez/opencv/commit/ea50be0529c248961e1b66293f8a9e4b807294a6 (Casey)


NumPy
SciPy

==================
Usage
==================
For processing videos simply call:

(note: if using on a linux machine, call dos2unix on all feature files)

cvModule.py -video=/usr/mylocation/myvideo
or
cvModule.py -video=0 for camera input

!! Make sure you have -calibrationPos enabled if you want the coordinates to be relative to the white area in the video as used
in the experiment 

The videos logic is as follows:

in Video.run():

while (not end of video)

	read frame
	if (length of tracked objects == 0 )
	
		segment objects and classify them # Image.processObjects()
		register with the tracker #Tracker.add(object)
	else:
	
		segment objects and extract raw information about them # Image.findObjects()
		call tracker to do the matching # Tracker.track(newObjects)
			in Tracker:
				find best match Tracker.match and call Object.update(newObject)
				to update distributions

-----------------------
To process an image
cvModule.py -processImage=/usr/mylocation/myfile # for real world images
and 
cvModule.py -processImage=/usr/mylocation/myfile -static # for static images


For images the logic is at follow:

	img.findContour() - edge and contour detection
	img.traverseContour() - traverse hierarchical contour structure and extract objects
	for object in objects:
		object.extractProperties() - extract raw information
		object.classify()		   - assign prob distributions over known shape and colour classes
-----------------------

Classification needs a trained classifier, by default this Knn for shape and MNL for colour,
the features are precomputed and their default location is in features/pento or 
features/generatedPento for generated images. A different location can be specified by

cvModule.py -classifier mnl /usr/myfolder
-------------------
To recompute the features from a new set of training data call
cvModule.py -newData /usr/mydatafolder
or 
cvModule.py -newDataC /usr/mydatafolder
for colour features

----------------------
More options, such as saving to xml or outputing video are provided in below

usage: cvModule.py [-h] [-video [VIDEO]] [-start [START]] [-stop [STOP]]
                   [-processImage PROCESSIMAGE] [-processFolder PROCESSFOLDER]
                   [-processFolderV PROCESSFOLDERV]
                   [-classifier CLASSIFIER [CLASSIFIER ...]]
                   [-colourClassifier [COLOURCLASSIFIER [COLOURCLASSIFIER ...]]]
                   [-newData [NEWDATA]] [-newDataC [NEWDATAC]] [-static]
                   [-writeVideo] [-calibrationPos] [-record] [-saveToXML]
                   [-collectData COLLECTDATA] [-saveTo SAVETO]
                   [-snapshot SNAPSHOT] [-perspectiveTransform] [-v]

optional arguments:
  -h, --help            show this help message and exit
  -video [VIDEO]        Process video from source, if none specified input
                        from camera will be processed
  -start [START]        Start frame for the video processing
  -stop [STOP]          Stop frame for the video processing
  -processImage PROCESSIMAGE
                        Filename of the image to be processed
  -processFolder PROCESSFOLDER
                        Process entire folder of images
  -processFolderV PROCESSFOLDERV
                        Process the entire folder of videos
  -classifier CLASSIFIER [CLASSIFIER ...]
                        Classifier for shape recognition. Second argument is
                        optional, specifies a location for the shape features
                        file
  -colourClassifier [COLOURCLASSIFIER [COLOURCLASSIFIER ...]]
                        Classifier for colour recognition. Second argument is
                        optional, specifies a location for the colour features
                        file
  -newData [NEWDATA]    Extract features for shape, argument is location of
                        the new examples
  -newDataC [NEWDATAC]  Extract features for colour, argument is location of
                        the new examples
  -static               Enable for computer generated images
  -writeVideo           Process static images
  -calibrationPos       Report x,y centroid coordinates relative to the
                        enclosing bounding box( this is useful for calibration
                        with Kinect data)
  -record               Record a video sequence
  -saveToXML            Save the object descriptions in XML format
  -collectData COLLECTDATA
                        Collect image data for specified shape default
                        location for storing is ../data/shape
  -saveTo SAVETO        save snapshot image under this name
  -snapshot SNAPSHOT    Collect image data for specified shape default
                        location for storing is dirpath\examples
  -perspectiveTransform
                        Adjust for any perspective transformations
  -v, --verbose         Verbosity level



