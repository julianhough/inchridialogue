package app;

import inpro.apps.SimpleReco;
import inpro.apps.SimpleText;
import inpro.apps.util.RecoCommandLineParser;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.TextBasedFloorTracker;
import inpro.incremental.sink.FrameAwarePushBuffer;
import inpro.incremental.source.GoogleASR;
import inpro.incremental.source.IUDocument;
import inpro.incremental.source.SphinxASR;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;
import inpro.io.rsb.RsbListenerModule;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JTextField;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import sium.module.ResolutionModuleWAC;
import sium.nlu.context.Context;
import sium.nlu.language.LingEvidence;
import sium.system.util.TakeCVSqlUtils;
import edu.cmu.sphinx.util.props.ConfigurationManager;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;

public class hri {
	
//	@S4Component(type = WebSpeech.class)
//	public final static String PROP_CURRENT_HYPOTHESIS = "webSpeech";
	
	@S4Component(type = SphinxASR.class)
	public final static String PROP_CURRENT_HYPOTHESIS = "currentASRHypothesis";	
	
	@S4Component(type = TextBasedFloorTracker.Listener.class)
	public final static String PROP_FLOOR_MANAGER_LISTENERS = TextBasedFloorTracker.PROP_STATE_LISTENERS;
	
	@S4Component(type = TextBasedFloorTracker.class)
	public final static String PROP_FLOOR_MANAGER = "textBasedFloorTracker";	
	
	private static ConfigurationManager cm;
	private static IUDocument iuDocument;
	private static PropertySheet ps;
	private List<PushBuffer> hypListeners;
	
	static List<EditMessage<IU>> edits = new ArrayList<EditMessage<IU>>();
	static int currentFrame = 0;
	static JTextField textField;
	
	private TakeCVSqlUtils take;
	
	public final static double DECISION_POINT = 0.1;
	private final static int TIME_OUT = 50000; //50 seconds then reset google
	private long lastReset;
	


	private void turnOffLoggers() {
		List<Logger> loggers = Collections.<Logger>list(LogManager.getCurrentLoggers());
		loggers.add(LogManager.getRootLogger());
		for ( Logger logger : loggers ) {
		    logger.setLevel(Level.OFF);
		}		
	}

	public void notifyListeners(List<PushBuffer> listeners) {
		if (edits != null && !edits.isEmpty()) {
			//logger.debug("notifying about" + edits);
			currentFrame += 100;
			for (PushBuffer listener : listeners) {
				if (listener instanceof FrameAwarePushBuffer) {
					((FrameAwarePushBuffer) listener).setCurrentFrame(currentFrame);
				}
				// notify
				listener.hypChange(null, edits);
				
			}
			edits = new ArrayList<EditMessage<IU>>();
		}
	}
	
	private void setGoldSlots(ResolutionModuleWAC resolution, String episode) throws SQLException {
		String gold = take.getGoldPiece(episode);
		resolution.setReferent(gold);
	}
	
	private void processLine(ResolutionModuleWAC resolution, ArrayList<LingEvidence> ling) {
		
		ArrayList<WordIU> ius = new ArrayList<WordIU>();
		
		WordIU prev = WordIU.FIRST_WORD_IU;
		
		for (LingEvidence ev : ling) {
			String word = ev.getValue("w1");
			if (word.equals("<s>")) continue;
			WordIU wiu = new WordIU(word, prev, null);
			resolution.addTrainingInstance(wiu);
//			ius.add(wiu);
//			edits.add(new EditMessage<IU>(EditType.ADD, wiu));
//			notifyListeners(hypListeners);
			prev = wiu;
		}
		
		for (WordIU iu : ius) {
			edits.add(new EditMessage<IU>(EditType.COMMIT, iu));
		}
		notifyListeners(hypListeners);
	}	
	
	private void setContext(ResolutionModuleWAC resolution, String episode) throws SQLException {
		Context<String,String> context = take.getRawFeatures(episode);
		resolution.setContext(context);
	}
	
	private void run() throws SQLException, IOException, UnsupportedAudioFileException {
		
		PropertyConfigurator.configure("log4j.properties");
		
		cm = new ConfigurationManager(new File("src/config/config.xml").toURI().toURL());
		ResolutionModuleWAC resolution =  (ResolutionModuleWAC) cm.lookup("wac");
		
		/*
		System.out.print("Training WAC...");
//		resolution.toggleTrainMode();
		take = new TakeCVSqlUtils();
		take.createConnection();
		ArrayList<String> episodes = take.getAllEpisodes();
		
//		setup the training data
		for (String episode : episodes) {
			setContext(resolution, episode);
			
			ArrayList<LingEvidence> ling = take.getLingEvidence(episode, "t");
			setGoldSlots(resolution, episode);
			processLine(resolution, ling);
		}
		
		System.out.println(" WAC training done.");
		*/
		
		
		ps = cm.getPropertySheet(PROP_CURRENT_HYPOTHESIS);
//		WebSpeech webSpeech = (WebSpeech) cm.lookup(PROP_CURRENT_HYPOTHESIS);
		
		
//		DialogueManager_Pentoland dm =  (DialogueManager_Pentoland) cm.lookup("dm_p");
//		dm.updateState("State", "start");   
		
		RsbListenerModule rsb = (RsbListenerModule) cm.lookup("sceneListener");
		rsb.iulisteners = cm.getPropertySheet("sceneListener").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		
		RsbListenerModule rsbCallback = (RsbListenerModule) cm.lookup("robotListener");
		rsbCallback.iulisteners = cm.getPropertySheet("robotListener").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		
//		RsbListenerModule rsbReset = (RsbListenerModule) cm.lookup("rsblistener_reset");
//		rsbReset.iulisteners = cm.getPropertySheet("rsblistener_reset").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		

		
		//resolution.train();
		resolution.toggleNormalMode();
		
//		TimeoutPentoland tm = (TimeoutPentoland) cm.lookup("timeout");
//		tm.iulisteners = cm.getPropertySheet("timeout").getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		
		hypListeners = ps.getComponentList(SphinxASR.PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		TextBasedFloorTracker textBasedFloorTracker = (TextBasedFloorTracker) cm.lookup(PROP_FLOOR_MANAGER);
		iuDocument = new IUDocument();
		iuDocument.setListeners(hypListeners);
		SimpleText.createAndShowGUI(hypListeners, textBasedFloorTracker);
		//setContext(resolution, "r9.92.p1");
		
		this.lastReset = System.currentTimeMillis();
        /*
      	//Google normal without shutdown
        new Thread(){ 
            public void run() {
                
                try {
                	while (true){
                		if ((System.currentTimeMillis()-lastreset)<timeout){
                			System.out.println("resetting ASR!");
                			GoogleASR webSpeech = (GoogleASR) cm.lookup("googleASR");
                	        final RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-M", "-G", "AIzaSyCDOkjKVuIKkMM_oic5gty7t91CWw8Y6ws"});
                        	SimpleReco simpleReco = new SimpleReco(cm, rclp);
                        	//simpleReco.recognizeInfinitely();
                        	//simpleReco.getRecognizer().recognize();
                        	simpleReco.recognizeOnce(); //for Google keep needing to reconnect
                       
                        	lastreset = System.currentTimeMillis();
                        }
                		System.out.println("ASR stopped working");
                	}
                    
                    
                } 
                catch (PropertyException e) {
                    e.printStackTrace();
                } 
                catch (IOException e) {
                    e.printStackTrace();
                } 
                catch (UnsupportedAudioFileException e) {
                    e.printStackTrace();
                }
            }
        }.start();
		*/
		
		
        
        new Thread() {
        	GoogleASR webSpeech = (GoogleASR) cm.lookup("googleASR");
        	String key1 = "AIzaSyAwGYGBv_fbmkbW4ghbr_iUiiylXj15-YI";
        	String key2 = "AIzaSyDXOjOCiM7v0mznDF1AWXXoR1ehqLeIB18"; 
        	String key3 = "AIzaSyCDOkjKVuIKkMM_oic5gty7t91CWw8Y6ws";
        	String key4 = "AIzaSyAP-uZb-zQWfoYMiJdI7xk9koneoQE6ED4";
        	String key5 = "AIzaSyAGjjX-cusL8V7GZ_9H-1cHtVoWv9Naf4w";
            RecoCommandLineParser rclp = new RecoCommandLineParser(new String[] {"-M", "-G", key2});
        	

            public void run() {
                
                try {
                    final SimpleReco simpleReco = new SimpleReco(cm, rclp);
                while (true) {
                    try {
                        
                    	//final SimpleReco simpleReco = new SimpleReco(cm, rclp);
                        new Thread(){ 
                            public void run() {
                                try {
                                    simpleReco.recognizeOnce();
                                } 
                                catch (PropertyException e) {
                                    e.printStackTrace();
                                } 
                                
                            }
                        }.start();
                        
                        Thread.sleep(10000);
                        webSpeech.shutdown(); //TODO add this back
                        
                        
                         //simpleReco.shutdownMic();
                    }
                        
                    catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                
                
            } catch (PropertyException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            } catch (UnsupportedAudioFileException e1) {
                e1.printStackTrace();
            }
        }
        }.start();

	
	turnOffLoggers();
	
}
	
	public static void main(String[] args) {
		try {
			new hri().run();
		} 
		catch (SQLException e) {
			e.printStackTrace();
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 
		catch (UnsupportedAudioFileException e) {
			e.printStackTrace();
		}
	}

}
