package module.robot;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import module.iu.RobotActionIU;


@SuppressWarnings("serial")
public class UserGUI extends JFrame{

	
	public static JTextArea gui_QUD; //The target action/question under discussion (can be an object in a scene)
	public static JTextArea gui_PENDING; //The actions which have been sent to the Robot/NLG but not confirmed as having been started
	public static JTextArea gui_CURRENTMOVE; //The action currently being carried out
	public static JTextArea gui_ROBOTSTATE; //The current state of the robot
	public static JTextArea gui_FACTS; //What has been done (i.e. complete actions)
	public static JTextArea gui_LOG;
	//public static JTextArea gui_Possible;
	//public static JTextField gui_input;
	public static JButton mybutton;
	//public static JButton addRuleButton;
	//public static JButton applyFirstRuleButton;
	//public static JToolBar toolBar = new JToolBar("Still draggable");

	private final static int KEY_RETURN = 10;
	
	public static PickAndPlaceRobot pentorob;
	
	
	public JTextArea getGui_PENDING() {
		return gui_PENDING;
	}


	public void setGui_PENDING(JTextArea gui_PENDING) {
		UserGUI.gui_PENDING = gui_PENDING;
	}


	public static JTextArea getGui_QUD() {
		return gui_QUD;
	}


	public static void setGui_QUD(JTextArea gui_QUD) {
		UserGUI.gui_QUD = gui_QUD;
	}
	
	
	public UserGUI(PickAndPlaceRobot pickAndPlaceRobot)
	{	
		pentorob = pickAndPlaceRobot;
		setSize(800,600);
		Toolkit toolkit = Toolkit.getDefaultToolkit();
        Dimension screenSize = toolkit.getScreenSize();
		int x = (screenSize.width - getWidth()) / 2;
        int y = (screenSize.height - getHeight()) / 2;
        setLocation(x, y);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // create a menu with File and Exit options
        JMenuBar menubar = new JMenuBar();
        setJMenuBar(menubar);

        // create the display panel
        gui_PENDING = new JTextArea(750,250);
        gui_PENDING.setPreferredSize(new Dimension(750,250));
        gui_PENDING.setBackground(Color.WHITE);
        gui_PENDING.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_PENDING.setLineWrap(true);
        gui_PENDING.setWrapStyleWord(true);
        gui_PENDING.setLayout(new FlowLayout()); 
        JLabel lab = new JLabel("PENDING");
        gui_PENDING.add(lab);

        // create a text area
        gui_QUD = new JTextArea(750,250);
        gui_QUD.setPreferredSize(new Dimension(750,250));
        gui_QUD.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_QUD.setLineWrap(true);
        gui_QUD.setWrapStyleWord(true);
        gui_QUD.setLayout(new FlowLayout());
        gui_QUD.add(new JLabel("QUD (piece)"));

        gui_FACTS = new JTextArea(750,250);
        gui_FACTS.setPreferredSize(new Dimension(750,250));
        gui_FACTS.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_FACTS.setLineWrap(true);
        gui_FACTS.setWrapStyleWord(true);
        gui_FACTS.setLayout(new FlowLayout());
        gui_FACTS.add(new JLabel("FACTS (completed actions)"));
        
        gui_CURRENTMOVE = new JTextArea(750,250);
        gui_CURRENTMOVE.setPreferredSize(new Dimension(750,250));
        gui_CURRENTMOVE.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_CURRENTMOVE.setLineWrap(true);
        gui_CURRENTMOVE.setWrapStyleWord(true);
        gui_CURRENTMOVE.setLayout(new FlowLayout());
        gui_CURRENTMOVE.add(new JLabel("CURRENT MOVE"));
        
        gui_ROBOTSTATE = new JTextArea(750,250);
        gui_ROBOTSTATE.setPreferredSize(new Dimension(750,250));
        gui_ROBOTSTATE.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_ROBOTSTATE.setLineWrap(true);
        gui_ROBOTSTATE.setWrapStyleWord(true);
        gui_ROBOTSTATE.setLayout(new FlowLayout());
        gui_ROBOTSTATE.add(new JLabel("CURRENT ROBOT STATE"));
        
        gui_LOG = new JTextArea(750,250);
        gui_LOG.setPreferredSize(new Dimension(750,250));
        gui_LOG.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_LOG.setLineWrap(true);
        gui_LOG.setWrapStyleWord(true);
        gui_LOG.setLayout(new FlowLayout());
        gui_LOG.add(new JLabel("LOG"));
        
        /*
        gui_Possible = new JTextArea(750,250);
        gui_Possible.setPreferredSize(new Dimension(750,250));
        gui_Possible.setEditable(false);   ///should this be true?- allows text area tp be editable/ not that important
        gui_Possible.setLineWrap(true);
        gui_Possible.setWrapStyleWord(true);
        gui_Possible.setLayout(new FlowLayout());
        gui_Possible.add(new JLabel("Possible Updates"));
        
        
        gui_input = new JTextField(100);
        gui_input.setEditable(true);   ///should this be true?- allows text area tp be editable/not that important
        gui_input.add(new JLabel("input"));
        */

        // add these two two scroll panes
        JScrollPane pane1 = new JScrollPane(gui_PENDING);
        JScrollPane pane2 = new JScrollPane(gui_QUD);
        JScrollPane pane3 = new JScrollPane(gui_FACTS);
        //JScrollPane pane4 = new JScrollPane(gui_input);
        JScrollPane pane5 = new JScrollPane(gui_CURRENTMOVE);
        JScrollPane pane6 = new JScrollPane(gui_ROBOTSTATE);
        JScrollPane pane7 = new JScrollPane(gui_LOG);
        
        // and add these to a Splitter pane
        //JSplitPane split5 = new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,pane1,pane5);
        //split5.setResizeWeight(0.5);
        
        JSplitPane split_PENDING_CURRENTMOVE = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,false,pane1,pane5);
        split_PENDING_CURRENTMOVE.setResizeWeight(0.5);
        
        JSplitPane split_QUD_FACTS = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,false,pane2,pane3);
        split_QUD_FACTS.setResizeWeight(0.5);

        JSplitPane split_rest = new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,split_PENDING_CURRENTMOVE,split_QUD_FACTS);
        split_rest.setResizeWeight(0.5);
        // add the splitter pane to the main frame
        
        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT,false,split_rest,pane6);
        split.setResizeWeight(0.9);
        
        JSplitPane split_with_log = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,false,split,pane7);
        split_with_log.setResizeWeight(0.5);
        
        mybutton = new JButton("START/END GAME");
        
        mybutton.addActionListener(new 	ActionListener() {
    			public void actionPerformed(ActionEvent ae) {
    				if (pentorob.getGame().inProgress()){
    					pentorob.endGameShutDown();
    				} else {
    					pentorob.startGame();
    				}
              }
            });
        add(mybutton);
        menubar.add(mybutton);
        
       // toolBar.setSize(new Dimension(250,100));
        //toolBar.add(mybutton);
        //addRuleButton = new JButton("NEW RULE");
        //applyFirstRuleButton = new JButton("APPLY FIRST RULE");
        //toolBar.add(addRuleButton);
        //toolBar.add(applyFirstRuleButton);
        
        //addRuleButton.addActionListener(new 	ActionListener() {
		//	public void actionPerformed(ActionEvent ae) {
         //      pentorob.Editor.WriteNewRuleToFile(file_newRules);
          //     try {
		//		pentorob.rules = new RuleSet(file_newRules);
//			} catch (IOException e) {
				// TODO Auto-generated catch block
	//			e.printStackTrace();
	//		}
     //     }
     //   });
        
        /*
        
        applyFirstRuleButton.addActionListener(new 	ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				Vector<Rule> possible = new Vector<Rule>(); 
				Boolean applicable = false;
				int i=0;
				for (i=0; i<5; i++)
				{
					if(pentorob.rules.getRule(i).applicable(pentorob))
					{
						applicable = true;
						possible.add(pentorob.rules.getRule(i));
						System.out.println(pentorob.rules.getRule(i).getName() + applicable);
					}
				
				
				
				applicable = false;
				
				}
				if (!possible.isEmpty())
				{
				pentorob.applyRule(possible.get(0));
				}
				updateDGBdisplay();
	               
	          }
	        });
        
        mybutton = new JButton();

		*/
       // add(toolBar, BorderLayout.PAGE_START);
        
        add(split_with_log);
     
        setVisible(true);
        
        /*
        gui_input.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KEY_RETURN) {
					pentorob.setCurrentUtt(gui_input.getText());
					displayUtt();
				}
				if (e.getKeyCode() == KEY_RETURN) 
					gui_input.setText("");
			}
		});
		*/
        this.updateDGBdisplay();
	}
	
	
	public void updateDGBdisplay() {
		
		try {
		//main method for updating the display, called from PentoRob class when desired
		//System.out.println("GUI 1");
		gui_QUD.selectAll();
		gui_QUD.setText("\n");
		if (!pentorob.getObjectUnderDiscussion().toString().equals("-1"))
		{
			gui_QUD.append(pentorob.getObjectUnderDiscussion().toString() + "\n");
		}
	
		//System.out.println("GUI 2");
		gui_CURRENTMOVE.selectAll();
		gui_CURRENTMOVE.setText("\n");
		if (pentorob.getCurrentAction()!=null){
			gui_CURRENTMOVE.append(pentorob.getCurrentAction().toPayLoad().toString() + "," + pentorob.getCurrentAction().getProgress().toString() + "," + String.valueOf(pentorob.getEndTimeOfCurrentAction()));
		}
		
		//System.out.println("GUI 3");
		gui_PENDING.selectAll();
		//System.out.println("GUI 3.1.1");
		gui_PENDING.setText("\n");
		//System.out.println("GUI 3.1.2");
		List<RobotActionIU> pending = pentorob.getPendingActions();
		//System.out.println("GUI 3.2");
		//System.out.println(pending.size());
		if (!pending.isEmpty()){
			for (RobotActionIU p : pending){
				if (p!=null) {
				gui_PENDING.append(p.toPayLoad().toString()+"\n");
				}
			}
		}
		//System.out.println("GUI 4");
		gui_FACTS.selectAll();
		gui_FACTS.setText("\n");
		List<RobotActionIU> facts = pentorob.getCompletedActions();  //to avoid concurrency probs
		if (!facts.isEmpty()){
			for (RobotActionIU p : facts){
				if (p==null) {continue;}
				gui_FACTS.append(p.toPayLoad().toString()+"\n");
			}
		}
		//System.out.println("GUI 5");
		gui_ROBOTSTATE.selectAll();
		gui_ROBOTSTATE.setText("\n");
		if (pentorob instanceof PentoRob){
			gui_ROBOTSTATE.append(((PentoRob) pentorob).getArmState().toString()); //should always have a state

		} else {
			gui_ROBOTSTATE.append(pentorob.getStateFluents().toArray().toString());
		}
		
		
		} catch (Exception e) {
			System.out.println("GUI ERROR");
			gui_LOG.selectAll();
			gui_LOG.append("ERROR: COULD NOT UPDATE GUI");
			gui_LOG.append(e.getMessage());
			for (StackTraceElement el : e.getStackTrace()){
				gui_LOG.append("\t" + el.toString() + "\n");
			}
			
		}
		
	}
	
	/*public void displayUtt()
	{	
		gui_PENDING.append(pentorob.getSpeaker() + " : " + pentorob.getCurrentUtt() + "\n");	
	}
	
	//@SuppressWarnings("deprecation")
	public void pause(int seconds)
	{
		try {
            int delay = 1000 * seconds;
            Thread.sleep(delay);

            } catch (InterruptedException ie) {
             // do nothing  t       
            }
            
 
            toolBar.add(mybutton);
            mybutton.addActionListener(new 	ActionListener() {
                @SuppressWarnings("deprecation")
				public void actionPerformed(ActionEvent ae) {
                   
                   if (pentorob.getUserMove().equalsIgnoreCase("end"))
                   {
                	   System.exit(0);
                	   
                   }else{mybutton.setLabel("");
                   if (pentorob.getUserMove().equalsIgnoreCase("deny"))
                   {
                   pentorob.setCurrentUtt("no I didn't");
                   displayUtt();
                   
                   }
                   }          
              }

          });
            
	}

	
	@SuppressWarnings("deprecation")
	public void button(String text)
	{
		final String button = text;
		mybutton.setLabel(button);
        toolBar.add(mybutton);
        mybutton.addActionListener(new 	ActionListener() {
            public void actionPerformed(ActionEvent ae) {
               
               if (mybutton.getLabel().equalsIgnoreCase("end"))
               {	
            	   pentorob.setCurrentUtt("OK bye then");
                   displayUtt();
            	   System.exit(0);
            	   
               }else if (mybutton.getLabel().equalsIgnoreCase("deny"))
               {
               pentorob.setCurrentUtt("no I didn't");
               displayUtt();
               mybutton.setLabel("blank");
               }   
               
          }

      });
        
				
	}
	*/
	
	public void displayError(String error){
		System.out.println(error);
		gui_LOG.selectAll();
		gui_LOG.append(error+"\n");
	}

	
	
}
