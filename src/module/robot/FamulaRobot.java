package module.robot;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import org.apache.commons.scxml.model.ModelException;

//import groundingstatemachine.FamulaRobotStateMachine;

import edu.cmu.sphinx.util.props.PropertySheet;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.io.SensorIU;
import module.game.Game;
import module.iu.RobotActionIU;
import module.iu.RobotStateUpdateIU;

public class FamulaRobot extends PickAndPlaceRobot {
	
	//FamulaRobotStateMachine hsm;
	
	@Override
	public void newProperties(PropertySheet ps) {
		super.newProperties(ps);
		// TODO Load the FAMULA state machine, pure XML verson of the HSM
		// below abstract booleans will be according to that HSM
				
	 /*try {
		 this.hsm = new FamulaRobotStateMachine(ps.getString(STATE_MACHINE_FILE));
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ModelException me) {
			me.printStackTrace();
		}*/
		
	}


	@Override
	public boolean isWaiting() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isWaitingForObject() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isMovingXY() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isDropping() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isPlacingObject() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isOverObject() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isHoldingObject() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isGrabbing() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isOverTargetArea() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isTakingObject() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isWaitingForLocation() {
		// TODO Auto-generated method stub
		return false;
	}

	/**
	 * Receives a sensorIU with a 
	 * @param iu
	 */
	public void updateStateFromRobotStateMachine(RobotStateUpdateIU iu){
		
	}


	@Override
	public String getDestinationUnderDiscussion() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getObjectUnderDiscussion() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int getTimeSinceLastGoalMovement() {
		// TODO Auto-generated method stub
		return 0;
	}


	@Override
	public Game getGame() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean updateStateFromCallBackMessage(CallBack callback) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean updateState(boolean displayUpdate) {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean currentGoalActionLegible() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public void startGame() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void endGameShutDown() {
		// TODO Auto-generated method stub
		
	}


	@Override
	public IU getCurrentAction() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<RobotActionIU> getCompletedActions() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<RobotActionIU> getPendingActions() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public int getEndTimeOfCurrentAction() {
		// TODO Auto-generated method stub
		return 0;
	}
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// TODO Auto-generated method stub
		List<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			if (iu instanceof SensorIU) {
				java.awt.Toolkit.getDefaultToolkit().beep();
				logger.info("recieved state update");
				//<STATE id=":root:sfb:processing:put:exec:placePos"/>
				List<String> lines = Arrays.asList(((SensorIU) iu).toPayLoad().split("\n"));
				for (String line: lines){
					if (line.contains("<STATE id=") ){
						logger.debug(line);
						String command = line.replace("<STATE id=\"root:","").replace("\"/>", "");
						
					}
				}
			}
		
		}
	}
	
	
}
