package module.robot;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import edu.cmu.sphinx.util.props.PropertySheet;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.io.SensorIU;
import module.game.Explore;
import module.game.Game;
import module.game.PutInNumberedSlot;
import module.iu.FrameIU;
import module.iu.MessageIU;
import module.iu.RobotActionIU;
import module.iu.RobotActionIU.Action;
import util.Pair;
import util.StateMachine;

/**
 * A module which send the commands to the PentoRob robot, adjusting them for safety when necessary.
 * Also receives call-backs in terms of estimation of completion time and success of PentoRob's actions.
 * Also updates simple state variables which can be accessed by NLU.
 */
public class PentoRob extends PickAndPlaceRobot {
	
	private StateMachine<ArmState> stateMachine;
	//simple state variables which can be accessed by NLU and other interested modules
	private ArmState armState; //what the arm is doing currently
	
	
	@Override
	public void newProperties(PropertySheet ps) {
		super.newProperties(ps);
		stateMachine = new StateMachine<ArmState>(ps.getString(STATE_MACHINE_FILE),ArmState.values());		
		armState = ArmState.STATIONARY;
	}
	
	public Boolean getDisplayConfidence() {
		return displayConfidence;
	}

	public void setDisplayConfidence(Boolean display_confidence) {
		this.displayConfidence = display_confidence;
	}

	public void startGame(){
		//sends a signal to start recording and waits 2 seconds
		MessageIU iu = new MessageIU(System.currentTimeMillis(),"START_RECORD");
		rightBuffer.addToBuffer(iu);
		super.notifyListeners();
		try {
			Thread.sleep(2000);
		} 
        catch (InterruptedException e) {
        	logger.error("PENTOROB: Initial thread to start game failed.");
		}
		
		setArmState(this.armState);
        this.game.start();
	}
	
	//getters and setters
	@Override
	public Game getGame() {
		return game;
	}
	
	public ArmState getArmState() {
		return armState;
	}

	public void setArmState(ArmState armstate) {
		this.armState = armstate;
		this.stateCache.add(new Pair<Set<String>, Integer>(this.getStateFluents(),this.getTime())); //adds previous one and its start time
		if (this.stateCache.size()>6){ //just keep the previous 5
			this.stateCache.remove(0);
		}
	}

	@Override
	public String getObjectUnderDiscussion() {
		return objectUnderDiscussion;
	}

	public void setObjectUnderDiscussion(String piece) {
		if (!piece.equals(this.getObjectUnderDiscussion())){
			logger.debug("PENTOROB: NEW object under discussion" + piece);
		}
		this.objectUnderDiscussion = piece;
	}
	
	public void setDestinationUnderDiscussion(String position) {
		if (!position.equals(this.getDestinationUnderDiscussion())){
			logger.debug("PENTOROB: NEW Destination under discussion" + position);
		}
		this.destinationUnderDiscussion = position;
		if (this.game instanceof PutInNumberedSlot){
			((PutInNumberedSlot) this.game).setCurrentTargetSlot(Integer.parseInt(position)-1); //becomes public in the game too
		} else if (this.game instanceof Explore){
			//((Explore) this.game).
		}

	}
	
	@Override
	public String getDestinationUnderDiscussion() {
		return this.destinationUnderDiscussion;
	}

	public RobotActionIU getCurrentAction() {
		return currentAction;
	}

	public void setCurrentAction(RobotActionIU currentaction) {
		this.currentAction = currentaction;
	}
	
	public List<RobotActionIU> getPendingActions() {
		return pendingActions;
	}

	public void setPendingActions(List<RobotActionIU> pendingactions) {
		this.pendingActions = pendingactions;
	}
	
	public List<RobotActionIU> getCompletedActions() {
		return completedActions;
	}
	
	//conversion of the commands to the PentoRob API's language
	/**
	 * Generates a move action with the coordinates of the target object
	 * @param ID The object's ID in the scene
	 * @param speed The percentage of the top speed
	 * @return
	 */
	public RobotActionIU moveToObject(String ID, double speed){
		String coords = scene.getObjectCentroidXY(ID);
		if (coords==null){
			return null;
		}
		RobotActionIU action = new RobotActionIU(Action.MOVE,coords + ":" + String.valueOf(speed));
		return action;
	}
	
	public RobotActionIU moveToStartPosition(){
		//Integer coords = this.getCurrentPosition();
		Integer[] coords = scene.getTopLeftCornerOfScene();
		if (coords==null){
			logger.error("PENTOROB ERROR: NOT MOVING TO START");
			return null;
		}
		String coordstring = String.valueOf(coords[0]) + ":" + String.valueOf(coords[1]);
		RobotActionIU action = new RobotActionIU(Action.MOVE,coordstring);
		return action;
	}

	/**
	 * Generates a move action which gets PentoRob away from the objects to 
	 * prevent obscuring the camera
	 * @return
	 */
	public RobotActionIU moveOutOfCameraView(){
		//Integer coords = this.getCurrentPosition();
		Integer[] coords = scene.getCentreOfTopOfScene();
		if (coords==null){
			logger.error("PENTOROB ERROR NOT GETTING OUT THE WAY");
			return null;
		}
		String coordstring = String.valueOf(coords[0]) + ":" + String.valueOf(coords[1]);
		RobotActionIU action = new RobotActionIU(Action.MOVE,coordstring);
		return action;
	}
	
	private RobotActionIU moveToCentreTopOfPlacementArea() {
		Integer[] i = this.game.getCentroidOfCentroidsOfAvailableTargetDestinations();
		return this.moveToCoordinates(i[0], i[1]-100, 0);
	}
	
	public RobotActionIU grabObject(){
		//return new RobotActionIU(Action.GRAB_LOW,null);  // switch to low mode for speed in easy domain with no obstructing objects
		return new RobotActionIU(Action.GRAB,null);
	}
	
	public RobotActionIU dropObject(){
		//return new RobotActionIU(Action.DROP_LOW,null);  // switch to low mode for speed in easy domain with no obstructing objects
		return new RobotActionIU(Action.DROP,null);
	}
	
	public RobotActionIU abortCurrentAction(){
		//return this.moveOutOfCameraView();
		this.currentAction.setAborted(true);
		this.currentAction.setEndTime(getTime()); //means in next update will be able to overridden
		return new RobotActionIU(Action.ABORT,null);
	}
	
	public RobotActionIU moveToCoordinates(int x, int y, int z){
		return new RobotActionIU(Action.MOVE,String.valueOf(x)+":"+String.valueOf(y));
	}
	
	//other derived state info and update methods
	public int getEndTimeOfCurrentAction() {
		if (currentAction==null){
			return 0; //beginning of time!
		}
		return this.currentAction.getEndTime();
	}
	
	public int getEndTimeOfLastCompletedAction() {
		if (this.completedActions.isEmpty()){
			return 0; //beginning of time!
		}
		return this.completedActions.get(this.completedActions.size()-1).getEndTime();
	}
	
	@Override
	public int getTimeSinceLastGoalMovement() {
		if (this.getArmState().equals(ArmState.STATIONARY)){
			return this.getTime()- this.getEndTimeOfLastCompletedAction();
		}
		return 0;
	}
	
	public RobotActionIU getMaxPending(){
		if (pendingActions.isEmpty()){
			return null;
		}
		return pendingActions.get(pendingActions.size()-1);
	}
	
	public RobotActionIU getLastCompletedAction(){
		if (completedActions.isEmpty()){
			return null;
		}
		return completedActions.get(completedActions.size()-1);
	}
	
	public int getEndTimeOfLastPendingAction() {
		// TODO for now the same as end of current action
		// needs to be the last cued action that's been sent to pentorob
		if (this.currentAction==null){
			return this.getMaxPending().getEndTime();
		}
		return this.getEndTimeOfCurrentAction();
	}
	
	/*
	public boolean holdingPiece(){
		if (armstate.equals(ArmState.MOVINGWITHOBJECT)||armstate.equals(ArmState.STATIONARYWITHOBJECT)){
			return true;
		}
		return false;
	}
	*/
	
	public Integer[] getCurrentPosition(){
		//TODO
		Integer [] coords = new Integer[2];
		//if (this.getArmstate().equals(ArmState.STATIONARY)){
		if (this.completedActions.isEmpty()){
			coords[0] = 40;
			coords[1] = 40;
		} else {
			RobotActionIU last = this.getCompletedActions().get(this.getCompletedActions().size()-1);
			if (!last.getActionType().name().contains("STATIONARY")){
				
				String[] coordstring = last.getActionParam().split(":");
				coords[0] = Integer.parseInt(coordstring[0]);
				coords[1] = Integer.parseInt(coordstring[1]);
			} else {
				
		    }
		}
		return coords;
	}
	
	/*
	public boolean armBlockingVision(){
		//TODO needs more principled of doing this
		if (this.getArmstate().equals(ArmState.STATIONARY)){
			if (this.completedactions.isEmpty()){
				return false;
			}
			RobotActionIU last = this.getCompletedactions().get(this.getCompletedactions().size()-1);
			if (last.getActiontype().equals(Action.MOVE)&&last.getActionparam().split(":")[1].equals("40")){
				return false;
			}
		}
		return true;
	}
	*/
	
	public boolean isOverTargetArea() {
		return overTargetArea;
	}

	public void setOverTargetArea(boolean over) {
		logger.debug("PENTOROB OVER TARGET AREA change: " + over);
		this.overTargetArea = over;
	}
	
	public boolean isHoldingObject() {
		return holdingObject;
	}

	public void setHoldingObject(boolean holding) {
		this.holdingObject = holding;
	}

	public boolean isMovingXY(){
		if (this.getArmState().name().contains("MOVING")){
			return true;
		}
		return false;
	}
	
	//superstate fluent
	public boolean isTakingObject() {
		switch (this.getArmState()){
			case MOVING_TO_OBJECT:
				return true;
			case GRABBING_OBJECT:
				return true;
			case STATIONARY_OVER_OBJECT:
				if (!this.getObjectUnderDiscussion().equals("-1"))
				{
					return true;
				}
			default:
				break;
		}
		return false;
	}
		
	//superstate fluent
	public boolean isPlacingObject(){
		if (this.isHoldingObject()&&
				!this.getObjectUnderDiscussion().equals("-1")&&
				!this.getDestinationUnderDiscussion().equals("-1")){
			return true;
		}
		return false;
	}
		
	//super superstate fluent
	public boolean isPuttingPiece() {
		if (this.getObjectUnderDiscussion().equals("-1"))
			return false;
		return true;
	}
	
	//superstate fluent mutually exclusive to putting
	@Override
	public boolean isWaitingForObject() {
		if (!this.isPuttingPiece()){
			return true;
		}
		return false;
	}
	
	//superstate fluent
	@Override
	public boolean isWaitingForLocation(){
		if (this.getDestinationUnderDiscussion().equals("-1")){
			return true;
		}
		return false;
	}
	
	//super superstate fluent another cutting of the state space
	@Override
	public boolean isWaiting(){
		return (this.isWaitingForObject() || this.isWaitingForLocation()) ? true : false;
	}
	
	@Override
	public boolean currentGoalActionLegible(){
		if (this.isMovingXY()){
			int s = this.getCurrentAction().getStartTime();
			int e = this.getCurrentAction().getEndTime();
			float legibilityTime = s + (e-s)/2;
			if (this.getTime()<legibilityTime){
				return false;
			}
		}
		return true;
	}

	public double getTimeSinceLastCommit(){
		return this.getTime() - lastCommitTime;
	}
	
	public void setLastCommitTime(int time){
		lastCommitTime = time;
		if (gui!=null){
			logger.debug("commit at " + lastCommitTime);
		}
	}
	
	public void objectGrabbed(){
		//piece under discussion already set, just need to set that piece is being held
		logger.debug("PENTOROB OBJECT GRABBED");
		java.awt.Toolkit.getDefaultToolkit().beep();
		setHoldingObject(true);
		this.setArmState(this.getArmState()); //just resets the fluents
	}
	
	public void objectPlaced(){
		logger.debug("PENTOROB OBJECT PLACED");
		java.awt.Toolkit.getDefaultToolkit().beep();
		try {
			if (this.game instanceof PutInNumberedSlot){
				((PutInNumberedSlot) this.game).piecePlacedAtSlotNumber(
						this.getObjectUnderDiscussion(),
						Integer.parseInt(this.getDestinationUnderDiscussion())-1);
			} else {
				
			}
			
		}
		catch (Exception e){
			logger.error("failed piece place");
		}
		logger.info(this.getObjectUnderDiscussion() + " placed at position " + this.getDestinationUnderDiscussion());
		setHoldingObject(false);
		setOverTargetArea(false); //semantics of game- no longer a target area once dropped
		//clear the slots
		this.setObjectUnderDiscussion("-1");
		this.setDestinationUnderDiscussion("-1");
		this.setArmState(this.getArmState()); //just resets the fluents as this is a state change
		
		if (this.game.completed()){
			this.endGameShutDown();
		}
	}
	
	public void endGameShutDown(){
		logger.info("GAME OVER!");
		this.game.end();
		MessageIU iu = new MessageIU(System.currentTimeMillis(),"STOP_RECORD");
		rightBuffer.addToBuffer(iu);
		super.notifyListeners();
		try {
			Thread.sleep(2000);
		} 
        catch (InterruptedException e) {
        	logger.error("PENTOROB: Final thread to end game failed.");
		}
		System.exit(0);
	}
	
	public void pushToPending(RobotActionIU iu){
		this.pendingActions.add(iu);
		this.gui.updateDGBdisplay();
	}
	
	public String stateSummary(){
		int actionID = -1;
		int timeleft = -1000;
		if (this.currentAction!=null){
			actionID = this.currentAction.getID();
			timeleft = this.getEndTimeOfCurrentAction() - this.getTime();
		}
		String message = "PENTOROB STATE:\n\tarm: " + armState.toString() + 
				"\n\tpiece: " + objectUnderDiscussion + "\n\tmove: " + actionID + 
				"\n\ttimeToEnd: " + timeleft;
		return message;
	}
	
	public synchronized void takeInitiative(){
		if (!this.game.inProgress()){
			return;
		}
		if (this.getArmState().equals(ArmState.STATIONARY_OVER_OBJECT)){
			logger.debug("TAKING initiative-- over piece");
			logger.debug("PENTOROB GRAB OBJECT ITS OVER");
			RobotActionIU last = this.getLastCompletedAction();
			logger.debug("current action");
			logger.debug(this.getCurrentAction());
			logger.debug("last completed action");
			logger.debug(last);
			logger.debug(this.completedActions);
			//IU lastgroundedin = last.groundedIn().get(0);
			//NB this isn't working so hacking a new FrameIU in:
			FrameIU lastgroundedin = new FrameIU(getObjectUnderDiscussion(),"GRAB",1.0);
			logger.debug("last completed action GRINS");
			logger.debug(last.groundedIn());
			RobotActionIU grab =  grabObject();
			grab.groundIn(lastgroundedin);
			this.pushToPending(grab);
			rightBuffer.addToBuffer(grab);
			super.notifyListeners();
		} else if (this.getArmState().equals(ArmState.STATIONARY_WITH_OBJECT)&&this.isOverTargetArea()){
			logger.debug("TAKING initiative-- over target area");
			logger.debug("PENTOROB DROP OBJECT");
			RobotActionIU last = this.getLastCompletedAction();
			logger.debug("current action");
			logger.debug(this.getCurrentAction());
			logger.debug("last completed action");
			logger.debug(last);
			logger.debug(this.completedActions);
			//IU lastgroundedin = last.groundedIn().get(0);
			//NB this isn't working so hacking a new FrameIU in:
			ArrayList<String> dev = new ArrayList<String>();
			dev.add(getDestinationUnderDiscussion());
			FrameIU lastgroundedin = new FrameIU(getObjectUnderDiscussion(),"DROP",dev,1.0); 
			logger.debug("last completed action GRINS");
			logger.debug(last.groundedIn());
			RobotActionIU drop =  dropObject();
			drop.groundIn(lastgroundedin);
			this.pushToPending(drop);
			//rightBuffer.addToBuffer(drop);
			
			logger.debug("PENTOROB: MOVE OUT OF WAY!");
			RobotActionIU moveaway = this.moveOutOfCameraView();
			FrameIU moveoutofwayiu = new FrameIU("MOVE_OUT_OF_VIEW","-1",1.0); // TODO for now creating fake frame
			moveoutofwayiu.setObjectID("-1");
			moveaway.groundIn(moveoutofwayiu);
			moveaway.addNextSameLevelLink(drop); //to show the two actions are joined
			this.pushToPending(moveaway);
			
			rightBuffer.addToBuffer(drop);
			rightBuffer.addToBuffer(moveaway);
			
			super.notifyListeners();
		}
		this.updateState(false);
	}

	private void checkTimeout(int new_endtime, double waitingTimePenalty) {
		logger.debug("Checking timeout");
		if (timeOutThread != null){ 
			if (inStroke){
				logger.debug("Timeout thread not started as in stroke timeout 2");
				return;
			}
			logger.debug("Timeout thread interrupted 1");
			timeOutThread.interrupt();
		}
		int waittime = (int) ((timeOut - (timeOut * waitingTimePenalty)) * 1000);
		logger.debug("waittime");
		logger.debug(waittime);
		endTime = new_endtime + waittime - getTime();
		if (endTime<0){
			if (endTime <-3){
				//allow tolerance of 3 ms
				logger.debug("WARNING: timeout negative value, not executing");
				logger.debug(endTime);
				return;
			}
			endTime = 0;
		}
	//	create a timeout ... if nothing happens after <timeout> seconds, then send time out
		//final int sleepduration = (endtime + (timeout * 1000)) - getTime();
		
		if (timeOut != -1) {
			timeOutThread = new Thread(
					new Runnable() {
			    public void run() {
			        try {
			        	int sleepduration = (endTime);
			        	//int sleepduration = endtime - getTime(); //for now just sleep till end of action
			        	logger.debug("sleeping for " + sleepduration);
						Thread.sleep(sleepduration);
					} 
			        catch (InterruptedException e) {
			        	logger.debug("Timeout thread interrupted 2");
	//		        	if something happened within the timeout, then just return (i.e., don't reset the context)
			        	return;
					}
			        updateState(false); //just updates at end
			        inStroke = true;
			        takeInitiative();
			        inStroke = false;
			   
			    }
			});
			timeOutThread.start();
		}
	}
	
	private void strokeTimeout(int stroke_endtime) {

		//to distinguish it from the general timeout thread which isn't in critical section
		//the mutual exclusion problem, can only be killed if not in critical
		//section
		//The below is dangerous as the parent thread could be a timeout thread..
		//if (timeoutThread != null){ 
		//	logger.debug("Timeout thread interrupted before stroke timeout");
		//	timeoutThread.interrupt();
		//}
		
		logger.debug("Making stroke timeout");
		//For actions with strokes that are initial GRABBING + DROPPING
		//update the holdingPiece variable more accurately
		//and update the pieceUnderDiscussion, destUnderDiscussion and Game variables more accurately
		//and also the game variables
		
		
		strokeEndTime = stroke_endtime; //won't get callback before this thread is over
		
		strokeTimeOutThread = new Thread(
					new Runnable() {
			    public void run() {
			        try {
			        	//int sleepduration = (timeout * 1000);
			        	int sleepduration = strokeEndTime - getTime(); //for now just sleep till end of action
			        	logger.debug("stroke sleeping for " + sleepduration);
						Thread.sleep(sleepduration);
					} 
			        catch (InterruptedException e) {
			        	logger.debug("Stroke timeout thread interrupted 2");
	//		        	if something happened within the timeout, then just return (i.e., don't reset the context)
			        	return;
					}
			        if (getArmState().equals(ArmState.GRABBING_OBJECT)){
			        	objectGrabbed();
			        } else if (getArmState().equals(ArmState.DROPPING_OBJECT)){
			        	objectPlaced();
			        }
			       //takeInitiative();
			    }
			});
		strokeTimeOutThread.start();
	}
	
	@Override
	public synchronized boolean updateState(boolean displayUpdate){
		//update state by checking whether the current action has ended or not
		logger.debug("UPDATING STATE");
		if (currentAction!=null&&getEndTimeOfCurrentAction()-getTime()<=0){
			currentAction.makeCompleted();
			completedActions.add(currentAction);
			//Action actiontype = currentaction.getActiontype();
			//simple logic to get the piece involved in the current action, if any
			String pud = "-1";
			String dud = "-1";
			if (!currentAction.groundedIn().isEmpty()){
				FrameIU grin = ((FrameIU) currentAction.groundedIn().get(0));
				if (grin.getObjectID()!=null){
					pud = grin.getObjectID();
				}
				if (grin.getDestinationIDs()!=null&&!grin.getDestinationIDs().isEmpty()){
					dud = grin.getDestinationIDs().get(0);
				}
			}
			//now simple calculation of the state based on the previous state and new current action
			//String actiontype = grin.getAction(); //optimistically getting this from GRIN
			String actiontype = currentAction.getActionType().toString(); //less optimistic- actual physical state
			Action action = currentAction.getActionType();
			switch (action){
				case MOVE:
					if (this.isHoldingObject()){
						actiontype+="_WITH_OBJECT";
					} else if (!pud.equals("-1")){
						//if (currentaction.getSameLevelLink()!=null&&
						//		((RobotActionIU) currentaction.getSameLevelLink()).getActiontype().equals(Action.DROP)){
						//	break;
						//}
						actiontype+="_TO_OBJECT";
					}
					break;
				default:
					break;
			}
			actiontype = actiontype.replace("_LOW","");
			//get the resulting state
			ArmState teststate = stateMachine.getResultingState(getArmState(),actiontype + ".END");
			if (teststate!=null){
				//logger.debug("PENTOROB: update state no callback: got successful state: " + teststate.toString());
				
				//if the state does not involve a piece then reset the piece under discussion to null
				//if (this.getArmstate().equals(ArmState.STATIONARY)||this.getArmstate().equals(ArmState.MOVING)){
				//	setPieceunderdiscussion("-1");
				//}
				if (actiontype.equals("MOVE_WITH_OBJECT")){
					this.setDestinationUnderDiscussion(dud); //should already have happened, just to be safe
					if (!dud.equals("-1")){
						setOverTargetArea(true);
					} else {
						setOverTargetArea(false);
					}
				}
				else if (actiontype.equals("DROP")&&isOverTargetArea()){
					//TODO for now assuming drop has happened successfully
					//piecePlaced(); //this happens separately
				
					//this.scene.setUpdating(true);
				}
				setArmState(teststate); // do this here to get the right values of the fluents for over area if needed
				
				currentAction = null; //reset action regardless of successful state transition

			} else {
				logger.debug("PENTOROB: WARNING: updateState (no callback) state problem " + actiontype 
						+ ".END" + " from " + getArmState().toString() + " not possible");
			}
			
		}
		//logger.debug("UPDATED STATE");
		
		if (getArmState().equals(ArmState.STATIONARY)){
			//scene.resetScene(); //TODO for now reset scene when stationary
			//this.scene.setUpdating(true);
		} else {
			//this.scene.setUpdating(false);
		}
		if (this.game.inProgress()){
		}
		if (displayUpdate){
			gui.updateDGBdisplay();
			//logger.debug("UPDATED GUI");
		}
		return true;
	}
	
	/**
	 *  Simple state update based on a callback message with ActionIU_id and predicted duration of action.
	 **/
	@Override
	public synchronized boolean updateStateFromCallBackMessage(CallBack cb){
		//either way they get popped off pending
		//logger.debug("PENTOROB: updateState (callback) 1: " + cb.getAction_IU_id() + " " + cb.getDuration() + " " + cb.isSuccessful());
		boolean success = false; //whether it was successfully done or not
		if (cb.getActionType().equals(Action.ABORT)){
			//TODO for now not checking in aborts into pending/facts etc.
			success = true;
			this.currentAction.setAborted(true);
			this.currentAction.setEndTime(getTime()); //means in next update will be able to overridden
		} else {
			boolean found = false;
			for (int i=0; i<pendingActions.size(); i++){ //find the corresponding action in PENDING
				
				if (cb.getAction_IU_id()==(pendingActions.get(i).getID())){
					found = true;
					if (!cb.isSuccessful()){ //didn't work, ignore it
						logger.debug("PENTOROB: updateState (callback) 1: " + cb.getAction_IU_id() + " failed to execute");
						pendingActions.remove(i); //gets popped off pending whether successful or not
						break;
					}
					if (currentAction!=null){
						completedActions.add(currentAction); //it worked, so the previous action is completed
					}
					//now push the action to current
					currentAction = pendingActions.get(i); //make the pending action current
					pendingActions.remove(i); //pop it off pending
					currentAction.makeOngoing();
					currentAction.setStartTime(cb.getTimeReceived());
					currentAction.setEndTime(cb.getTimeReceived() + (Math.round(cb.getActionDuration())));
					
					//simple logic to get the piece involved in this action, if any- optimisitc grounding
					String pud = "-1";
					String dud = "-1";
					//double confidence = 1;
					if (!currentAction.groundedIn().isEmpty()){
						FrameIU grin = ((FrameIU) currentAction.groundedIn().get(0));
						if (grin.getObjectID()!=null){
							pud = grin.getObjectID();
						}
						if (grin.getDestinationIDs()!=null){
							dud = grin.getDestinationIDs().get(0);
						}
						//confidence = Double.max(0.05,grin.getConfidence());
					}
					
					//now simple calculation of the new state based on the previous state and new current action
					//String actiontype = grin.getAction(); //optimistically getting this from GRIN
					String actiontype = cb.getActionType().toString(); //less optimistic- actual physical state
					Action action = cb.getActionType();
					switch (action){
						case MOVE:
							if (this.isHoldingObject()){
								actiontype+="_WITH_OBJECT";
							} else if (!pud.equals("-1")){
								//if (currentaction.getSameLevelLink()!=null&&
								//		((RobotActionIU) currentaction.getSameLevelLink()).getActiontype().equals(Action.DROP)){
								//	break;
								//}
								actiontype+="_TO_OBJECT";
							}
							break;
						default:
							break;
					}
					actiontype = actiontype.replace("_LOW","");
					//now do the state calculation
					//NB for chaining do not just look at current state but what the previous one is
					ArmState teststate = stateMachine.getResultingState(getArmState(),actiontype + ".START");
					//TODO need to change this?
					if (teststate==null) {
						//recurse till sucessful
						int timeleft = this.getTime()-this.getEndTimeOfLastCompletedAction();
						logger.debug(this.getLastCompletedAction());
						logger.debug(this.currentAction);
						logger.debug("PENTOROB: WARNING: executing anyway, but updateState (callback): " + 
									actiontype + ".START" + " from " + getArmState().toString() + " not possible ");
						if (timeleft>3){ //some tolerance in the calculation
							logger.error("FATAL problem: serious underestimation of timing.");
							System.exit(0);
						}
						//it's 3ms short, reset to where we were before and try again
						this.currentAction.makeUpcoming();
						this.pendingActions.add(this.currentAction);
						this.currentAction = this.completedActions.get(this.completedActions.size()-1);
						this.currentAction.setEndTime(this.getTime());
						this.completedActions.remove(this.completedActions.size()-1);
						this.updateState(false);
						return this.updateStateFromCallBackMessage(cb);
						
					}
					
					setArmState(teststate);
					setObjectUnderDiscussion(pud);
					setDestinationUnderDiscussion(dud);
					if (this.getArmState().equals(ArmState.GRABBING_OBJECT)||
							this.getArmState().equals(ArmState.DROPPING_OBJECT)){
						this.strokeTimeout(this.getCurrentAction().getStartTime()+(Math.round(cb.getStrokeDuration())));
					}
					//else if (this.getArmstate().equals(ArmState.MOVINGTOOBJECT)){
					//	//only checking time outs for moving to piece, some delay otherwise with thread
					//	this.checkTimeout(this.getCurrentaction().getEndtime(),confidence);
					//} else if (this.getArmstate().equals(ArmState.MOVINGWITHOBJECT)){
					//	this.checkTimeout(this.getCurrentaction().getEndtime(),confidence);
					//}
					
					
					success = true;
					break;
				}
			}
			if (found==false){
				gui.displayError("WARNING: updateState (callback): " + cb.getAction_IU_id() + " not found in pending");
			}
		}
		//get the scene updating
		
		if (getArmState().equals(ArmState.STATIONARY)){
			//scene.resetScene(); //TODO for now reset scene if stationary
			//this.scene.setUpdating(true);
		} 
		else {
			//this.scene.setUpdating(false);
		}
		if (this.game.inProgress()){
			//this.checkTimeout(this.currentaction.getEndtime());
		}
		gui.updateDGBdisplay();
		return success;
	}
	
	@Override
	public boolean isDropping(){
		return this.getArmState().equals(ArmState.DROPPING_OBJECT);
	}
	
	@Override
	public boolean isOverObject(){
		return this.getArmState().equals(ArmState.STATIONARY_OVER_OBJECT);
	}
	
	@Override
	public boolean isGrabbing(){
		return this.getArmState().equals(ArmState.GRABBING_OBJECT);
	}
	
	public Set<String> getPastStateFluents(int ms_ago){
		//get one of the last 5 states
		//the state stack will have the current state and its start time, and all previous ones
		for (int i=this.stateCache.size()-1; i>-1; i--){
			if (this.stateCache.get(i).getRight()<(this.getTime()-ms_ago)){
				return this.stateCache.get(i).getLeft();
			}
		}
		logger.debug("PENTOROB warning null previous state");
		return null;
	}
	
	//the control loop
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		
		List<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
		
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			
			if (iu instanceof FrameIU) {	
				logger.debug(iu);
				boolean test = false;
				if (test) {
					continue;
				}
				//logger.debug("PENTOROB: BEFORE UPDATE no cb:" + this.stateSummary());
				try {
					updateState(false); //update the state to clear the last actions if possible
				} catch (Exception e){
					logger.error("PENTOROB: ERROR: UPDATE STATE NO CB DID NOT WORK");
					logger.error(e.getMessage());
					logger.error(e.getCause().toString());
					for (StackTraceElement el : e.getStackTrace()){
						logger.error("PENTOROB: \t" + el.toString());
					}
				}
			
				//logger.debug("AFTER UPDATE no cb:" + this.stateSummary());
				//TODO revoking frames should be dealt with via ABORTs
				switch (edit.getType()) {
					case ADD:
						logger.debug("added IU");
						FrameIU siu = (FrameIU) iu;
						String ID = siu.getObjectID();
						String action = siu.getAction();
						Double confidence = Double.max(0.05,siu.getConfidence());
						
						if (action.equals("START")){
							logger.debug("PENTOROB: PENTOROB STARTING/RESETTING");
							RobotActionIU move = this.moveOutOfCameraView();
							move.groundIn(iu);
							this.pushToPending(move);
							newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
							break;
						} else if (action.equals("WAIT")){
							logger.debug("PENTOROB: PENTOROB GO BACK TO WAITING");
							RobotActionIU move = this.moveOutOfCameraView();
							move.groundIn(iu);
							this.pushToPending(move);
							newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
							break;
						} 
						else if (action.equals("ABORT")){
							//java.awt.Toolkit.getDefaultToolkit().beep(); //just to indicate revoke action has been sent
							if (this.isMovingXY()&&this.isTakingObject()){
								logger.debug("PENTOROB: PENTOROB ABORTING action!");
								//TODO this is not straight forward
								RobotActionIU move = null;
								if (ID.equals("-1")){
									move = moveOutOfCameraView(); //currently confidence always max to retreat to dwell position
								} else {
									double speed = this.getDisplayConfidence() ? confidence : 1.0;
									move = moveToObject(ID,speed);
									if (move!=null){
										logger.debug("PENTOROB: MOVE_TO_OBJECT " + move.toString());
										//RobotActionIU abort =  abortCurrentAction();
										//abort.groundIn(iu);
										//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
									} else {
										logger.debug("PENTOROB: WARNING: MOVE_TO_OBJECT " + ID + " not poss!");
									}
								}
								
								move.groundIn(iu);
								//this.pendingactions.clear();
								move.setInterruptive(true);
								this.pushToPending(move);
								this.updateState(false);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
								//setLastCommitTime(this.getTime()); //not needed for abort?
								
								
								//RobotActionIU abort =  abortCurrentAction();
								//abort.groundIn(iu);
								//end the current action (optimistic grounding)//allowing for new actions
								
								//this.pendingactions.clear();
								//this.updateState(false);
								//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
								
							} else 
							if (armState.equals(ArmState.STATIONARY_OVER_OBJECT)){
								logger.debug("PENTOROB: PENTOROB REVOKING choice!");
								RobotActionIU move = null;
								if (ID.equals("-1")){
									move = moveOutOfCameraView(); //currently confidence always max to retreat to dwell position
								} else {
									double speed = this.getDisplayConfidence() ? confidence : 1.0;
									move = moveToObject(ID,speed);
									if (move!=null){
										logger.debug("PENTOROB: MOVE_TO_OBJECT " + move.toString());
										//RobotActionIU abort =  abortCurrentAction();
										//abort.groundIn(iu);
										//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
									} else {
										logger.debug("PENTOROB: WARNING: MOVE_TO_OBJECT " + ID + " not poss!");
									}
								}
								
								move.groundIn(iu);
								//this.pendingactions.clear();
								this.pushToPending(move);
								this.updateState(false);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
								//setLastCommitTime(this.getTime()); //not needed for abort?
							}
							break;
							
						} else if (action.equals("ABORT_DESTINATION")){
							
							if (armState.equals(ArmState.MOVING_WITH_OBJECT)){
								logger.debug("PENTOROB: PENTOROB ABORTING action of destination and REVOKING!");
								String destinationID = siu.getDestinationIDs().get(0);
								logger.debug(destinationID);
								//RobotActionIU abort =  abortCurrentAction();
								//abort.groundIn(iu);
								//end the current action (optimistic grounding)//allowing for new actions
								//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
								RobotActionIU move = this.moveToCentreTopOfPlacementArea();
								move.setInterruptive(true);
								move.groundIn(iu);
								//this.pendingactions.clear(); 
								if (timeOutThread != null){
									if (inStroke){
									
										logger.debug("Timeout thread not started as in stroke timeout");
										
									} else {
										logger.debug("Timeout thread interrupted before new instructions");
										timeOutThread.interrupt();
									}
								}
								
								
								
								this.pushToPending(move);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
								//assuming this works, we end the current action now, also need to abort
								
								
								
							} else if (this.isOverTargetArea()){
								//get out the targets
								String destinationID = siu.getDestinationIDs().get(0);
								logger.debug("PENTOROB REVOKING DESTINATION " + destinationID);
								RobotActionIU move = this.moveToCentreTopOfPlacementArea();
								move.groundIn(iu);
								//this.pendingactions.clear();
								this.pushToPending(move);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
								
							}
							
							
							break;
						}
						/*if (this.getTimeSinceLastCommit()<updateRate){
							//TODO a general rule not to allow updates too often
							logger.debug("PENTOROB: Update too soon! " + this.getTime());
							logger.debug(this.stateSummary());
							break;
						}
						*/

						//now check if the action in the frame is executable in the current state
						//TODO or possible to queue it in pending
						//TODO we recursively rely on the fact that every action admitted in PENDING was possible in previous state
						
						
						action = action.replace("_LOW", "");
						String testaction = action.replace("_TO_TARGET","_WITH_OBJECT");
						if (action.equals("PICK")){
							//this is a higher level action, i.e. trying to enter a superstate PUTTING state
							testaction = "MOVE_TO_OBJECT";
						} else if (action.equals("PLACE")){
							testaction = "MOVE_WITH_OBJECT";
						} else if (action.equals("TAKE")){
							//higher level again, only admit possible actions
							testaction = "MOVE_TO_OBJECT";
						}
						
						//TODO currently only permitting permissible actions
						//from the current state (even if they are complex)
						//TODO a translation to state machine
						logger.debug(action);
						logger.debug(testaction);
						ArmState teststate = this.stateMachine.getResultingState(getArmState(), testaction + ".START");
						if (teststate==null){
							
							//only stipulation is PENDING has to be empty, otherwise we're doing more than look ahead 1
							//another action is already queued- future work, actually traverse pending for consistency/action chaining
							
							logger.debug("PENTOROB: WARNING: internal loop test: " + testaction + " (test), " + 
										action + " (actual) from " + getArmState().toString() + " not possible");
							
							if (lookAhead==0) {
								break;
							}
							else {
								logger.debug("PENTOROB: trying test again with from end of current action- LOOKAHEAD!");
								if (!this.getPendingActions().isEmpty()){
									break;
								}
								String testcurrentaction = currentAction.getActionType().toString(); //less optimistic- actual physical state
								
								switch (currentAction.getActionType()){
									case MOVE:
										if (this.isHoldingObject()){
											testcurrentaction+="_WITH_OBJECT";
										} else if (!this.getObjectUnderDiscussion().equals("-1")){
											//if (currentaction.getSameLevelLink()!=null&&
											//		((RobotActionIU) currentaction.getSameLevelLink()).getActiontype().equals(Action.DROP)){
											//	break;
											//}
											testcurrentaction+="_TO_OBJECT";
										}
										break;
									default:
										break;
								}
								testcurrentaction = testcurrentaction.replace("_LOW","");
								logger.debug(testcurrentaction);
								ArmState nextteststate = this.stateMachine.getResultingState(getArmState(), testcurrentaction + ".END");
								logger.debug(nextteststate);
								logger.debug(testaction + ".START");
								ArmState futurestate = this.stateMachine.getResultingState(nextteststate, testaction + ".START");
								logger.debug(futurestate);
								if (nextteststate==null||futurestate==null){
									logger.debug("didn't work second time");
									break;
								}
								logger.debug("PENTOROB: PENTOROB INTERNAL LOOP: SUCCEEDED WITH LOOKAHEAD");
							} 
							
						}
						//if we get here the action is executable
						setLastCommitTime(this.getTime());
						logger.debug("PENTOROB: \t" + ID + " " + action);
						
						if (action.equals("PICK")||action.equals("PLACE")){
							logger.debug("PENTOROB: PUTTING COMPLEX ACTION " + iu.toPayLoad().toString());
							//the instruction is to enter the superstate PUTTING //TODO need to change statemachine
							//this consists of a chain of actions:
							//MOVE_TO_OBJECT
							//GRAB
							//MOVE_TO_TARGET
							//THEN SHOULD END UP STATIONARY WITH OBJECT WAITING FOR 
							//CONFIRMATION
							//idea is to push all these to pending
							//then let the callbacks take care of making each one
							//the current action as they come in
							if ((this.pendingActions.isEmpty())||!this.pendingActions.get(this.pendingActions.size()-1).getActionType().toString().equals(testaction)) {
								RobotActionIU move_to_piece = moveToObject(ID,confidence);
								if (move_to_piece!=null){
									logger.debug("PENTOROB: MOVE_TO_OBJECT " + move_to_piece.toString());
									//RobotActionIU abort =  abortCurrentAction();
									//abort.groundIn(iu);
									//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
									move_to_piece.groundIn(iu);
									this.pushToPending(move_to_piece);
									newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move_to_piece));
								} else {
									logger.debug("PENTOROB: WARNING: MOVE_TO_OBJECT " + ID + " not poss!");
								}
								
								logger.debug("PENTOROB: PENTOROB GRAB OBJECT ITS OVER");
								//RobotActionIU abort =  abortCurrentAction();
								//abort.groundIn(iu);
								//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
								RobotActionIU grab =  grabObject();
								grab.groundIn(iu);
								this.pushToPending(grab);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, grab));
							}
							if (action.equals("PLACE")&&!this.pendingActions.get(this.pendingActions.size()-1).getActionType().toString().equals("MOVE")){
								logger.debug("PENTOROB: PENTOROB MOVE_WITH_OBJECT TO TARGET AREA");
								//the big test of incrementality- 3 actions to queue up
								
								Integer[] target = null;
								if (this.game instanceof PutInNumberedSlot){
									String destination = ((FrameIU) iu).getDestinationIDs().get(0);
									target = ((PutInNumberedSlot) this.game).getCentroidOfSlot(Integer.parseInt(destination)-1);
								} else if (this.game instanceof Explore){
									target = ((Explore) this.game).getFreeSpaceForObjectFromRelationAndReferenceObjects(
											this.scene,
											((FrameIU) iu).getObjectID(),
											((FrameIU) iu).getRelation(),
											((FrameIU) iu).getDestinationIDs());
											
								} else {
									logger.error("No target calculation for game " + this.game.getName());
								}
								
								RobotActionIU move = moveToCoordinates(target[0],target[1],0);
								if (move!=null){
									logger.debug("PENTOROB: MOVE_WITH_OBJECT " + move.toString());
									//RobotActionIU abort =  abortCurrentAction();
									//abort.groundIn(iu);
									//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
									move.groundIn(iu);
									this.pushToPending(move);
									newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
								} else {
									logger.debug("PENTOROB: WARNING: MOVE_WITH_OBJECT " + ID + " not poss!");
								}
							}
							
							
						} else if (action.equals("MOVE_TO_TARGET")){
							logger.debug("PENTOROB: PENTOROB MOVE_WITH_OBJECT TO TARGET AREA");
							//the big test of incrementality- 3 actions to queue up
							Integer[] target = null;
							if (this.game instanceof PutInNumberedSlot){
								String destination = ((FrameIU) iu).getDestinationIDs().get(0);
								target = ((PutInNumberedSlot) this.game).getCentroidOfSlot(Integer.parseInt(destination)-1);
							} else if (this.game instanceof Explore){
								target = ((Explore) this.game).getFreeSpaceForObjectFromRelationAndReferenceObjects(
										this.scene,
										((FrameIU) iu).getObjectID(),
										((FrameIU) iu).getRelation(),
										((FrameIU) iu).getDestinationIDs());
										
							} else {
								logger.error("No target calculation for game " + this.game.getName());
							}
							
							RobotActionIU move = moveToCoordinates(target[0],target[1],0);
							if (move!=null){
								logger.debug("PENTOROB: MOVE_WITH_OBJECT " + move.toString());
								//RobotActionIU abort =  abortCurrentAction();
								//abort.groundIn(iu);
								//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
								move.groundIn(iu);//this gives the destination under disucssion
								this.pushToPending(move);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
							} else {
								logger.debug("PENTOROB: WARNING: MOVE_WITH_OBJECT " + ID + " not poss!");
							}
						} else if (action.equals("DROP")){
							RobotActionIU drop = dropObject();
							drop.groundIn(iu);
							//drop.addNextSameLevelLink(move);
							this.pushToPending(drop);
							newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, drop));
							
							
							logger.debug("PENTOROB: MOVE OUT OF WAY!");
							RobotActionIU moveaway = this.moveOutOfCameraView();
							FrameIU moveoutofwayiu = new FrameIU("MOVE_OUT_OF_VIEW","-1",1.0); // TODO for now creating fake frame
							moveoutofwayiu.setObjectID("-1");
							moveaway.groundIn(moveoutofwayiu);
							moveaway.addNextSameLevelLink(drop); //to show the two actions are joined
							this.pushToPending(moveaway);
							newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, moveaway));
							
							//scene.resetScene();
		
						} else if (action.equals("MOVE_TO_OBJECT")){
							logger.debug("PENTOROB Recieved Moving to piece request");
							double speed = this.getDisplayConfidence() ? confidence : 1.0;
							RobotActionIU move = moveToObject(ID,speed);
						
							if (move!=null){
								logger.debug("PENTOROB: MOVE_TO_OBJECT " + move.toString());
								//RobotActionIU abort =  abortCurrentAction();
								//abort.groundIn(iu);
								//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
								move.groundIn(iu);
								this.pushToPending(move);
								newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, move));
							} else {
								logger.debug("PENTOROB: WARNING: MOVE_TO_OBJECT " + ID + " not poss!");
							}
							
						} else if (action.equals("GRAB")) {
							logger.debug("PENTOROB: PENTOROB GRAB OBJECT ITS OVER");
							//RobotActionIU abort =  abortCurrentAction();
							//abort.groundIn(iu);
							//newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, abort));
							RobotActionIU grab =  grabObject();
							grab.groundIn(iu);
							this.pushToPending(grab);
							newEdits.add(new EditMessage<RobotActionIU>(EditType.ADD, grab));
						} else {
							logger.debug("PENTOROB: PENTOROB UNKNOWN ACTION " + action);
						}
						
						break;
					case COMMIT:
						break;
					case REVOKE:
						break;
					default:
						break;
					
				}
				//this guarantees pentorob is receiving commands, get rid of the timeout thread
				//though timeout thread can be a parent of a stroke thread
				if (timeOutThread != null){
					if (inStroke){
					
						logger.debug("Timeout thread not started as in stroke timeout");
						
					} else {
						logger.debug("Timeout thread interrupted before new instructions");
						timeOutThread.interrupt();
					}
				}
				
			} else if (iu instanceof SensorIU) {
				java.awt.Toolkit.getDefaultToolkit().beep();
				//TODO timeouts directly from PentoRob can be passed here
				//not callback format, payload == timeout_trigger.
				//logger.debug("PENTOROB: BEFORE UPDATE cb:" + this.stateSummary());
				try{
					updateState(false); //TODO currently needs to do this to check chained actions
					//doesn't need to pass it on as IU but state available to other modules
					boolean success = updateStateFromCallBackMessage(new CallBack(((SensorIU) iu).toPayLoad(),this.getTime()));
					if (success){
						logger.debug(System.currentTimeMillis());
						logger.debug(this.getTime());
						logger.debug("PENTOROB CURRENT ACTION:");
						double confidence = 1.0;
						if (this.getCurrentAction()!=null){
							logger.debug(this.getCurrentAction().toPayLoad());
							if (!currentAction.groundedIn().isEmpty()){
								FrameIU grin = ((FrameIU) currentAction.groundedIn().get(0));
								logger.debug("grounded in " + grin.toPayLoad());
								confidence = grin.getConfidence();
								logger.debug(confidence);
							}
						} else {
							logger.debug("null");
						}
						logger.debug("PENTOROB CURRENT STATE:");
						logger.debug(this.getArmState());
						logger.debug(this.getStateFluents());
						
						if (this.getArmState().equals(ArmState.MOVING_TO_OBJECT)){
							//only checking time outs for moving to piece, some delay otherwise with thread
							double waitingTimePenalty = this.getDisplayConfidence() ? confidence : 0.2;
							this.checkTimeout(this.getCurrentAction().getEndTime(),waitingTimePenalty);
						} else if (this.getArmState().equals(ArmState.MOVING_WITH_OBJECT)){
							double waitingTimePenalty = this.getDisplayConfidence() ? confidence : 0.2;
							this.checkTimeout(this.getCurrentAction().getEndTime(),waitingTimePenalty);
						}
						//logger.debug(this.get)
						
						//pass on the Callback IU
						//newEdits.add(new EditMessage<TimeOutIU>(EditType.ADD, new TimeOutIU(this.getTimeofendofcurrentaction())));
						//checkTimeout(this.getTimeofendofcurrentaction());
					}
				} catch (Exception e){
					logger.debug("PENTOROB: ERROR: UPDATE STATE CB DID NOT WORK");
					logger.debug(e.getMessage());
					for (StackTraceElement el : e.getStackTrace()){
						logger.debug("PENTOROB: \t" + el.toString());
					}
				}	
			}
		}
		
		rightBuffer.setBuffer(newEdits);		
	}

}