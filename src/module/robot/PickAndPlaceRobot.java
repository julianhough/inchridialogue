package module.robot;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Boolean;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;
import module.game.Explore;
import module.game.Game;
import module.game.PutInNumberedSlot;
import module.iu.RobotActionIU;
import module.vision.SceneUpdateModule;
import util.Pair;

public abstract class PickAndPlaceRobot extends Robot {
	
	@S4Component(type = SceneUpdateModule.class)
	public final static String SCENE = "scene";
	protected SceneUpdateModule scene;
	
	@S4String(defaultValue = "")
	public final static String GAME = "game";
	
	@S4String(defaultValue = "")
	public final static String STATE_MACHINE_FILE = "state_machine_file";
	@S4Integer(defaultValue = 0)
	public final static String LOOKAHEAD = "lookahead";
	protected int lookAhead;
	
	@S4Integer(defaultValue = -1)
	public final static String TIMEOUT = "timeout";
	protected int timeOut; //number of seconds after last pentorob action after which to do something
	protected Thread timeOutThread;
	protected Thread strokeTimeOutThread; //a critical uninterruptable thread that must not be stopped
	
	@S4Integer(defaultValue = -1)
	public final static String NUMBER_OF_OBJECTS = "number_of_objects";
	
	@S4Boolean(defaultValue = false)
	public final static String DISPLAY_CONFIDENCE = "display_confidence";
	protected Boolean displayConfidence;
	
	//simple state variables which can be accessed by NLU and other interested modules
	//protected ArmState armState; //what the arm is doing currently
	protected ArrayList<Pair<Set<String>,Integer>> stateCache; //a list of the previous 5 states and the time they were entered
	protected String objectUnderDiscussion; //the piece in focus. Note the name- this is the public/visible piece that is being worked on
	protected String destinationUnderDiscussion;//the location in focus
	protected RobotActionIU currentAction;
	protected List<RobotActionIU> pendingActions;
	protected List<RobotActionIU> completedActions;
	protected Game game; //the game being played and its state
	protected boolean holdingObject; //whether holding piece or not
	protected boolean overTargetArea; //whether over the target area or not
	protected boolean inStroke; //whether in the middle of a stroke or not
	
	public UserGUI gui;

	protected int lastCommitTime = 0; //millis
	// protected int updateRate = 1000; //millis when action is sent

	protected int endTime; //just a marker of the finishing time of the current action
	protected int strokeEndTime; //just a marker of the finishing stroke time of the current action
	
	@Override
	public void newProperties(PropertySheet ps) {
		super.newProperties(ps);
		scene = (SceneUpdateModule) ps.getComponent(SCENE); //in this class, scene is queried rather than reset
		//stateMachine = new StateMachine<ArmState>(ps.getString(STATE_MACHINE_FILE),ArmState.values());
		timeOut = ps.getInt(TIMEOUT);
		lookAhead = ps.getInt(LOOKAHEAD);
		displayConfidence = ps.getBoolean(DISPLAY_CONFIDENCE);
		pendingActions = new ArrayList<RobotActionIU>();
		completedActions = new ArrayList<RobotActionIU>();
		currentAction = null;
		
		String gameName = ps.getString(GAME);
		if (gameName.equals("PutInNumberedSlot")){
			game = new PutInNumberedSlot(
					ps.getInt(NUMBER_OF_OBJECTS),
					scene.getTopLeftCornerOfScene(),
					scene.getBottomRightCornerOfScene());
		} else {
			game = new Explore();
		}
		
		stateCache = new ArrayList<Pair<Set<String>,Integer>>();
		//armState = ArmState.STATIONARY;
		holdingObject = false;
		overTargetArea = false;
		inStroke = false;
		objectUnderDiscussion = "-1";
		destinationUnderDiscussion = "-1";
		lastCommitTime = this.getTime();
		gui = new UserGUI(this);
	}


	public abstract boolean isWaiting();
	
	public abstract boolean isWaitingForObject();
	
	public abstract boolean isMovingXY();
	
	public abstract boolean isDropping();
	
	public abstract boolean isPlacingObject();

	public abstract boolean isOverObject();

	public abstract boolean isHoldingObject();

	public abstract boolean isGrabbing();
	
	public abstract boolean isOverTargetArea();
	
	public abstract boolean isTakingObject();
	
	public abstract boolean isWaitingForLocation();

	public abstract String getDestinationUnderDiscussion();

	public abstract String getObjectUnderDiscussion();
	
	@Override
	public Set<String> getStateFluents(){
		/*
		 * Fluents as strings which are required for the state machines
		 * And also store these in the previous states.
		 */
		Set<String> fluents = new HashSet<String>();
		if (this.isWaiting()){
			fluents.add("ROBOT.WAITING");
		}
		if (this.isWaitingForObject()){
			fluents.add("ROBOT.WAITING_FOR_OBJECT");
		}
		if (this.isMovingXY()){
			fluents.add("ROBOT.MOVING_XY");
		}
		if (this.isDropping()){
			fluents.add("ROBOT.DROPPING");
		}
		if (this.isPlacingObject()){
			fluents.add("ROBOT.PLACING");
		}
		if (this.isOverObject()){
			fluents.add("ROBOT.OVER_OBJECT");
		}
		if (this.isHoldingObject()){
			fluents.add("ROBOT.HOLDING_OBJECT");
		}
		if (this.isGrabbing()){
			fluents.add("ROBOT.GRABBING");
		}
		if (this.isOverTargetArea()){
			fluents.add("ROBOT.OVER_DESTINATION");
		}
		if (this.isTakingObject()){
			fluents.add("ROBOT.TAKING");
		}
		if (this.isWaitingForLocation()){
			fluents.add("ROBOT.WAITING_FOR_DESTINATION");
		}
		if (this.game.completed()){
			fluents.add("GAME.OVER");
		}
		if (this.currentGoalActionLegible()){
			fluents.add("ROBOT.CURRENT_ACTION_LEGIBLE");
		}
		fluents.add("ROBOT_OBJECT." + this.getObjectUnderDiscussion());
		fluents.add("ROBOT_DESTINATION." + this.getDestinationUnderDiscussion());
		return fluents;
	}


}
