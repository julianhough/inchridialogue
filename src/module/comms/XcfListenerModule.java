package module.comms;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4String;
import inpro.io.ListenerModule;
import net.sf.xcf.ActiveMemory;
import net.sf.xcf.InitializeException;
import net.sf.xcf.XcfManager;
import net.sf.xcf.event.MemoryEvent;
import net.sf.xcf.event.MemoryEventAdapter;
import net.sf.xcf.memory.MemoryException;
import net.sf.xcf.naming.NameNotFoundException;
import net.sf.xcf.xml.XPath;

/**
 * @author julianhough
 *
 */
public class XcfListenerModule extends ListenerModule  {

	static Logger log = Logger.getLogger(XcfListenerModule.class.getName());
 	
 	@S4String(defaultValue = "")
	public final static String XPATH = "xpath";	 
 	
 	@S4String(defaultValue = "")
	public final static String MEMORY_NAME = "memory";
 	
 	public XPath xpathQuery;
	public XcfManager xm;
	public ActiveMemory am;
	public MemoryEventAdapter adapter;
	private final BlockingQueue<MemoryEvent> queue =
            new LinkedBlockingQueue<MemoryEvent>();
	
	
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		//String query = "/*[(not(@name) or (@name!='WorldModelUpdate' and @name!= 'checkGrasp')) and name()!='pointing' and name()!='HSM_INFO']";
		String query = ps.getString(XPATH);
		//System.out.println(query);
		try {
			xm = XcfManager.createXcfManager();
		} catch (InitializeException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			am = xm.createActiveMemory(ps.getString(MEMORY_NAME));
		} catch (InitializeException | NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			xpathQuery = new XPath(query);
		} catch (Exception e){
			logger.debug("Failed to create XPath");
			e.printStackTrace();
		}
		
		adapter = new MemoryEventAdapter(xpathQuery) {
            @Override
            public void handleEvent(MemoryEvent e) {
                try {
                    queue.put(e);
                    getNextString();
                    
                    
                } catch (InterruptedException ex) {
                    System.out.println("Interrupted while adding Memory " +
                            "input to queue: " + ex);
                }
            }
        };
        
        try {
			am.addListener(adapter);
		} catch (MemoryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		logger.info("Listening to XCF query: " + query);

	}
	
	 public boolean hasNext() {
	 		// We can just pass this through to the queue
	 		return !queue.isEmpty();
	 	}
		 
    public String getNextString() {
        // Get the next event from the memory
        MemoryEvent event = queue.poll();

        // If there was no event, there is no input... :)
        if (event == null) {
            return null;
        }

        logger.debug("Received input from memory: " +
                 event.getData().getDocumentAsText());
        String data = event.getData().getDocumentAsText();
        this.process(data);
        // Create a UserInput from this event:
        //return new XOPData(event.getData()).getDocument();
        return data;
    }

	

}
