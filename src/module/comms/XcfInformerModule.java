package module.comms;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4String;
import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import net.sf.xcf.ActiveMemory;
import net.sf.xcf.InitializeException;
import net.sf.xcf.XcfManager;
import net.sf.xcf.event.MemoryEvent;
import net.sf.xcf.event.MemoryEventAdapter;
import net.sf.xcf.memory.MemoryException;
import net.sf.xcf.naming.NameNotFoundException;
import net.sf.xcf.transport.XOPData;
import net.sf.xcf.xml.SyntaxException;
import net.sf.xcf.xml.XPath;


/**
 * @author julianhough
 *
 */
public class XcfInformerModule extends IUModule {
	
	static Logger log = Logger.getLogger(XcfInformerModule.class.getName());
	
 	//@S4String(defaultValue = "")
	//public final static String XPATH = "xpath";	 
 	
 	@S4String(defaultValue = "")
	public final static String MEMORY_NAME = "memory";
 	
 	public XPath xpathQuery;
	public XcfManager xm;
	public ActiveMemory am;
	public MemoryEventAdapter adapter;
	private final BlockingQueue<MemoryEvent> queue =
            new LinkedBlockingQueue<MemoryEvent>();
	
	
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		//String query = "/*[(not(@name) or (@name!='WorldModelUpdate' and @name!= 'checkGrasp')) and name()!='pointing' and name()!='HSM_INFO' and name()!='objects']";
		//String test = ps.getString(XPATH);
		//System.out.println(test);
		//xpathQuery = new XPath(query);
		try {
			xm = XcfManager.createXcfManager();
		} catch (InitializeException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			am = xm.createActiveMemory(ps.getString(MEMORY_NAME));
		} catch (InitializeException | NameNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
 	
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		//Simply put the payload of any IU onto the scope
		for (EditMessage<? extends IU> edit : edits) {
			if (edit.getType().isCommit()) continue;
			try {
				am.insert(XOPData.fromString(edit.getIU().toPayLoad()));
			} catch (MemoryException | SyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}



}

