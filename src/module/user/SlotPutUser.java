package module.user;

import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import module.game.Game;
import module.game.PutInNumberedSlot;
import module.iu.FrameIU;
import module.iu.MessageIU;
import module.robot.ArmState;
import module.robot.PentoRob;
import module.robot.PickAndPlaceRobot;
import module.robot.Robot;
import module.vision.SceneUpdateModule;
import sium.iu.SlotIU;
import sium.module.ResolutionModuleWAC;
import sium.nlu.stat.Distribution;
import util.Stats;
import util.Pair;
import edu.cmu.sphinx.util.props.Configurable;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Boolean;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4Double;
import edu.cmu.sphinx.util.props.S4Integer;
import edu.cmu.sphinx.util.props.S4String;

/**
 * The principal interpretation module of PentoLand of the user's state.
 * Uses the REFRES classifier decisions and other logic for incremental reference resolution decision.
 * It has basic action interpretation and location/destination understanding through key word spotting.
 * It reasons in a 'closed' loop way, in that it classifies the user's state using knowledge of the current state of PentoRob's arm.
 * i.e. It behaves according to what PentoRob is showing/grounding (a la Clark 1996).
 */
public class SlotPutUser extends User {

	
	//callable components from elsewhere in the system and/or variables
	@S4Component(type = ResolutionModuleWAC.class)
	public final static String WAC_PROP = "WAC";
	ResolutionModuleWAC refRes;
	
	@S4Boolean(defaultValue = true)
	public final static String INCREMENTAL = "incremental";
	private boolean incremental; //whether we wait for all slots to be filled PLACE(X,Y) or move after each part (i.e. TAKE(X) then PLACE(X,Y)).
	
	@S4Double(defaultValue = 0.03)
	public final static String THRESHOLD = "threshold";
	private double threshold; //the margin over the second rank above which a selection decision is made (for pieces and locations)
	
	
	//user state variables for simple two slot grammar of piece + destination
	private UserActionState userState; //what the user is currently 'doing', consistent with the state machine
	//pieces
	private String selectedPiece; //the currently selected piece, default -1 if nothing currently selected
	private Distribution<String> pieceDistribution; //the distribution over the pieces
	private ArrayList<Distribution<String>> pieceDistributionStack; //the stored distributions over pieces for each word processed
	private List<String> rejectedPieces; //the pieces currently ruled out based on rejection/repair for the current game move
	//destinations
	private String selectedDestination; //the currently selected location, default "-1" if nothing currently selected
	private Distribution<String> destinationDistribution; //the distribution over the destinations based on the words, likely to just either uniform or 0 entropy
	private List<String> rejectedDestinations; //the destinations currently ruled out based on rejection/repair for the current game move
	private ArrayList<Distribution<String>> destinationDistributionStack;
	
	/** The edits to be passed on when leftBufferUpdate is called */
	public List<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		logger.info("Setting up SlotPut NLU");
		
		refRes = (ResolutionModuleWAC) ps.getComponent(WAC_PROP);
		incremental = ps.getBoolean(INCREMENTAL);
		threshold = ps.getDouble(THRESHOLD);
		
		////initialize the state vars
		userState = UserActionState.START;
		
		//pieces
		this.initObjects();
		
		//destinations
		this.initDestinations();
		
		//useful logging info
		logger.info("NLU STATE MACHINE: " + ps.getString(STATE_MACHINE_FILE));
		logger.info("THRESHOLD: " + this.threshold);

	}
	
	public void initObjects() {
		System.out.println("Resetting pieces.");
		selectedPiece = "-1";
		pieceDistribution = Stats.uniformDistribution(new HashSet<String>(((PutInNumberedSlot) this.getGame()).getPiecesInPlay()));
		rejectedPieces = new ArrayList<String>();
		pieceDistributionStack = new ArrayList<Distribution<String>>();	
	}
	
	public void initDestinations() {
		System.out.println("Resetting destinations.");
		selectedDestination = "-1";
		destinationDistribution = Stats.uniformDistribution(new HashSet<String>(((PutInNumberedSlot) this.getGame()).getPositionsInPlay()));
		rejectedDestinations = new ArrayList<String>();
		destinationDistributionStack = new ArrayList<Distribution<String>>();
	}
	
	public void initReferenceResolution(){
		System.out.println("Resetting Reference Resolution.");
		//System.out.println("Current refres list" + wordStack.toString());
		currentDialogueActWordStack = new ArrayList<Pair<WordIU, String>>();
		refRes.setIgnore(); //turn ref res off
		//turn Refres back on and remove the CV outline
		refRes.reset();
		refRes.setListen();
		this.initDestinations();
		this.initObjects();
	}
	
	@Override
	public void setRobot(Configurable a_robot) {
		robot = (PentoRob) a_robot;
	}
	
	@Override
	public PickAndPlaceRobot getRobot(){
		return (PickAndPlaceRobot) robot;
	}
	
	@Override
	public void setGame(Robot a_robot) {
		game = (PutInNumberedSlot) a_robot.getGame();
	}
	
	@Override
	public PutInNumberedSlot getGame(){
		return (PutInNumberedSlot) game;
	}
	
	public UserActionState getUserActionState() {
		return userState;
	}

	public void setUserActionState(UserActionState userState) {
		this.userState = userState;
	}
	
	@Override
	protected synchronized void processAddedWordIU(WordIU iu) {
		super.processAddedWordIU(iu);
		
		this.getRobot().updateState(false);
		//interpret the instruction based on the state when the word actually came in due to latency
		//Set<String> fluents = this.pentorob.getPastStateFluents(200);
		//
		//or do it for the current state:
		Set<String> fluents = this.getRobot().getStateFluents();

		double duration = iu.endTime() - iu.startTime();
		ArrayList<String> wordhyps = wordUtil.normalizeFromASRNBest(iu.toPayLoad().toLowerCase());
		String word = wordUtil.get1Best(wordhyps, ((PutInNumberedSlot) this.getGame()).getPositionsInPlay());
		System.out.println("**********************************");
		System.out.println(System.currentTimeMillis());
		System.out.println(this.getTime());
		System.out.println("WORD: " + word);
		System.out.println("duration");
		System.out.println(duration);
		System.out.print("REFRES:");
		System.out.println(pieceDistribution.toString());
		System.out.print("DESTINATION:");
		System.out.println(destinationDistribution.toString());
		System.out.print("PENTOROB state:");
		System.out.println(fluents);
		this.pieceDistributionStack.add(pieceDistribution); //as ref res processes word before this NLU module
		System.out.println("*****");
		
		
		//Using the user statemachine and the information from the new word (and pentorob state)
		//classify the user move/d-act (and by default get the userstate resulting from this)
		//depending on the policy (non-inc vs. semi-inc vs. continuous)
		//different FrameIUs will be sent to PentoRob (at different times)
		Pair<Boolean, DialogueAct> pair = DA.recognize(
				this.getTime()-this.getTimeSinceLastWordEnd(),
				this.pieceDistributionStack,
				word, 
				this.currentDialogueActWordStack, 
				this.getUserActionState(),
				fluents,
				((PutInNumberedSlot) this.getGame()).getPiecesInPlay(),
				((PutInNumberedSlot) this.getGame()).getPositionsInPlay(),
				incremental);
		boolean newmove = pair.getLeft();
		DialogueAct movetype = pair.getRight();

		this.currentDialogueActWordStack.add(new Pair<WordIU, String>((WordIU) iu,word));
		this.setLastWordEndTime(this.getTime()); //Assumption that this is the end time of word- need to adjust for Google latency
		
		if (movetype==null){
			System.out.println("NLU WARNING no movetype");
			//movetype = MoveType.INSTRUCT_OBJECT; //TODO for now assume default
			movetype = this.currentDialogueAct;
			newmove = false;
			return; //do not pass this on to PRob
		}
		System.out.println(movetype.toString() + " " + String.valueOf(newmove));

		//update the state based on the fluents from the time the word ended
		
		
		boolean success = updateState(movetype, fluents);
		if (!success){
			System.out.println("NLU WARNING no state update");
			return;
		}
		System.out.println("NLU USERSTATE: " + userState);
		
		//TODO for now just deal with instructions (subsuming repairs, just in terms of argmax change) and rejects/confirms
		if (userState.equals(UserActionState.REPAIRING_OBJECT)){
			setUserActionState(UserActionState.INSTRUCTING_OBJECT);
		} else if (userState.equals(UserActionState.REPAIRING_DESTINATION)){
			setUserActionState(UserActionState.INSTRUCTING_DESTINATION);
		}
		
		FrameIU selectIU = null; // The frame to be filled if it gets passed on
		switch (userState){
		case INSTRUCTING_OBJECT:
			if (pieceDistribution.isEmpty()){
				System.out.println("WARNING no allowable pieces during instruction.");
				break;
			}
			double margin = 1.0; //the egregious case of only one object in the distribution
			String argmaxpiece = this.pieceDistribution.getArgMax().getEntity();
			if (pieceDistribution.size()>1){ //else get its margin over nearest distractor
				//simple:
				margin = pieceDistribution.getMargins().get(0);
				//more complex: ratio of max prob over the combined mass of the top 2
				margin = pieceDistribution.getConfidence() / (pieceDistribution.getConfidence() + pieceDistribution.getConfidence()-margin);
				margin = 1 + (Math.log(margin) / Math.log(2));
			}
			System.out.println("NLU time since last goal movement: "  + this.getRobot().getTimeSinceLastGoalMovement());
			System.out.println("NLU piece margin: " + margin);
			//NB changing this from selectedPiece.equals("-1")
			if (this.getRobot().getTimeSinceLastGoalMovement()>15000){
				System.out.println("NLU Waited too long, faking threshold");
				margin = threshold+0.00001;
			}
			if (margin > threshold&!selectedPiece.equals(argmaxpiece)) { //TODO  && this.scene.isUpdating() only allow after resets
				System.out.println("NLU FRAME for NEW argmax piece: " + this.pieceDistribution.getArgMax().getEntity());
				selectedPiece = argmaxpiece;
			}
			//now check whether the piece under discussion is this, or we need to repair/change
			if ((!this.getRobot().getObjectUnderDiscussion().equals(selectedPiece))&&
					!selectedPiece.equals("-1")&&margin>threshold){
				//this.scene.setUpdating(false); //stop it updating until next time
				if (incremental){
					
					selectIU = new FrameIU(selectedPiece,"MOVE_TO_OBJECT",margin);
					if (this.currentFrame.payloadEquals(selectIU)
						&&((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_TO_OBJECT)
							&&this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)){
						System.out.println("Already commited! sent ");
						System.out.println(selectIU);
						break;
					}
					this.currentFrame = selectIU;
					newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
					System.out.println("NLU Sending MOVE_TO_OBJECT to pentorob");
				} else {
					if (!selectedDestination.equals("-1")){
						ArrayList<String> dest = new ArrayList<String>();
						dest.add(selectedDestination);
						selectIU = new FrameIU(selectedPiece,"PLACE",dest,1.0);
						if (this.currentFrame.payloadEquals(selectIU)
							&&((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_TO_OBJECT)
								&&this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)){
							System.out.println("Already commited! ");
							System.out.println(selectIU);
							break;
						}
						this.currentFrame = selectIU;
						newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
						System.out.println("NLU Sending PLACE to pentorob");
					}
				}
			}
			break;
		case REPAIRING_OBJECT:
			
			ArrayList<String> subset = new ArrayList<String>();
			rejectedPieces.add(this.getRobot().getObjectUnderDiscussion());
			for (String piece : ((PutInNumberedSlot) this.getGame()).getPiecesInPlay()){
				if (!rejectedPieces.contains(piece)){
					subset.add(piece);
				}
			}
			pieceDistribution = Stats.adjustedDistributionForSubset(pieceDistribution,subset);
			if (pieceDistribution.isEmpty()) {
				System.out.println("WARNING no allowable pieces after prune.");
				return;
			}
			margin = 1.0; //the egregious case of only one object in the distribution
			if (pieceDistribution.size()>1){ //else get its margin over nearest distractor
				//simple:
				margin = pieceDistribution.getMargins().get(0);
				//more complex: ratio of max prob over the combined mass of the top 2
				margin = pieceDistribution.getConfidence() / (pieceDistribution.getConfidence() + pieceDistribution.getConfidence()-margin);
				margin = 1 + (Math.log(margin) / Math.log(2));
			}
			System.out.println("NLU time since last goal movement: " + this.getRobot().getTimeSinceLastGoalMovement());
			System.out.println("NLU piece margin: " + margin);
			if (this.getRobot().getTimeSinceLastGoalMovement()>15000){
				System.out.println("NLU Waited too long, faking threshold");
				margin = threshold+0.00001;
			}
			//this replaces the old argmax, so includes repair as well as selecting anew
			if (margin > threshold) { //TODO  && this.scene.isUpdating() only allow after resets
				System.out.println("REPAIRED FRAME for argmax piece: " + this.pieceDistribution.getArgMax().getEntity());
				selectedPiece = this.pieceDistribution.getArgMax().getEntity();
				//this.scene.setUpdating(false); //stop it updating until next time
				if (incremental){
					selectIU = new FrameIU(selectedPiece,"MOVE_TO_OBJECT",margin);
					if (this.currentFrame.payloadEquals(selectIU)
						&&((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_TO_OBJECT)
							&&this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)){
						System.out.println("Already commited! ");
						System.out.println(selectIU);
						break;
					}
					this.currentFrame = selectIU;
					newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
				} else {
					if (!selectedDestination.equals("-1")){
						ArrayList<String> dest = new ArrayList<String>();
						dest.add(selectedDestination);
						selectIU = new FrameIU(selectedPiece,"PLACE",dest,1.0);
						if (this.currentFrame.payloadEquals(selectIU)
							&&((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_TO_OBJECT)
								&&this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)){
							System.out.println("Already commited! ");
							System.out.println(selectIU);
							break;
						}
						this.currentFrame = selectIU;
						newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
					}
				}
				
			} else {
				//if not over threshold, give the one in play another chance
				rejectedPieces.remove(this.getRobot().getObjectUnderDiscussion());
			}
			
			break;
		case REJECTING_OBJECT:
			/*//strategy 1. Abandon the current piece and start again
		  	selectedpiece = "-1";
			*/
			//strategy 2. Go to the next rank down (inference from negation)
			//we need to know what the rejection is referring to
			//needs to work for both the piece and location and both (PLACE)
			//in the incremental case,
			//can abort either the taking part, or the placing part
			//this can only happen in the incremental one, one can only repair/restart in the non-inc one
			subset = new ArrayList<String>();
			rejectedPieces.add(this.getRobot().getObjectUnderDiscussion());
			for (String piece : ((PutInNumberedSlot) this.getGame()).getPiecesInPlay()){
				if (!rejectedPieces.contains(piece)){
					subset.add(piece);
				}
			}
			pieceDistribution = Stats.adjustedDistributionForSubset(pieceDistribution,subset);
			if (pieceDistribution.isEmpty()) {
				System.out.println("WARNING no allowable pieces after prune.");
				return;
			}
			//System.out.println("ABORTED entropy");
			//System.out.println(pieceDistribution.toString());
			//System.out.println(pieceDistribution.getEntropy());
			margin = 1.0; //the egregious case of only one object in the distribution
			if (pieceDistribution.size()>1){ //else get its margin over nearest distractor
				//simple:
				margin = pieceDistribution.getMargins().get(0);
				//more complex: ratio of max prob over the combined mass of the top 2
				margin = pieceDistribution.getConfidence() / (pieceDistribution.getConfidence() + pieceDistribution.getConfidence()-margin);
				margin = 1 + (Math.log(margin) / Math.log(2));
			}
			System.out.println("NLU time since last goal movement: "  + this.getRobot().getTimeSinceLastGoalMovement());
			System.out.println("NLU piece margin: " + margin);
			selectedPiece = "-1";
			if (margin > threshold) { 
				System.out.println("After rejection, FRAME for argmax piece: " + pieceDistribution.getArgMax().getEntity());
				selectedPiece = pieceDistribution.getArgMax().getEntity();
			}
			selectIU = new FrameIU(selectedPiece,"ABORT",margin); //even in rejects only allow the margin worth of confidence 
			if (this.currentFrame.payloadEquals(selectIU)&&
					this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)){
				System.out.println("Already commited! ");
				System.out.println(selectIU);
				break;
			}
			this.currentFrame = selectIU;
			newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
			break;
		case CONFIRMING_OBJECT:
			System.out.println("NLU requesting PICK");
			selectIU = new FrameIU(this.getRobot().getObjectUnderDiscussion(),"PICK",1.0); 
			if (this.currentFrame.payloadEquals(selectIU)&&
					((PentoRob) this.getRobot()).getArmState().equals(ArmState.GRABBING_OBJECT)){
				System.out.println("Already commited! ");
				System.out.println(selectIU);
				break;
			}
			this.currentFrame = selectIU;
			newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
			break;
		case INSTRUCTING_DESTINATION:
			//very basic NLU here through key word spotting
			ArrayList<String> mypositions = ((PutInNumberedSlot) this.getGame()).getPositionsInPlay();
			System.out.println("positions in play");
			System.out.println(mypositions);
			boolean relevantupdate = false;
			
			if (destinationDistribution.isEmpty()) {
				System.out.println("WARNING no allowable locations after prune. Resetting.");
				selectedDestination = "-1";
				destinationDistribution = Stats.uniformDistribution(new HashSet<String>(((PutInNumberedSlot) this.getGame()).getPositionsInPlay()));
				rejectedDestinations.clear();
			}
			for (String targetpos : mypositions){
				int targetasnumber = Integer.parseInt(targetpos);
				String targetasword = wordUtil.convertInteger(targetasnumber); 
				
				if (word.equals(targetpos)|
						word.equals(wordUtil.ordinalFromStringInt(targetpos))|
						word.equals(wordUtil.romanFromStringInt(targetpos))|
						word.equals(targetasword)){
					
					//solution is to double the likelihood of the target and renormalize
					//System.out.println("location reference successful");
					destinationDistribution.setProbabilityForItem(targetpos, 2 * destinationDistribution.getProbabilityForItem(targetpos));
					destinationDistribution.normalize();
					relevantupdate = true;
					
					
				}
			}
			if (!relevantupdate){
				break;
			}
			System.out.println(destinationDistribution);
			//fairly pointless final margin step as only one match above (no ambiguity), but keeps things probabilistic
			double location_margin = 1.0; //the egregious case of only one object in the distribution
			String argmaxlocation = this.destinationDistribution.getArgMax().getEntity();
			if (destinationDistribution.size()>1){ //else get its margin over nearest distractor
				location_margin = destinationDistribution.getMargins().get(0);
			} 
			
			if (location_margin > 0.13 && !this.selectedDestination.equals(argmaxlocation)){
				//we have a change in argmax, though not necessarily the DestinationUnderDiscussion
				System.out.println("NLU NEW FRAME for argmax location: " + this.destinationDistribution.getArgMax().getEntity());
				selectedDestination = argmaxlocation;
			}
			//as a separate step, whether the argmax has changed or not, 
			//make sure Prob is indeed moving to it if possible
			if (!this.getRobot().getDestinationUnderDiscussion().equals(selectedDestination)&&!selectedDestination.equals("-1")) {	
				//this isn't the destination under discussion as PentoRob is not over it/moving to it
				
				if (incremental){
					//even in incremental mode, we can't move there without a selected piece
					//according to the affordance of the PLACE action
					if (!this.getRobot().getObjectUnderDiscussion().equals("-1")){
						System.out.println("NLU SENDING MOVE_TO_TARGET TO PENTOROB");
						ArrayList<String> dest = new ArrayList<String>();
						dest.add(selectedDestination);
						selectIU = new FrameIU(this.getRobot().getObjectUnderDiscussion(),"MOVE_TO_TARGET", dest, 1.0);
						if (this.currentFrame.payloadEquals(selectIU)&&
								((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_WITH_OBJECT)&&
								this.getRobot().getDestinationUnderDiscussion().equals(selectedDestination)){
							System.out.println("Already commited! ");
							System.out.println(selectIU);
							break;
						}
						this.currentFrame = selectIU;
						newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
					}
				} else {
					//in non-incremental mode, must wait also wait for the other slot
					//to be filled- it is not under discussion from pentorob as no showing of it online
					if (!selectedPiece.equals("-1")){
						System.out.println("NLU SENDING PLACE TO PENTOROB");
						ArrayList<String> dest = new ArrayList<String>();
						dest.add(selectedDestination);
						selectIU = new FrameIU(selectedPiece,"PLACE", dest, 1.0);
						if (this.currentFrame.payloadEquals(selectIU)&&
								((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_TO_OBJECT)&&
								this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)&&
								this.getRobot().getDestinationUnderDiscussion().equals(selectedDestination)){
							System.out.println("Already commited! ");
							System.out.println(selectIU);
							break;
						}
						this.currentFrame = selectIU;
						newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
					}
				}
			}
			
			break;
		case REPAIRING_DESTINATION:
			
			
			subset = new ArrayList<String>();
			rejectedDestinations.add(this.getRobot().getDestinationUnderDiscussion());
			for (String pos : ((PutInNumberedSlot) this.getGame()).getPositionsInPlay()){
				if (!rejectedPieces.contains(pos)){
					subset.add(pos);
				}
			}
		
			destinationDistribution = new Distribution<String>();
			relevantupdate = false;
			for (String targetpos : subset){
				int targetasnumber = Integer.parseInt(targetpos);
				String targetasword = wordUtil.convertInteger(targetasnumber); 
				if (word.equals(targetpos)|
						word.equals(wordUtil.ordinalFromStringInt(targetpos))|
						word.equals(wordUtil.romanFromStringInt(targetpos))|
						word.equals(targetasword)){
					
					//solution is to double the likelihood of the target and renormalize
					//System.out.println("location reference successful");
					destinationDistribution.setProbabilityForItem(targetpos, 2 * destinationDistribution.getProbabilityForItem(targetpos));
					destinationDistribution.normalize();
					relevantupdate = true;
					
					
				}
			}
			if (!relevantupdate){
				break;
			}
			System.out.println(destinationDistribution);
			//fairly pointless final margin step as only one match above (no ambiguity), but keeps things probabilistic
			location_margin = 1.0; //the egregious case of only one object in the distribution
			if (destinationDistribution.size()>1){ //else get its margin over nearest distractor
				location_margin = destinationDistribution.getMargins().get(0);
			}
			if (selectedDestination.equals("-1") && location_margin > 0.13) { 
				System.out.println("FRAME for argmax location: " + this.destinationDistribution.getArgMax().getEntity());
				selectedDestination = this.destinationDistribution.getArgMax().getEntity();
				
				if (incremental){
					//even in incremental mode, we can't move there without a selected piece
					//according to the affordance of the PLACE action
					if (!selectedPiece.equals("-1")){
						ArrayList<String> dest = new ArrayList<String>();
						dest.add(selectedDestination);
						selectIU = new FrameIU(selectedPiece,"MOVE_TO_TARGET", dest, 1.0);
						if (this.currentFrame.payloadEquals(selectIU)
							&&((PentoRob) this.getRobot()).getArmState().equals(ArmState.DROPPING_OBJECT)&&
								this.getRobot().getDestinationUnderDiscussion().equals(selectedDestination)){
							System.out.println("Already commited! ");
							System.out.println(selectIU);
							break;
						}
						this.currentFrame = selectIU;
						newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
					}
				} else {
					//in non-incremental mode, must wait also wait for the other slot
					if (!selectedPiece.equals("-1")){
						ArrayList<String> dest = new ArrayList<String>();
						dest.add(selectedDestination);
						selectIU = new FrameIU(selectedPiece,"PLACE", dest, 1.0);
						if (this.currentFrame.payloadEquals(selectIU)&&
								((PentoRob) this.getRobot()).getArmState().equals(ArmState.MOVING_TO_OBJECT)&&
								this.getRobot().getObjectUnderDiscussion().equals(selectedPiece)&&
								this.getRobot().getDestinationUnderDiscussion().equals(selectedDestination)){
							System.out.println("Already commited! ");
							System.out.println(selectIU);
							break;
						}
						this.currentFrame = selectIU;
						newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
					}
				}
			}
			
			break;
		case REJECTING_DESTINATION:
			//If it's just rejecting the location, then move to the centre of the
			//available options left, or the out of the area
			System.out.println("NLU ABORT destination");
			subset = new ArrayList<String>();
			rejectedDestinations.add(this.getRobot().getDestinationUnderDiscussion());
			for (String pos : ((PutInNumberedSlot) this.getGame()).getPositionsInPlay()){
				if (!rejectedDestinations.contains(pos)){
					subset.add(pos);
				}
			}
			selectedDestination = "-1";
			destinationDistribution = Stats.adjustedDistributionForSubset(destinationDistribution,subset);
			if (destinationDistribution.isEmpty()) {
				System.out.println("WARNING no allowable destinations after prune.");
				rejectedDestinations.clear();
				//have to wait for next word as no possible options
				break;
			}
			System.out.println("ABORTED entropy");
			System.out.println(destinationDistribution.toString());
			System.out.println(destinationDistribution.getEntropy());
			location_margin = 1.0; //the egregious case of only one object in the distribution
			if (destinationDistribution.size()>1){ //else get its margin over nearest distractor
				location_margin = destinationDistribution.getMargins().get(0);
			}
			
			//if (location_margin > threshold) { //may not make sense here- just abandon
			//	System.out.println("After rejection, FRAME for argmax destination: " + destinationDistribution.getArgMax().getEntity());
			//	selectedDestination = destinationDistribution.getArgMax().getEntity();
			//}
			ArrayList<String> dest = new ArrayList<String>();
			dest.add(selectedDestination);
			selectIU = new FrameIU(this.getRobot().getObjectUnderDiscussion(),
					"ABORT_DESTINATION",dest,1.0); 
			if (this.currentFrame.payloadEquals(selectIU)&&
					this.getRobot().getDestinationUnderDiscussion().equals(selectedDestination)){
				System.out.println("Already commited! ");
				System.out.println(selectIU);
				break;
			}
			this.currentFrame = selectIU;
			newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
			break;
			
		case CONFIRMING_DESTINATION:
			System.out.println("NLU requesting DROP");
			ArrayList<String> confirmdest = new ArrayList<String>();
			confirmdest.add(this.getRobot().getDestinationUnderDiscussion());
			selectIU = new FrameIU(this.getRobot().getObjectUnderDiscussion(),
					"DROP", confirmdest,1.0); 
			if (this.currentFrame.payloadEquals(selectIU)
					&&((PentoRob) this.getRobot()).getArmState().equals(ArmState.DROPPING_OBJECT)){
				System.out.println("Already commited! ");
				System.out.println(selectIU);
				break;
			}
			this.currentFrame = selectIU;
			newEdits.add(new EditMessage<FrameIU>(EditType.ADD, selectIU));
			this.initReferenceResolution(); //piece has been confirmed
			
			break;
		case WAITING_FOR_ROBOT_TO_DROP:
			break;
		case WAITING_FOR_ROBOT_TO_GRAB:
			break;
		case WAITING_FOR_ROBOT_TO_MOVE:
			break;
		case WAITING_WHILE_ROBOT_DROPS:
			break;
		case WAITING_WHILE_ROBOT_GRABS:
			break;
		case WAITING_WHILE_ROBOT_MOVES:
			break;
		case START:
			break;
		case END:
			break;
		default:
			break;
		
		}
	}
	
	
	/**
	 * Gathers values for the relevant fluents for the State machine.
	 * Then checks is possible transition, if so, change state.
	 */
	public synchronized boolean updateState(DialogueAct move, Set<String> fluents){
		this.currentDialogueAct = move;
		//given we think we know what the move is, get the relevant fluent values
		
		UserActionState teststate = actionStateMachine.getResultingState(this.userState, move.toString(), fluents);
		if (teststate==null){
			System.out.println("NLU WARNING update state failed from " + this.userState + 
					" with move " + move.toString() + " with conditions "  + Arrays.toString(fluents.toArray()));
			return false; // no change in the state, may need other policy, else keep the same
		}
		this.setUserActionState(teststate);	
		return true;
	}
	
	//Main update loop from ASR wordIUs and Ref Res SlotIUs
	//Principal jobs:
	//1. Decide on the dialogue act type and, where necessary add the right parameters to it.
	//2. Decide on whether the incoming word extends the current dialogue act or starts a new one.
	//3. The decisions will lead to a UserState transition (even if staying in the same state)
	//4. Based on the current user state and the user action/d-act, send on an instruction FrameIU to PentoRob
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		
		newEdits = new ArrayList<EditMessage<? extends IU>>();
		editLoop : for (EditMessage<? extends IU> edit : edits) {
			
			if (!this.getGame().inProgress()){
				continue;
			}
			this.getRobot().updateState(false);//before any interpretation, pentorob's state must be updated
			//before interpreting slots/words, reset the board if needs be if there has been a change
			//TODO this is happening too much
			//if (this.pentorob.getGame().getPiecesinplay().size()!=pieceDistribution.size()){
			//	this.resetPieces();
			//	this.resetReferenceResolution();
			//}
			//if (this.pentorob.getGame().getPositionsInPlay().size()!=destinationDistribution.size()){
			//	this.resetDestinations();
			//}
					
			IU iu = edit.getIU();
			if (iu instanceof SlotIU) {
				//Using refres module to update the piece distribution
				//these come in marginally before the words hit the other decision process with all the words below
				switch (edit.getType()) {
				case ADD:
					SlotIU siu = (SlotIU) iu;
					Distribution<String> rawdist = siu.getDistribution();
					if (rawdist==null||rawdist.isEmpty()) {
						//TODO third disjunct = bug where occasionally first word is way too low for some values
						System.out.println("REFRES WARNING no allowable pieces raw.");
						if (rawdist!=null){
							pieceDistribution = Stats.uniformDistribution(new HashSet<String>(((PutInNumberedSlot) this.getGame()).getPiecesInPlay()));
							//rejectedPieces.clear();
						}
						break;
					} 
					//get here we have a distribution
					System.out.print("wwwwwwwwwww REFRES update: raw");
					System.out.println(rawdist.toString());
					
					if (rawdist!=null&&siu.getDistribution().getArgMin().getProbability()<0.001){
						System.out.println("REFRES WARNING: too low a prob in raw?");
						//pieceDistribution = Stats.uniformDistribution(this.pentorob.getGame().getPiecesinplay());
						//rejectedPieces.clear();
					}
					
					//Only passes in on the pieces in play TODO it could do this later on
					ArrayList<String> subset = new ArrayList<String>();
					for (String piece : ((PutInNumberedSlot) this.getGame()).getPiecesInPlay()){
						if (!rejectedPieces.contains(piece)){
							subset.add(piece);
						}
					}
					pieceDistribution = Stats.adjustedDistributionForSubset(rawdist,subset);
					if (pieceDistribution.isEmpty()) {
						System.out.println("REFRES WARNING no allowable pieces after prune. Have rejected all pieces or finished.");
						pieceDistribution = Stats.uniformDistribution(new HashSet<String>(((PutInNumberedSlot) this.getGame()).getPiecesInPlay()));
						rejectedPieces.clear(); //put them all back in play
						break;
					}
					break;
				case COMMIT:
					break;
				case REVOKE:
					System.out.println("REFRES REVOKE update!");
					System.out.println(iu.toPayLoad());
					break;
				default:
					break;
				}
				
			} else if (iu instanceof WordIU) {
				/******************************************************************************/
				//NLU processing..
				//Currently this is getting words reliably just later than REFRES, so we have up-to-date distributions
				//if (iu.toPayLoad().toLowerCase()=="<sil>"){
				//	continue;
				//}
				System.out.println(iu.toPayLoad());
				//boolean see = true;
				//if (see){
				//	continue;
				//}
				switch (edit.getType()) {
					case ADD:
						this.processAddedWordIU((WordIU) iu);
						break;
					case COMMIT:
						break;
					case REVOKE:
						this.processRevokedWordIU((WordIU) iu); 
						break;
					default:
						break;
				}
			} else if (iu instanceof MessageIU){
				this.getRobot().updateState(false);
				System.out.println("TIME OUT recieved");
				String word = "<silentconfirm>";
			}
		} 
		
		rightBuffer.setBuffer(newEdits);	
	}
}
