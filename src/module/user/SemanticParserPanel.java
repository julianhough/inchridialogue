package module.user;

import java.util.Arrays;
import java.util.List;

import javax.swing.JFrame;

import org.apache.log4j.Logger;

import qmul.ds.ContextParser;
import qmul.ds.InteractiveContextParser;
import qmul.ds.Utterance;
import qmul.ds.dag.DAG;
import qmul.ds.dag.DAGTuple;
import qmul.ds.dag.GroundableEdge;
import qmul.ds.formula.TTRFormula;
import qmul.ds.gui.ParserPanel;

@SuppressWarnings("serial")
public class SemanticParserPanel extends ParserPanel {
	
	private static Logger logger = Logger.getLogger(SemanticParserPanel.class);

	public SemanticParserPanel(boolean applet, JFrame containing) {
		super(applet, containing);
		
	}
	
	public DAG<DAGTuple, GroundableEdge> getParserState(int topN){
		if (super.getParser() instanceof InteractiveContextParser){
			return ((InteractiveContextParser) super.getParser()).getState();
		}
		logger.error("No parser state to return");
		return null;
	}
	
	public void setParserState(DAG<DAGTuple, GroundableEdge> state){
		logger.debug("Re-setting state");
		if (super.getParser() instanceof InteractiveContextParser){
			((InteractiveContextParser) super.getParser()).setState(state);
		} else if (super.getParser() instanceof ContextParser){
			//((ContextParser) this.parser).setState(state);
			logger.error("Parse state not applicable for context parser");
		}
	}
	
	public TTRFormula getBestTTRSemantics(){
		return this.getParser().getBestTuple().getSemantics();
	}
	
	public boolean done(){
		return super.getStatusLabel().getText().equalsIgnoreCase("done");
	}
	
	public boolean error(){
		boolean error = false;
		if (super.getStatusLabel().getText().equalsIgnoreCase("Error parsing")){
			error = true;
			//JOptionPane.getFrameForComponent(this.get).dispose();
		}
		return error;
	}
	
	public boolean ready(){
		return super.getStatusLabel().getText().equalsIgnoreCase("ready");
	}
	
	public boolean parseWordFromExternalSource(String word, boolean turn_released){
		Utterance utt = new Utterance(word);
		getpTextPane().setText(getpTextPane().getText() + word + " ");
		getpTextPane().setCaretPosition(getpTextPane().getText().length()-1);
		//super.parse(turn_released);
		boolean successfulparse = super.getParser().parseUtterance(utt);
		logger.debug("success of parse of " + word + " : " + successfulparse);
		super.updateGUI();
		if (successfulparse){
			this.displayParsedUtterance(utt);
		}
		return successfulparse;
	}
	
	public void removeLastNWordsFromDisplay(int n){
		
		//When words are revoked- TODO could use a strikethrough
		logger.debug("removing last " + n + " words");
		List<String> speakerUtt = Arrays.asList(super.getParsedTextPane().getText().split(":"));
		//logger.debug(speakerUtt);
		String speaker = speakerUtt.get(0);
		List<String> words = Arrays.asList(speakerUtt.get(1).trim().split("\\s+"));
		//logger.debug(words);
		//logger.debug(words.size());
		words = words.subList(0, words.size()-n);
		//logger.info(words);
		super.getParsedTextPane().setText(speaker + ": " + String.join(" ", words) + (words.size()>0 ? " " : ""));
		super.getpTextPane().setText("");
		
	}



}
