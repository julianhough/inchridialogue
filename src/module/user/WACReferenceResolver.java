package module.user;

import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.WordIU;

import java.util.Collection;
import java.util.List;

import module.iu.SceneIU;
import edu.cmu.sphinx.util.props.PropertySheet;
import sium.module.ResolutionModuleWAC;

public class WACReferenceResolver extends ResolutionModuleWAC {
	
	public boolean listening; //the opposite of the hidden ignore variable
	
	@Override
	public void newProperties(PropertySheet ps){
		super.newProperties(ps);
		this.listening = true;
	}
	
	
	@Override
	public void setListen(){
		super.setListen();
		this.listening = true;
	}
	
	@Override
	public void setIgnore(){
		super.setIgnore();
		this.listening = false;
	}
	
	@Override
	public void reset(){
		super.reset();
	}
	
	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
	
		if (!listening) return;
		
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
//			WordIU wordIU = IUUtils.getWordIU(iu);
			if (iu instanceof WordIU){
				switch (edit.getType()) {
				case ADD:
					if (isNormalMode()) 
						applyInstance(iu);
					ground(iu); //note that "ground" here is different from grIn
					setRightBuffer(EditType.ADD, iu);
					break;
					
				case COMMIT:
	//				when training, only consider committed data
					if (isTrainMode())
						addTrainingInstance(iu);
					setRightBuffer(EditType.COMMIT, iu);
					break;
					
				case REVOKE:
	//				this makes the (reasonable) assumption that only the most recent ADD can be revoked
					removeGround(iu);
					revoke(iu);
					setRightBuffer(EditType.REVOKE, iu);
					break;
					
				default:
					break;
				}
			} else if (iu instanceof SceneIU){
				this.setContext(((SceneIU) iu).getContext());
			}
		}
	}

}
