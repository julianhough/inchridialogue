package module.dev;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import module.iu.MessageIU;
import module.robot.PentoRob;
import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;
import edu.cmu.sphinx.util.props.S4Integer;

public class TimeOut extends IUModule {
	/***
	 * A simple timeout module which listens to the callbacks from pentorob.
	 * When there hasn't been a successful started action for N seconds,
	 * pass a TimeOutIU to NLU, which will interpret this lack of action in context.
	 */
	@S4Component(type = PentoRob.class)
	public final static String ROBOT_NAME = "robot";
	private PentoRob pentorob;
	
	@S4Integer(defaultValue = -1)
	public final static String TIME_OUT = "timeout";
	private int timeout; //number of seconds after last pentorob action after which to do something
	private Thread timeoutThread;
	
	private int endtime;
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		System.out.println("Setting up timeout");
		pentorob = (PentoRob) ps.getComponent(ROBOT_NAME);
		
		timeout = ps.getInt(TIME_OUT);
	}

	public void sendTimeOut(){
		System.out.println("About to send time out");
		this.pentorob.updateState(false);
		if (this.pentorob.getCurrentAction()==null){ //if there is indeed no current action
			if ((getTime() - this.pentorob.getEndTimeOfLastCompletedAction()) > (timeout * 1000)){
				//if indeed there has been the timeout maximium of inactivity
				System.out.println("Sending timeout!");
				this.rightBuffer.addToBuffer(new MessageIU(endtime,"Timeout"));
			}
		} else {
			System.out.println("Timeout failed, pento state:");
			System.out.println(pentorob.stateSummary());
		}
	}
	
	private void checkTimeout(int new_endtime) {
		
		if (timeoutThread != null) 
			timeoutThread.interrupt();
		endtime = new_endtime;
//		create a timeout ... if nothing happens after <timeout> seconds, then send time out
		//final int sleepduration = (endtime + (timeout * 1000)) - getTime();
		
		if (timeout != -1) {
			timeoutThread = new Thread(
					new Runnable() {
			    public void run() {
			        try {
			        	int sleepduration = (endtime + (timeout * 1000)) - getTime();
			        	System.out.print("sleeping for ");
			        	System.out.print(sleepduration);
			        	System.out.println();
						Thread.sleep(sleepduration);
					} 
			        catch (InterruptedException e) {
//			        	if something happened within the timeout, then just return (i.e., don't reset the context)
			        	return;
					}
			       sendTimeOut();
			   
			    }
			});
			timeoutThread.start();
		}
	}
		
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
		// TODO Auto-generated method stub
		List<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
		for (EditMessage<? extends IU> edit : edits) {
			IU iu = edit.getIU();
			System.out.println("TIMEOUT Getting IU");
			if (iu instanceof MessageIU) {
				
				long newendtime = ((MessageIU) iu).getTime();
			
				System.out.println("GOT timeout from successful CB");
				System.out.println(newendtime);
				System.out.println(getTime());
				newEdits.add(new EditMessage<MessageIU>(EditType.ADD,(MessageIU) iu));
				//this.checkTimeout(newendtime);
					
				
			}
		}
		rightBuffer.setBuffer(newEdits);
			
	}

}
