package module.dev;

import inpro.incremental.IUModule;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.EditType;
import inpro.incremental.unit.IU;
import inpro.io.SensorIU;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import module.iu.RobotActionIU;
import module.iu.RobotActionIU.Action;

public class FakeRsbListenerModule extends IUModule {

	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) {
	
		List<EditMessage<? extends IU>> newEdits = new ArrayList<EditMessage<? extends IU>>();
		for (EditMessage<? extends IU> edit : edits) {
			switch (edit.getType()) {
			case ADD:
				//only will recieve robot action IUs
				IU iu = edit.getIU();
				int id = iu.getID();
				
				if (iu instanceof RobotActionIU){
					Action a = ((RobotActionIU) iu).getActionType();
					float duration = 0.0f;
					if (a.equals(Action.GRAB_LOW)||a.equals(Action.DROP_LOW)){
						duration = 1550.0f;
					} else if (a.equals(Action.MOVE)){
						duration = 4000.0f;
					} else if (a.equals(Action.GRAB)||a.equals(Action.DROP)){
						duration = 8000.0f;
					}
					
					
					
					SensorIU sensor_iu = new SensorIU(String.valueOf(id)+":"+String.valueOf(duration),"pentorob/callbacks");
					newEdits.add(new EditMessage<SensorIU>(EditType.ADD, sensor_iu));
				}
					
				break;
			case COMMIT:
				break;
			case REVOKE:
				break;
			default:
				break;
			
			}
			
		}
		rightBuffer.setBuffer(newEdits);
		
		
		
		
		
	}

}
