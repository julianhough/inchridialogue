package module.dev;

import inpro.audio.DispatchStream;
import inpro.incremental.IUModule;
import inpro.incremental.PushBuffer;
import inpro.incremental.processor.AdaptableSynthesisModule;
import inpro.incremental.sink.LabelWriter;
import inpro.incremental.unit.EditMessage;
import inpro.incremental.unit.IU;
import inpro.incremental.unit.SysInstallmentIU;
import inpro.synthesis.MaryAdapter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import edu.cmu.sphinx.util.props.PropertyException;
import edu.cmu.sphinx.util.props.PropertySheet;
import edu.cmu.sphinx.util.props.S4Component;

public class Generation extends IUModule {
	
	@S4Component(type = LabelWriter.class)
	public final static String PROP_LABEL_WRITER = "labelWriter";
	
	@S4Component(type = DispatchStream.class)
	public static final String PROP_DISPATCHER = "dispatchStream";
	DispatchStream speechDispatcher;
	
	int stimulusID;
	ArrayList<Integer> randomStimulusIDs;
	String knowledgeFile;
	private AdaptableSynthesisModule synthesisModule;
	private Thread speechThread;
//	for some reason, in this and ALL subclasses, we need to load the hypListeners explicitely
	protected List<PushBuffer> hypListeners; 
	
	@Override
	public void newProperties(PropertySheet ps) throws PropertyException {
		super.newProperties(ps);
		hypListeners = ps.getComponentList(PROP_HYP_CHANGE_LISTENERS, PushBuffer.class);
		System.out.println("Setting up Generation");
		// normal program flow
//	    speechDispatcher = SimpleMonitor.setupDispatcher();
		MaryAdapter.initializeMary();
		init(ps);
	}
	
	protected void init(PropertySheet ps) {
	    speechDispatcher =  (DispatchStream) ps.getComponent(PROP_DISPATCHER);
		synthesisModule = new AdaptableSynthesisModule(speechDispatcher);
	}

	
	@Override
	protected void leftBufferUpdate(Collection<? extends IU> ius,
			List<? extends EditMessage<? extends IU>> edits) { 
		
		for (EditMessage<? extends IU> edit : edits) {
			play(edit.getIU().toPayLoad());
		}
			
	}
	
	protected void finalPlay(String text) {
		System.out.println("playing through speechDispatcher " + text);

		SysInstallmentIU nonincremental = new SysInstallmentIU(text);
		speechDispatcher.playStream(nonincremental.getAudio(), false);
		speechDispatcher.waitUntilDone();
	}

	

	protected void play(String text) {
		text = text.toLowerCase();
		text = text.replaceAll(":", ""); // get rid of colons, which mess up pitch optimization
		text = text.replaceAll(" \\| ", " ");
		
		if (speechThread != null && speechThread.isAlive()) {          
			speechThread.interrupt();
			return;
		} 

		
//		this tells the DM that it is still talking
//		ArrayList<EditMessage<GenerationEventIU>> newEdits = new ArrayList<EditMessage<GenerationEventIU>>();
//		newEdits.add(new EditMessage<GenerationEventIU>(EditType.ADD, new GenerationEventIU("ongoing")));
//		System.out.println("ongoing sent");
//		rightBuffer.setBuffer(newEdits);
//		rightBuffer.notify(hypListeners);
//		newEdits.clear();		
		
		final String text2 = text;
		speechThread = new Thread (new Runnable() {
			public void run() {
				finalPlay(text2);
//				ArrayList<EditMessage<GenerationEventIU>> newEdits2 = new ArrayList<EditMessage<GenerationEventIU>>();
//				newEdits2.add(new EditMessage<GenerationEventIU>(EditType.ADD, new GenerationEventIU("done")));
//				System.out.println("done sent");
//				rightBuffer.setBuffer(newEdits2);
//				rightBuffer.notify(hypListeners);
				System.out.println("THIS IS DONE");
			}
		});
		speechThread.start();
	}
	
}
 