package util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class OrderTheory {

	public static <T> Set<Set<T>> powerSet(Set<T> originalSet) {
	    Set<Set<T>> sets = new HashSet<Set<T>>();
	    if (originalSet.isEmpty()) {
	        sets.add(new HashSet<T>());
	        return sets;
	    }
	    List<T> list = new ArrayList<T>(originalSet);
	    T head = list.get(0);
	    Set<T> rest = new HashSet<T>(list.subList(1, list.size())); 
	    for (Set<T> set : powerSet(rest)) {
	        Set<T> newSet = new HashSet<T>();
	        newSet.add(head);
	        newSet.addAll(set);
	        sets.add(newSet);
	        sets.add(set);
	    }       
	    return sets;
	}  
	
	public static <T> Set<SortedSet<T>> powerSet(Set<T> originalSet, int maxSetSize) {
		Set<Set<T>> powerset = powerSet(originalSet);
		Set<Set<T>> remove =  new HashSet<Set<T>>();
		for (Set<T> s : powerset) {
			 if(s.size()>maxSetSize||s.size()==0){
				 remove.add(s);
			 }
		 }
		for (Set<T> s : remove){
			powerset.remove(s);
			
		}
		
		Set<SortedSet<T>> names = new HashSet<SortedSet<T>>();
		for (Set<T> s : powerset){
			SortedSet<T> sorted = new TreeSet<>();
			sorted.addAll(s);
			names.add(sorted);
		}
		return names;
	}

    public static List<List<List<String>>> partitions(List<String> inputSet) {
        List<List<List<String>>> res = new ArrayList<>();
        if (inputSet.isEmpty()) {
            List<List<String>> empty = new ArrayList<>();
            res.add(empty);
            return res;
        }
        // Note that this algorithm only works if inputSet.size() < 31
        // since you overflow int space beyond that. This is true even
        // if you use Math.pow and cast back to int. The original
        // Python code does not have this limitation because Python
        // will implicitly promote to a long, which in Python terms is
        // an arbitrary precision integer similar to Java's BigInteger.
        int limit = 1 << (inputSet.size() - 1);
        // Note the separate variable to avoid resetting
        // the loop variable on each iteration.
        for (int j = 0; j < limit; ++j) {
            List<List<String>> parts = new ArrayList<>();
            List<String> part1 = new ArrayList<>();
            List<String> part2 = new ArrayList<>();
            parts.add(part1);
            parts.add(part2);
            int i = j;
            for (String item : inputSet) {
                parts.get(i&1).add(item);
                i >>= 1;
            }
            for (List<List<String>> b : partitions(part2)) {
                List<List<String>> holder = new ArrayList<>();
                holder.add(part1);
                holder.addAll(b);
                res.add(holder);
            }
        }
        return res;
    }
    
    public static Set<List<SortedSet<String>>> partitionsWithSetNumberPartitions(List<String> inputSet, int numberPartitions){
    	List<List<List<String>>> res = partitions(inputSet);
    	Set<List<SortedSet<String>>> finalSets = new HashSet<List<SortedSet<String>>>();
    	for (List<List<String>> aList : res){
    		if (aList.size()==numberPartitions){
    			//change to a sorted set here
    			List<SortedSet<String>> newList = new ArrayList<SortedSet<String>>();
    			for (List<String> a_list : aList){
    				newList.add(getSort(a_list));
    			}
    			
    			
    			finalSets.add(newList);
    		}
    	}
    	
    	return finalSets;
    	
    }
    
    public static List<Integer> range(int start, int stop)
    {
       List<Integer> result = new ArrayList<Integer>();

       for(int i=0;i<stop-start;i++){
           result.add(start+i);
       }

       return result;
    }
    
    public static List<String> rangeAsString(int start, int stop)
    {
       List<String> result = new ArrayList<String>();

       for(int i=0;i<stop-start;i++){
           result.add(String.valueOf(start+i));
       }

       return result;
    }
    
    public static TreeSet getSort (List list){
        TreeSet set =new TreeSet(list);
         return set;
        }
    
    public static List<SortedSet<Integer>> permute(Integer[] arr){
    	List<List<Integer>> permute = permuteHelper(arr, 0, new ArrayList<List<Integer>>());
    	List<SortedSet<Integer>> permutations = new ArrayList<SortedSet<Integer>>();
    	for (List<Integer> p : permute){
    		permutations.add(getSort(p));
    	}
    	return permutations;
    	
    }
    


    private static List<List<Integer>> permuteHelper(Integer[] arr, int index, List<List<Integer>> permutationList){
        
    	if(index >= arr.length - 1){ //If we are at the last element - nothing left to permute
            //System.out.println(Arrays.toString(arr));
            //Print the array
    		//System.out.print("[");
            List<Integer> newPermutation = new ArrayList<Integer>();
            for(int i = 0; i < arr.length - 1; i++){
                //System.out.print(arr[i] + ", ");
            	newPermutation.add(arr[i]);
            }
            if(arr.length > 0) 
                //System.out.print(arr[arr.length - 1]);
            	newPermutation.add(arr[arr.length - 1]);
            //System.out.println("]");
            permutationList.add(newPermutation);
            return permutationList;
        }

        for(int i = index; i < arr.length; i++){ //For each index in the sub array arr[index...end]

            //Swap the elements at indices index and i
            int t = arr[index];
            arr[index] = arr[i];
            arr[i] = t;

            //Recurse on the sub array arr[index+1...end]
            permutationList = permuteHelper(arr, index+1, permutationList);

            //Swap the elements back
            t = arr[index];
            arr[index] = arr[i];
            arr[i] = t;
        }
        return permutationList;
    }
	
	public static void main(String[] args){
		Integer[] array = new Integer[range(0,4).size()];
		range(0,4).toArray(array); // fill the array
		List<SortedSet<Integer>> permutations = permute(array);
		System.out.println("*****");
		for (SortedSet<Integer> permutation : permutations){
			System.out.println(permutation);
		}
		
		
		for (String i : rangeAsString(1,4)){
			System.out.println(i);
		}
		
		for (List<SortedSet<String>> partitions : partitionsWithSetNumberPartitions(Arrays.asList("a", "b", "c"), 2)) {
			System.out.println(partitions);
		}
		
		
		
		Set<Integer> mySet = new HashSet<Integer>();
		 mySet.add(1);
		 mySet.add(2);
		 mySet.add(3);
		 for (Set<Integer> s : powerSet(mySet,2)) {
			 System.out.println(s.size());
		     System.out.println(s);
		 }
	}
}
