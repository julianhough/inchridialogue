package util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class UtteranceCollection {
	
	private HashMap<String, HashMap<String, String>> utterances;
	private Random rnd;
	
	public UtteranceCollection(String filename){
		init();
		loadUtterancesFromXML(filename);
	}
	
	public Document readXML(String path) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();  
		DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
		return documentBuilder.parse(path);  
	}
	
	public void init() {
		rnd = new Random();
		rnd.setSeed(12345);
	}
	
	public String getUtt(String id) {
		String utt = null;
		if (utterances.get(id) != null) {
			utt = utterances.get(id).get("text");
		} else {
			System.out.println("No utterance found with ID: " + id);
			System.out.println(utterances.keySet());

		}
		return utt;
	}
	
	// returns random string from the collection
	public String getUtt() {
		Object[] keys = utterances.keySet().toArray();
		return utterances.get((String) keys[rnd.nextInt(utterances.size())]).get("text");
	}
	
	public String getUttByAttributeAtLeast(String attr, float value) {
		String result = null;
		ArrayList<String> potentialResults = new ArrayList<String>();
		for (HashMap<String,String> attributes : utterances.values()) {
			if (!attributes.isEmpty()) {
				if (attributes.get(attr) != null) {
					float v = Float.parseFloat(attributes.get(attr));
					if (v >= (value)) {
						potentialResults.add(attributes.get("text"));
					}
				}
			}
		}
		if (!potentialResults.isEmpty()) {
			Random rnd = new Random();
			result = potentialResults.get(rnd.nextInt(potentialResults.size()));
		}
		return result;
	}
	
	public String getUttByAttributeAtMost(String attr, float value) {
		String result = null;
		ArrayList<String> potentialResults = new ArrayList<String>();
		for (HashMap<String,String> attributes : utterances.values()) {
			if (!attributes.isEmpty()) {
				if (attributes.get(attr) != null) {
					float v = Float.parseFloat(attributes.get(attr));
					if (v <= (value)) {
						potentialResults.add(attributes.get("text"));
					}
				}
			}
		}
		if (!potentialResults.isEmpty()) {
			result = potentialResults.get(rnd.nextInt(potentialResults.size()));
		}
		return result;
	}
	
	public String getUttByAttribute(String attr, String value) {
		String result = null;
		ArrayList<String> potentialResults = new ArrayList<String>();
		for (HashMap<String,String> attributes : utterances.values()) {
			if (!attributes.isEmpty()) {
				if (attributes.get(attr) != null) {
					if (attributes.get(attr).equals(value)) {
						potentialResults.add(attributes.get("text"));
					}
				} else {
					System.out.println("couldn't find attribute: " + attr + ": " + value);
				}
			} else {
				System.out.println("has not attributes");
			}
		}
		if (!potentialResults.isEmpty()) {
			//just randomized if more than 1 result
			result = potentialResults.get(rnd.nextInt(potentialResults.size()));
		} else {
			System.out.println("no results");
		}
		return result;
	}
	
	public void loadUtterancesFromXML(String path) {	
		HashMap<String,HashMap<String,String>> myUtterances = new HashMap<String, HashMap<String,String>>();
		
		try {
			Document doc = readXML(path);
			doc.getDocumentElement().normalize();
			NodeList nodeList = doc.getElementsByTagName("utterance"); 
			
			for (int n=0; n<nodeList.getLength(); n++) {
				HashMap<String,String> myUtterance = new HashMap<String,String>();
				Node node = nodeList.item(n);
				if (node.getNodeType() == Node.ELEMENT_NODE) { 
					Element utterance = (Element) node;
					myUtterance.put("text", utterance.getTextContent());
					
					NamedNodeMap nnm = utterance.getAttributes();
					for (int nn = 0; nn < nnm.getLength(); nn++) {
						Node attribute = nnm.item(nn);
						if(!attribute.getNodeName().equals("id")) {
							myUtterance.put(attribute.getNodeName(), attribute.getNodeValue());
						}
					}
					String id = utterance.getAttribute("id");
					myUtterances.put(id, myUtterance);

				}
			}
		} 
		catch (ParserConfigurationException e) {
			String msg = "Problem loading the xml file.";
			System.out.println(msg);
			e.printStackTrace();
			throw new RuntimeException();
		} 
		catch (SAXException e) {
			String msg = "Problem parsing the xml file.";
			System.out.println(msg);
			e.printStackTrace();
			throw new RuntimeException();
		} 
		catch (IOException e) {
			String msg = "XML file cannot be read...does it exist?";
			System.out.println(msg);
			e.printStackTrace();
			throw new RuntimeException();
		}
		
		utterances = myUtterances;
	}
	
	public static void main(String[] args) {
		UtteranceCollection utts = new UtteranceCollection("/home/dsg-labuser/git/015_pentoland/code/java/Pentoland/resources/Language/ShortFeedback.xml");
		System.out.println(utts.getUtt("neutral1"));
		System.out.println(utts.getUttByAttribute("emotion", "glad"));
		System.out.println(utts.getUttByAttribute("text", "hhhh."));
		System.out.println(utts.getUttByAttributeAtLeast("valence", 0.5f));
		
		//UtteranceCollection utts2 = new UtteranceCollection("/homes/idekok/workspace/ICSpaceDialogue/resource/speech/squat/ErrorDescriptions.xml");
		//System.out.println(utts2.getUttByAttribute("error", "STANCE"));
	}
}