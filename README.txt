Installation on Ubuntu (16)

1. Install Gradle, probably with:

sudo apt install gradle

2. Build and get the dependencies using Gradle:

gradle build


(BELOW STEPS OPTIONAL IF YOU WANT TO USE OPENCV RUN ON PYTHON FOR COMPUTER VISION WITH AN RSB COMMUNICATION LINK)


3. Installing RSB from the cor-lab

http://docs.cor-lab.org/rsb-manual/trunk/html/install-binary.html

Be sure to install the python packages as well. 

Note you'll have to get a verification key for the downloads to work as explained here: http://packages.cor-lab.org/



4. Installing openCV 

(One should follow http://sysads.co.uk/2014/05/install-opencv-2-4-9-ubuntu-14-04-13-10/ with a couple of changes below):

Before starting the instructions on the sysads website, check to see which graphics card you have (see how to do this from the terminal here (for Linux) http://www.cyberciti.biz/faq/linux-tell-which-graphics-vga-card-installed/) or (for Mac) here http://apple.stackexchange.com/questions/74879/how-do-i-get-technical-information-on-my-video-card-model-ram-mhz-etc.

(Linux instructions)
If you don't have an NVIDIA card, make sure you make the right choice in what to run in the instructions on the sysads website. i.e. Make sure in this command you execute the below with 
ocl-icd-libopencl1 instead of libopencv-dev:

sudo apt-get -qq install ocl-icd-libopencl1 build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev libgtk2.0-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils

OR if you do have an NVIDIA card:

sudo apt-get -qq install libopencv-dev build-essential checkinstall cmake pkg-config yasm libjpeg-dev libjasper-dev libavcodec-dev libavformat-dev libswscale-dev libdc1394-22-dev libxine-dev libgstreamer0.10-dev libgstreamer-plugins-base0.10-dev libv4l-dev python-dev python-numpy libtbb-dev libqt4-dev libgtk2.0-dev libfaac-dev libmp3lame-dev libopencore-amrnb-dev libopencore-amrwb-dev libtheora-dev libvorbis-dev libxvidcore-dev x264 v4l-utils

Instead of the open-cv package suggested on the sysads website, download and unzip the below file:
http://sourceforge.net/projects/opencvlibrary/files/opencv-unix/2.4.10/

NOTE there is an error in this standard open-cv distribution, in the file opencv/modules/core/src/system.cpp. 
You need to replace it with one in the Itseez github version:
https://github.com/Itseez/opencv/blob/2.4/modules/core/src/system.cpp
This requires downloading or cloning the 2.4 branch of https://github.com/Itseez/opencv/ and manually replacing the opencv/modules/core/src/system.cpp you downloaded from sourceforge with the version in this repo.

The remaining instructions should now be identical to those on the cysads website.

cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_TBB=ON -D BUILD_NEW_PYTHON_SUPPORT=ON -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=ON -D INSTALL_PYTHON_EXAMPLES=ON -D BUILD_EXAMPLES=ON -D WITH_QT=ON -D WITH_OPENGL=ON ..

(Mac instructions)
https://jjyap.wordpress.com/2014/05/24/installing-opencv-2-4-9-on-mac-osx-with-python-support/


5. Running the CVmodule:

First, attach the webcam via USB and set it up in a good position.

(NB if you are using a USB webcam and if opencv uses your default inbuilt camera (you need to check whether this is the case), you can override by:

sudo mv /dev/video1 /dev/video0

note that this is reverted after reboot)

To run the cv module in terminal, go to code/cvModule/src folder:

python cvModule.py -video 0 -rsb

This enables usage of webcam and RSB to send/receive data to/from InproTK. 

If there are issues with recognizing objects, check the config.py file in cvModule/src for things like minimum and maximum expected size.

When considering the output for object representations the module is giving through RSB (as XML formatted strings such as the below), it is important to get sensible output from each object in terms of its properties (i.e. for the most part, the expected English colour word should be the colour BestResponse. i.e. for the below this is the object representation the Yellow U. This is the visual information that will enter the dialogue system. 

<obj id="3">
	<position global="center top" x="295" y="146"/>
	<shape BestResponse="U">
		<distribution F="0.0" I="0.0" L="0.0" N="0.0" P="0.0" T="0.0" U="1.0" V="0.0" W="0.0" X="0.0" Y="0.0" Z="0.0"/>
		<orientation value="43.0782305863"/>
		<skewness horizontal="left-skewed" vertical="bottom-skewed"/>
		<nbEdges value="8"/>
	</shape>
	<colour BestResponse="Yellow">
		<distribution Blue="1.21128122006e-33" Brown="0.178973237171" Gray="0.000765047198635" Green="7.25471176277e-07" Orange="0.266320192591" Pink="0.0104111171661" Purple="5.62242377834e-32" Red="0.00123365209448" Yellow="0.542296028309"/>
		<hsvValue H="26.1058444547" S="140.216751035" V="132.848136217"/>
		<rgbValue B="63.2632305568" G="122.900138058" R="132.23699954"/>
	</colour>
</obj>

If it is not giving sensible information, either the objects are very unfamiliar to it in terms of colour and shape, or adjustments can be made to the relative size and angle of the camera shot.

To quit the cvModule, simply type 'q' whilst selecting the camera window. To reset the tracker, which is necessary if you're introducing more objects into view, type 'r'.









