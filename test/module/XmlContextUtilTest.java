package module;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.xml.sax.SAXException;

import sium.nlu.context.Context;
import util.WorldBelief;
import util.WorldBeliefParser;
import util.XmlContextUtil;

public class XmlContextUtilTest {

	@Test
	public void testXmlToContext() throws ParserConfigurationException, SAXException, IOException {
		
		Context<String,String> context = new Context<String,String>();
		
		String xmlString = "	<obj id=\"3\"> "+
	"<position global=\"left top\" x=\"58\" y=\"36\"/>"+
	"<shape BestResponse=\"L\">"+
	"	<distribution F=\"0.211178226419\" I=\"0.13178111597\" L=\"0.221920055768\" N=\"0.0913010419543\" P=\"0.0587420489\" T=\"0.0\" U=\"0.013947137604\" V=\"0.164712905676\" W=\"0.0854967613028\" X=\"0.0\" Y=\"0.006973568802\" Z=\"0.013947137604\"/>"+
	"	<orientation value=\"-28.0061243848\"/>"+
    "	<skewness horizontal=\"right-skewed\" vertical=\"bottom-skewed\"/>"+
	"	<nbEdges value=\"9\"/>"+
    "</shape>"+
	"<colour BestResponse=\"Gray\">"+
		"<distribution Blue=\"2.71610413117e-08\" Brown=\"0.162080697696\" Gray=\"0.307031853875\" Green=\"0.0233555470581\" Orange=\"0.13709689745\" Pink=\"0.133059522439\" Purple=\"0.076566009398\" Red=\"0.0435255782886\" Yellow=\"0.117283866634\"/>"+
		"<hsvValue H=\"55.5093930636\" S=\"22.9873554913\" V=\"44.0036127168\"/>"+
		"<rgbValue B=\"45.6907514451\" G=\"45.0252890173\" R=\"46.7478323699\"/>"+
	"</colour>"+
"</obj>";
		
		XmlContextUtil.updateContextWorldBeliefStyle(xmlString, context);
		
		WorldBelief wb = new WorldBeliefParser().convertWorldBeliefFromContext(context);
		System.out.println(wb);
		
		
	}
	
	
}
