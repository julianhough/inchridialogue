0
Gib mir die rechte Kartoffel.
	Give me the potato on the right.
Gib mir die hintere Kartoffel.
	Give me the rear potato.
Gib mir die Kartoffel neben der Banane.
	Give me the potato near the banana.
Gib mir die Kartoffel bei der Banane.
	Give me the potato by the banana.
Gib mir die Kartoffel neben dem roten Apfel.
	Give me the potato near the red apple.
Gib mir die Kartoffel bei dem roten Apfel.
	Give me the potato by the red apple.
Gib mir die Kartoffel zwischen dem Ball und der Banane.
	Give me the potato between the ball and the banana.
Gib mir die Kartoffel zwischen dem blauen Ball und der gelben Banane.
	Give me the potato between the blue ball and the yellow banana.
Gib mir die Kartoffel neben der Banane, dem Apfel und dem Ball.
	Give me the potato near the banana, the apple and the ball.
Gib mir die Kartoffel bei der Banane, dem Apfel und dem Ball.
	Give me the potato by the banana, the apple and the ball.

1
Gib mir den Ball.
	Give me the ball.
Gib mir den blauen Ball.
	Give me the blue ball.
Gib mir den runden Ball.
	Give me the round ball.
Gib mir den Ball zwischen dem roten und dem grünen Apfel.
	Give me the ball between the red and the green apple.
Gib mir den Ball in der Mitte.
	Give me the ball in the center.
Gib mir den Ball zwischen den Kartoffeln.
	Give me the ball between the potatos.

2
Gib mir den Korb.
	Give me the basket.
Gib mir den runden Korb.
	Give me the round basket.
Gib mir den Korb in der Mitte.
	Give me the basket in the center.
Gib mir den großen Korb.
	Give me the big basket.
Gib mir den großen runden Korb.
	Give me the big round basket.
Gib mir den großen runden Korb in der Mitte.
	Give me the big round basket in the center.

3 or 4
Gib mir die Kartoffel hier vorne.
	Give me the potato here at the front.
Gib mir die mittlere Kartoffel.
	Give me the potato in the center.
Gib mir die Kartoffel in der Mitte.
	Give me the potato in the center.
Gib mir die vordere Kartoffel.
	Give me the potato at the front.
Gib mir die Kartoffel zwischen dem blauen Ball und dem grünen Apfel.
	Give me the potato between the blue ball and the green apple.

5
Gib mir den Apfel in der Mitte.
	Give me the apple in the center.
Gib mir den grünen Apfel in der Mitte.
	Give me the green apple in the center.
Gib mir den grünen Apfel neben der Kartoffel.
	Give me the green apple near the potato.
Gib mir den grünen Apfel bei der Kartoffel.
	Give me the green apple by the potato.

6
Gib mir den mittleren grünen Apfel.
	Give me the central green apple.
Gib mir den linken vorderen grünen Apfel.
	Give me the left green apple in the front.
Gib mir den vorderen linken grünen Apfel.
	Give me the left green apple in the front.

7
Gib mir die Banane ganz links.
	Give me the banana to the very left.
Gib mir die Banane neben den grünen Äpfeln.
	Give me the banana near the green apples.
Gib mir die Banane bei den grünen Äpfeln.
	Give me the banana by the green apples.

8
Gib mir die Banane neben dem roten Apfel.
	Give me the banana near the red apple.
Gib mir die Banane bei dem roten Apfel.
	Give me the banana by the red apple.
Gib mir die Banane ganz rechts.
	Give me the banana on the right.
Gib mir die Banane neben der Kartoffel.
	Give me the banana near the potato.
Gib mir die Banane bei der Kartoffel.
	Give me the banana by the potato.

9
Gib mir den roten Apfel.
	Give me the red apple.
Gib mir den roten Apfel neben der Banane.
	Give me the red apple near the banana.
Gib mir den roten Apfel bei der Banane.
	Give me the red apple by the banana.
Gib mir den roten Apfel neben der Kartoffel.
	Give me the red apple near the potato.
Gib mir den roten Apfel bei der Kartoffel.
	Give me the red apple by the potato.
Gib mir den Apfel neben der Banane, dem Ball und der Kartoffel.
	Give me the apple near the banana, the ball and the potato.
Gib mir den Apfel bei der Banane, dem Ball und der Kartoffel.
	Give me the apple by the banana, the ball and the potato.

10
Gib mir den hintersten Apfel.
	Give me the very rear apple.
Gib mir den Apfel hinten links.
	Give me the rear apple on the left.
